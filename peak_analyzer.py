import numpy as np
import sys

rdf = np.loadtxt(sys.argv[1])

record_a = True
for i, line in enumerate(rdf):
    val = line[0]
    if val >= 2.8 and record_a == True:
        a = i
        record_a = False
    if val >= 3.4:
        b = i
        break

#print rdf[a:b][:,1]

peak = np.amax(rdf[a:b][:,1])
#print peak
average = np.mean(rdf[a:b][:,1])
print(average)
error = np.std(rdf[a:b][:,2])
#print error
limit = 3.0*error + average
#print limit

#if peak >= limit and (abs(average-1.)>=0.5):
#    print "True"
#else:
#    print "False"

if average < 2.5:
    print("Vapor")
else:
    print("Drop")
