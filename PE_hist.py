import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
import matplotlib.mlab as mlab

def get_eq(array):
    size = len(array)
    end = size - int(0.1*size)
    print("SIZE", size, "END", end)
    mean = np.mean(array[end:])
    print("MEAN", np.mean(array), "NEW MEAN", mean)
    for i in range(5, size):
        if array[i] <= mean:
            eq = i
            break
    print(eq)
    return eq

log = np.loadtxt('log', skiprows = 1)
PE = log[:,1]/1e3
LJ = log[:,6]/1e3

PEeq = get_eq(PE)
LJeq = get_eq(LJ)

#PEn, PEbins, PEpatches = plt.hist(PE[PEeq:], bins = 5, label = "Potential Energy", normed = True)
#LJn, LJbins, LJpatches = plt.hist(LJ[LJeq:], bins = 5, label = "Lennard-Jones", normed = True)
PEn, PEbins = np.histogram(PE[PEeq:], bins = 100, normed = True)
LJn, LJbins = np.histogram(LJ[LJeq:], bins = 100, normed = True)


muPE, sigmaPE = norm.fit(PE[PEeq:])
muLJ, sigmaLJ = norm.fit(LJ[LJeq:])
PEplot = mlab.normpdf(PEbins, muPE, sigmaPE)
LJplot = mlab.normpdf(LJbins, muLJ, sigmaLJ)
l = plt.plot(PEbins-np.mean(PE[PEeq:]), PEplot, 'b-', linewidth = 2, label = "Potential Energy")
k = plt.plot(LJbins-np.mean(LJ[LJeq:]), LJplot, 'g-', linewidth = 2, label = "Lennard-Jones")

#plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
plt.xlabel("Energy (1 x 10$^{3}$)")
plt.ylabel("Probability (1 x 10$^{-3}$)")
plt.title("Ordered (F-Pe, $\phi$ = 1.15, T = 10.0)", fontsize = "small")
plt.legend(loc = "upper left", fontsize = "x-small", handlelength = 1)
plt.xlim([-15,10])
plt.savefig("energy_histogram.pdf")
