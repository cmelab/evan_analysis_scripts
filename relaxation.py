#Import files
from sys import argv
import numpy as np
from numpy import *
import matplotlib.pyplot as plt

#Load the data into the file
data = loadtxt(argv[1])

data = loadtxt(argv[1], skiprows = 1)
#Splitting up the data:
x = data[:,0]
y = data[:,1]

#doing the standard deviation of the whole graph
last = y[-4:]
std = np.std(last)
std = float(std)
print("Your standard deviation is: %s" %std)
#get the final position
final_position = y[-1:]
final_position = float(final_position)
print("Your final position is: %d" %final_position)

#set the limits of the graph
limit = 100*std + final_position
print("Your uppoer limit is: %s" %limit)
limit = float(limit)
#Do the average over the limits
row = 1
moveave = final_position
num_array = []
while moveave < limit:    
    newmat = y[-(int(row)):]
    moveave = sum(newmat)/len(newmat)
    #print moveave
    row_length = int(row)
    length_num_array = int(len(num_array))
    if moveave < std:  
        num_array.append(float(moveave))
#    print row
    row = row + 1


length_array = len(num_array)
y = -int(length_array)
x = np.loadtxt(x, skiprows= 1)
x = x[y:]
lengthx = len(x)

#print lengthx
#print length_array

#print num_array
#print x
#Doing the Graph
plt.plot(x,num_array)
#plt.show()
