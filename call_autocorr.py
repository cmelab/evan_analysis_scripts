from cme_utils.analyze import autocorr

if __name__=="__main__":
#    t = autocorr.autocorrelation()
    print("DOING P3HT SHRINK INFO FOR AUTOCOR.")
    t = autocorr.autocorrelation(log_write_time=1e5, start_t = 1e8, dcd_write_time=1e7)

    to_write = "%s %s" %(int(t[0]/2), t[1])
    autocorr_text_file = './'+'autocorr.txt'
    with open(autocorr_text_file, 'w+') as textFile:
        textFile.writelines(to_write)
