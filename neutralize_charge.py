'''This script takes an xml file and neutralizes
the charge by subracting the mean charge from
every atom. Then writes an xml with the 
correct charges.'''
import numpy
import helper_functions.matty_helper as mh
'''matty_helper is used to parse and rewrite
the xml files. It is found in the
helper_functions directory of the repo.'''

def neutralize(array):
    '''Takes numpy.array containing charge
    data and returns a list of the reduced
    charges.'''
    t_charge = numpy.mean(array)
    print("OLD CHARGE:", t_charge)
    new_charge = []
    for i in range(len(array)):
        a_charge = float(array[i])
        a_charge -= (t_charge)
        new_charge.append(a_charge)
    print("NEW CHARGE:", numpy.mean(new_charge))
    return new_charge

input_xml = 'init.xml'
output_xml = 'neutral.xml'
xml = mh.loadMorphologyXML(input_xml)
charge = numpy.array(xml['charge'])

new_charge = neutralize(charge)

xml['charge'] = new_charge

mh.writeMorphologyXML(xml, output_xml)
