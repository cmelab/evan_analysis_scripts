#!/home/evan/miniconda2/bin/python
import numpy as np
import matplotlib.pyplot as plt
import os
import math
import sys

def parse_PE_data(a1, a2, count = 0):
    m = len(data)
    n = len(order)
    for i in range(n):
        for j in range(m):
            if order[i][0] == data[j][0] and order[i][1] == data[j][1] and order[i][2] <= 0.5:
                count += 1
                row = np.array( [data[j][0], data[j][1], data[j][2], data[j][3], data[j][4]] )
                total_list.append(row)
    dist = math.sqrt(float(count))
    global dist
    return total_list

def parse_ORDER_data(a1, a2, count = 0):
    m = len(data)
    n = len(order)
    for i in range(n):
        for j in range(m):
            if order[i][0] == data[j][0] and order[i][1] == data[j][1] and order[i][2] >= 0.5:
                row = np.array( [data[j][0], data[j][1], data[j][2], data[j][3]] )
                total_list.append(row)
                count += 1
    dist = math.sqrt(float(count))
    global dist
    return total_list

def get_title():
    direct = os.getcwd().split('/')
    mol = direct[-2]
    if mol == 'ua_perylene':
        molecule = "Perylene"
    elif mol == 'perylothiophene':
        molecule = "Perylothiophene"
    system = "%s" %direct[-1]
    if system == 'Rigid' or system == 'rigid' or system == 'phitests' or system == 'phi_tests':
        model = "R"
    elif system == 'Nonrigid' or system == 'flexible' or system == 'nonrigid-phi' or system == 'nonrigid-phi_tests':
        model = "F"
    title = model + "-" + molecule
    global title
    return title

def plot_TPS(array):
    array = array
    mean = float(np.mean(array))
    deviation = np.std(array)/dist
    ordofmag = int(math.log10(deviation))
    mean = round(mean, -ordofmag)
    deviation = round(deviation, -ordofmag)
    legend = "Mean: " + str(int(mean)) + " $\pm$ " + str(int(deviation))
    plt.hist(data, np.arange(1400, 7000, 500))
    plt.axvspan(mean, mean, color = 'r', alpha = 0.3, label = legend)
    plt.ylabel("Counts")
    plt.legend(fontsize = 'x-small', handlelength = 0.1)
    plt.xlim([1400, 7200])
    plt.xlabel("Timesteps per Second")
    plt.title(title)
    plt.savefig(DATA+"-"+title+"-"+"TPS.pdf")
    plt.close()
    del mean, deviation

def plot_ta(array):
    array = array
    print((title + "-Autocor"))
    plt.hist(array, bins = np.arange(0, 15, 1))
    mean = np.mean(array)
    deviation = np.std(array)/dist
    print(("Deviation", deviation))
    ordofmag = int(round(math.log10(deviation)))
    mean = round(mean, -ordofmag)
    #deviation = round(deviation, 0)
    #mean = int(round(mean, -ordofmag))
    deviation = int(round(deviation, 0))
    if deviation <= 0.01:
        deviation = 0.01
    #deviation = round(deviation, -ordofmag)
    print(("NEW deviation", deviation))
    legend = "Mean: " + str(mean) + " $\pm$ " + str(deviation)
    print(legend)
    plt.xlabel("Timesteps (x $10^{6}$)")
    plt.xlim([0, 15])
    plt.axvspan(mean, mean, color = 'r', alpha = 0.3, label = str(legend))
    plt.ylabel("Counts")
    plt.legend(fontsize = 'x-small', handlelength = 0.1)
    plt.title(title)
    plt.savefig(DATA+"-"+title+"-"+"Autocor.pdf")
    plt.close()
    del mean, deviation

def plot_tr(array):
    print(array)
    print((title + "-Relaxation"))
    mean = float(np.mean(array))
    deviation = np.std(array)/dist
    #mean = int(float(np.mean(array)))
    #deviation = int(np.std(array)/dist)
    print(("Deviation", deviation))
    ordofmag = int(round(math.log10(deviation)))
    #mean = round(mean, -ordofmag)
    #deviation = round(deviation, -ordofmag)
    mean = int(round(mean, -ordofmag))
    deviation = int(round(deviation, -ordofmag))
    if deviation <= 1:
        deviation = 1
    print(("NEW deviation", deviation))
    legend = "Mean: " + str(mean) + " $\pm$ " + str(deviation)
    print(legend)
    plt.hist(array, bins = np.arange(0, 120, 5))
    plt.axvspan(mean, mean, color = 'r', alpha = 0.3, label = legend)
    plt.ylabel("Counts")
    plt.legend(fontsize = 'x-small', handlelength = 0.1)
    plt.xlabel("Timesteps (x 10$^{6}$)")
    plt.xlim([0, 120])
    plt.title(title)
    plt.savefig(DATA+"-"+title+"-"+"Relaxation.pdf")
    plt.close()
    del mean, deviation

order = np.genfromtxt("order.txt")
total_list = []
name = get_title()

DATA = sys.argv[1]
#DATA = "PE"
count = 0

if DATA == "PE":
    print("DOING POTENTIAL")
    data = np.loadtxt("sim-data.txt")
    total_list = parse_PE_data(data, order, count)
    total_list = np.array(total_list)
    TPS = total_list[:,2]
    ta = total_list[:,3]
    tr = total_list[:,4]/1e6
    graph_TPS = plot_TPS(TPS)
    graph_ta = plot_ta(ta)
    graph_tr = plot_tr(tr)

if DATA == "ORDER":
    print("DOING ORDER")
    data = np.loadtxt("order-data.txt")
    total_list = parse_ORDER_data(data, order, count)
    total_list = np.array(total_list)
    ta = total_list[:,2]
    tr = total_list[:,3]
    graph_ta = plot_ta(ta)
    graph_tr = plot_tr(tr)

#ave_list = []
#for i in range(len(ta)):
#    if ta[i] <= 12.0:
#        ave_list.append(ta[i])
