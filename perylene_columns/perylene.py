import numpy as np
import hoomd
from hoomd import md
from hoomd import deprecated
from cme_utils.manip.initialize_system import interactions_from_xml_verbatim, interactions_from_xml_tabulated

normalization = 3.8
sigma = normalization
diam = 1.0

molA = np.array([
[0.943255, 1.932, 1.836985],
[2.272255, 1.927, 2.305985],
[3.128255, 0.965, 1.858985],
[2.694255, -0.044, 0.929985],
[3.587255, -1.027, 0.481985],
[3.153745, -1.963, -0.425985],
[1.838745, -1.946, -0.876985],
[0.922745, -1.01, -0.455985],
[1.349255, -0.026, 0.468985],
[0.461255, 0.992, 0.942985],
[-0.943255, -1.932, -1.836985],
[-2.272255, -1.927, -2.305985],
[-3.128255, -0.965, -1.858985],
[-2.694255, 0.044, -0.929985],
[-3.587255, 1.027, -0.481985],
[-3.153745, 1.963, 0.425985],
[-1.838745, 1.946, 0.876985],
[-0.922745, 1.01, 0.455985],
[-1.349255, 0.026, -0.468985],
[-0.461255, -0.992, -0.942985]
])/sigma
molA_types = []
molA_diam = 20*[diam]
molA_types += 20*['C']
molA = tuple(map(tuple, molA))

hoomd.context.initialize()

system = hoomd.init.create_lattice(hoomd.lattice.sc(4, type_name = 'A'), n=5)

system.particles.types.add('C')

nl = md.nlist.cell()

#lj, bonds, angles, dihedrals = interactions_from_xml_verbatim(system,factor=1.0, model_file= 'model.xml', model_name= 'default')

#lj.pair_coeff.set('A', 'C', epsilon = 0, sigma = 1.0)

center = hoomd.group.rigid_center()
rigid = hoomd.md.constrain.rigid()
grp = hoomd.group.all()

rigid.set_param('A', positions=molA, types = molA_types, diameters = molA_diam)

rigid.create_bodies()
hoomd.deprecated.dump.xml(grp, "init.xml", all=True)
exit()


hoomd.md.integrate.mode_standard(dt=1e-4, aniso = True)
int_rig = hoomd.md.integrate.nvt(group = center,kT=0.5,tau=2.)

hoomd.dump.dcd(filename = "traj.dcd", period = 1e2, overwrite = True)
hoomd.analyze.log(filename = "log", quantities = ['potential_energy'], period = 1e3, overwrite = True)
hoomd.deprecated.dump.xml(grp, "init.xml", all=True)
hoomd.run(1e3)
print("\nYOUR BOX IS:")
print(system.box)
print("\n \nI just ran the simulation!\n \n")
hoomd.deprecated.dump.xml(grp, "post.xml", all=True)
