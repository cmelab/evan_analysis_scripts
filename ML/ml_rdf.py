import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import pickle

def new_data(phase_dirs):
    '''Takes in a list containing the filenames for the phase data.'''
    posits, phase = np.empty([0,100]), np.empty([0,2])
    for item in phase_dirs:
        posits, phase = collate(array1 = posits, array2 = phase, dirc = item)
    posits.dump('rdf.dat')
    phase.dump('phase.dat')

def collate(array1, array2, dirc):
    runs = np.loadtxt(dirc+'phase_file.txt')
    for run in runs:
        load = np.loadtxt(dirc+'rdf/struc-phi-{:1.2f}-T-{:04.1f}.txt'.format(run[0], run[1]))
        #print(dirc+'rdf/struc-phi-{:1.2f}-T-{:04.1f}.txt'.format(run[0], run[1]))
        #print(len(load[1,:]))
        load = load[1,:100]
        load = load.flatten()
        array1 = np.vstack((array1, load))
        labels = np.zeros(2)
        labels[int(run[5])] = 1.
        array2 = np.vstack((array2, labels))
    return array1, array2

def simple(plots, labels,n=100,n_classes=2): #start with classes: equlibrated or not.
    #create model
    x = tf.placeholder(tf.float32,[None,n])
    W = tf.Variable(tf.zeros([n,n_classes]))
    b = tf.Variable(tf.zeros([n_classes]))
    y = tf.matmul(x,W)+b

    #define loss and optimizer
    y_ = tf.placeholder(tf.float32,[None,n_classes])
    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

    #start session
    sess = tf.Session()
    init = tf.global_variables_initializer()
    sess.run(init)

    #train
    n = len(plots)
    print("Attempting to train from {} points.".format(n))
    h = n//2
    f = .8
    d = int(n*f/2)
    print("Starting to train")
    training_plots = plots[h-d:h+d]
    training_labels = labels[h-d:h+d]
    for _ in range(2000):
        sess.run(train_step, feed_dict={x:training_plots,y_:training_labels})

    #test trained model
    print("Starting to test")
    test_plots = np.vstack((plots[:h-d],plots[h+d:]))
    test_labels = np.vstack((labels[:h-d],labels[h+d:]))
    correct = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
    accuracy = tf.reduce_mean(tf.cast(correct,tf.float32))
    print("accuracy is: ",sess.run(accuracy,feed_dict={x:test_plots,y_:test_labels}))
    
if __name__=="__main__":
    phase_dirs = ['flexible_perylene/', 'rigid_perylene/', 'flexible_perylothiophene/', 'rigid_perylothiophene/']
    new_data(phase_dirs)
    posits, phase = np.load('rdf.dat'), np.load('phase.dat')
    simple(posits, phase)

    #for i, val in enumerate(posits):
    #    print(posits[i], phase[i])
