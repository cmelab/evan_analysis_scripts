"""This script is meant to take in a series of rdfvalues and determine at which point the RDF for other temperatures is no longer distinguishable from the highest temperature. This should work tell the vapor transition if the highest temperature run is vapor"""

"""Import modules."""
import sys
import numpy
import glob
import matplotlib.pyplot as plt
import re
import os

data = []#Place RDF data here.
temp = []#To store the temperature of the run.

"""Phi value for these RDFs"""
directory = os.getcwd().split('/')[-1]
phi = re.findall(r'phi(.*)',directory)
phi = phi[0]

"""Loop over all the RDF files in the directory"""
for run in glob.glob("rdf-*.txt"):
    data.append(numpy.loadtxt(run))
    """num is to extract the run temperature from naming scheme"""
    num = run[4:8]
    num = float(num)
    temp.append(num)
    """Plot the data if desired."""
    plt.plot(data[-1][:,0],data[-1][:,1],label = num)
    """Get the data to compare over the desired range.
    2.9 to 3.4 is chosen due to not too much pi stacking
    and the intracolumnar distances. """
    record_a = True
    for i, line in enumerate(data[-1]):
        val = line[0]
        if val >= 2.9 and record_a == True:
            a = i
            record_a = False
        if val >= 3.4:
            b = i
            break

"""Create image."""
plt.legend(fontsize = 'xx-small', ncol =3)
plt.savefig("rdf.pdf")

"""Set up comparison."""
compare = numpy.mean(data[-1][a:b][:,1])
maximum = numpy.mean(data[-1][a:b][:,1])

"""Make comparison and print if distinguishable."""
for i in range(len(data)):
#    print temp[i], numpy.mean(data[i][a:b][:,1])-1.
    if abs(numpy.mean(data[i][a:b][:,1])-compare) <= maximum*0.05/float(phi) or abs(1.0-numpy.mean(data[i][a:b][:,1])) <= 0.1 :
        print((phi, temp[i], "1"))
    else:
        print((phi, temp[i], "0"))
