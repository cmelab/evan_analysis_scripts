#Import the necessary files.
import sys
import numpy 
import matplotlib.pyplot as plt
from operator import itemgetter

"""Enumerate over all common conditions to create phase diagram."""

def det_den(array):
    if array[3] == 0:
        phase = 0.
        return phase
    else:
        phase = 1.
        return phase

def det_rdf(array):
    if array[0] <= 0.5:
        phase = det_den(array)
        return phase
    else:
        phase = 2.
        return phase

def det_order(array):
    if array[2] < 0.4:
        phase = det_rdf(array)
        return phase
    else:
        phase = 3.
        return phase

def det_phase(array):
    if array[4] < 0.85:
        phase = det_order(array)
        return phase
    else: 
        phase = 4.
        return phase
        
def findall(order, vapor, lowT):
    tot = numpy.concatenate((order[:,0:2], vapor[:,0:2], lowT[:,0:2]), axis=0)
    b = numpy.ascontiguousarray(tot).view(numpy.dtype((numpy.void, tot.dtype.itemsize * tot.shape[1])))
    _, idx = numpy.unique(b, return_index=True)
    unique_tot = tot[idx]
    buff = numpy.zeros((len(unique_tot[:,0]),4))
    unique_tot = numpy.concatenate((unique_tot, buff), axis = 1)
    return unique_tot

def plot_data(array):
    datum = {4.:'ms', 3.:'rs', 2.:'cs', 1.:'bs', 0.:'gs'}
    plt.plot(array[0], array[1], datum[array[5]], markersize=20)

#Import order and vapor txt files and convert to array.
order = numpy.genfromtxt("order.txt")
vapor = numpy.genfromtxt("vapor.txt")
lowT = numpy.genfromtxt("column_order.txt")

tot = findall(order, vapor, lowT)

"""Used on the loop for whichever is longer to ensure all points are captured.
Create array with all points."""
n = len(tot)
m = len(order)
for i in range(n):
    for j in range(m):
        if tot[i][0] == order[j][0] and tot[i][1] == order[j][1]:
            tot[i][2] = order[j][2]

n = len(tot)
m = len(vapor)
for i in range(n):
    for j in range(m):
        if tot[i][0] == vapor[j][0] and tot[i][1] == vapor[j][1]:
            tot[i][3] = vapor[j][2]

n = len(tot)
m = len(lowT)
for i in range(n):
    for j in range(m):
        if tot[i][0] == lowT[j][0] and tot[i][1] == lowT[j][1]:
            tot[i][4] = lowT[j][2]

O_D = False
for i, val in enumerate(tot):
    phase = det_phase(val)
    if O_D:
        if phase >= 3:
            phase = 1
        else:
            phase = 0
    tot[i,5] = phase
    plot_data(tot[i])
    
total = tot.tolist()

total.sort(key=lambda x: (x[0], x[1]))

if O_D:
    phase_file = open('phase_file.txt', 'w')
else: 
    phase_file = open('all_phase_file.txt', 'w')
for i, val in enumerate(total):
    phase_file.write("{} {:04.1f} {} {} {} {}\n".format(val[0], val[1], val[2], val[3], val[4], val[5]))
    print("{} {:04.1f} {} {} {} {}".format(val[0], val[1], val[2], val[3], val[4], val[5]))

#Plot options.
plt.xlabel("$\phi$")
plt.ylabel('T')
plt.title('Flex Perylene')
plt.axis([-.1,1.5,0,30])
plt.xticks([0.01,0.1,0.3,0.5,0.7,0.85,1.0,1.15,1.3,1.45])
#plt.yticks([10,12,14,16,18,20,22,24,26])
plt.grid(True)
#plt.savefig('diagram.pdf')
#plt.show()
