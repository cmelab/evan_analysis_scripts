#!/home/evan/anaconda/bin/python
"""
The purpose of the script is to calculate normal vectors
for each molecule in the script using mdtraj from the 
final frame.The dot product from the normal vectors is 
calculated, and an average dot prodcut value is determined.
This gives an measure of the order of the rings.
"""
#Import necessary modules
from sys import argv
import numpy as np
import mdtraj as md

#Gets the values from traj and restart 
TRAJ_FILE = argv[1]#this is the traj.dcd file
TOP_FILE = argv[2]#This is the restart.hoomdxml file
t = md.load(TRAJ_FILE, top = TOP_FILE)

#A function to set up the normal vector. 
cp_list = [] #An empty list for the normal vectors
axes = t.unitcell_lengths[t.n_frames-1]

def pbc(vec):
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]= v-axes[i]
        if v<-axes[i]/2.:
            vec[i]= v+axes[i]
    return vec

def vector_creation(p1, p2, p3):
    v1 = pbc(p3 - p1)
    v2 = pbc(p3 - p2)
    cp = np.cross(v1, v2)
    cp /= np.linalg.norm(cp)
#    print "Normal vector: %s" %cp
    cp_list.append(cp)

i = (t.n_frames - 1) #Gets final frame.
num_residues = 21 #This is needed because xml file does not have them as moledules. It is 20 for UA-perylene and 21 for UA-perylothiophene.
for j in range(t.n_atoms/num_residues):
    p1 = t.xyz[i,(4+j*num_residues),:]#This is multiplied by 10 because the mdtraj is in nanometers.
#    print "Point one: %s" %p1
    p2 = t.xyz[i,(8+j*num_residues),:]
#    print "Point two: %s" %p2
    p3 = t.xyz[i,(11+j*num_residues),:]
#    print "Point three: %s" %p3
    vector_creation(p1, p2, p3)

dot_list=[]
for j in range(len(cp_list) - 1):
    for i in range(j+1,len(cp_list)):
        a = cp_list[j]
        b = cp_list[i]
        dot_product = np.dot(a, b)
        dot_list.append(dot_product)
average = np.mean(dot_list)
absaverage = np.mean(np.absolute(dot_list))
deviation = np.std(dot_list)
absdeviation = np.std(np.absolute(dot_list))
print absaverage, absdeviation
