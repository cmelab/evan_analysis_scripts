#!/home/evan/anaconda/bin/python
"""The purpose of the script is to calculate normal vectors
in the script using mdtraj to go from frame to frame.
The normal vectors will be plotted for each frane so a
movie can be created. 
The dot-product order will also be added in.
"""
#Import necessary modules
from sys import argv
import numpy as np
import matplotlib.pyplot as plt
import mdtraj as md
from mpl_toolkits.mplot3d import Axes3D
import pylab

#Some lines to set up the plot
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_xlim([-1,1])
ax.set_ylim([-1,1])
ax.set_zlim([-1,1])

#Gets the values from traj and restart 
TRAJ_FILE = argv[1]#this is the traj.dcd file
TOP_FILE = argv[2]#This is the restart.hoomdxml file
t = md.load(TRAJ_FILE, top = TOP_FILE)

cp_list = [] #An empty list for the normal vectors
axes = t.unitcell_lengths[t.n_frames-1]

def pbc(vec):
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]= v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

#A function to set up the normal vector. 
def vector_creation(p1, p2, p3):
    v1 = pbc(p3 - p1)
    v2 = pbc(p3 - p2)
    cp = np.cross(v1, v2)
    cp_list.append(cp/np.linalg.norm(cp))

#A function to plot the vectors
def vector_plot(row):
    crp = abs(np.array(cp_list))
    X = crp[row,0]
    Y = crp[row,1]
    Z = crp[row,2]
    ax.quiver(X,Y,Z,X,Y,Z)

num_residues = 20 #This is needed because xml file does not have them as moledules. It is 20 for UA-perylene and 21 for UA-perylothiophene.
#Need to loop over all the frames.
for i in range(t.n_frames):
    #I need to loop over all the rings in the simulations and get the coords.
    for j in range(t.n_atoms/num_residues):
        p1 = t.xyz[i,(4+j*num_residues),:]
        p2 = t.xyz[i,(9+j*num_residues),:]
        p3 = t.xyz[i,(11+j*num_residues),:]
        vector_creation(p1, p2, p3)
    for j in range(t.n_atoms/num_residues):
        vector_plot(j)
    #print cp_list[0] 
    
    #Set up the dot product for each frame.
    dot_list=[]
    for k in range(len(cp_list) - 1):
        for l in range(k+1,len(cp_list)):
            a = cp_list[k]
            b = cp_list[l]
            dot_product = np.dot(a, b)
            dot_list.append(dot_product)
    absaverage = np.mean(np.absolute(dot_list))
    absdeviation = np.std(np.absolute(dot_list))

    #Some lines to set up the plot
    fig.text(0,0,"Dot Product = %f +/- %f" %(absaverage, absdeviation))  
    plt.title("Normal Vectors for Frame {}".format(i))
    if i < 10:
        plt.savefig("Normal_Vectors_Frame_0000{}".format(i))
    else:
        plt.savefig("Normal_Vectors_Frame_000{}".format(i))
    print("Finished plot {}".format(i))
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.set_xlim([-1,1])
    ax.set_ylim([-1,1])
    ax.set_zlim([-1,1])
    cp_list = []
