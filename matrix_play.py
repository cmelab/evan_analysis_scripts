import numpy as np

#[aa', ab', ac', ad', ae', ba', bb', bc', bd', be', ca', cb', cc', cd', ce', da' db', dc', dd', de', ea', eb', ec', ed', ee']
def build_graph():
    A = np.zeros((5,5))
    A[0][1] = 1
    A[1][0] = 1
    A[1][2] = 1
    A[2][1] = 1
    A[2][3] = 1
    A[2][4] = 1
    A[3][2] = 1
    A[4][2] = 1
    return A

def huge_matrix(G1, G2):
    m1 = len(G1[0,:])
    n1 = len(G2[0,:])
    m2 = len(G1[:,0])
    n2 = len(G2[:,0])
    asub1 = np.array([[i, j] for i in range(m1) for j in range(m2)])
    asub2 = np.array([[i, j] for i in range(n1) for j in range(n2)])
    huge = np.array([np.array([asub1[i], asub2[j]]) for i in range(m1*m2) for j in range(n1*n2)])
    return huge

def check_graph(index1, index2, graph):
    #if G1[index1][index2] != 0 or index1 == index2:
    if G1[index1][index2] != 0:
    #if G1[index1][index2] != 0 and index1 < index2:
        return True
    else:
        return False

def get_value(index1, index2, G1, G2):
    #print("indices:", index1, index2)
    norm1 = sum([sum(row) for row in G1])
    norm2 = sum([sum(row) for row in G2])
    n1 = sum(G1[index1,:])
    n2 = sum(G2[index2,:])
    value = n1/norm1*n2/norm2
    #value = (1/(n1*n2))
    return value

def parse_huge(G1, G2, huge):
    A = np.zeros(len(huge))
    for i,val in enumerate(huge):
        link1=False
        link2=False
        if i >=25 and i<=49:
            print(i)
        link1 = check_graph(val[0][0], val[0][1], G1)
        link2 = check_graph(val[1][0], val[1][1], G2)
        if link1 and link2:
            value = get_value(val[0][1], val[1][1], G1, G2)
            if i >=25 and i<=49:
                print(val[0], val[1],value)
            A[i] = value
        #if val[0][0] == val[0][1] and val[1][0] == val[1][1] and val[0][0] == val[1][0]:
        #    value = 1.0
        #    A[i] = value
    m1 = len(G1[0,:])
    n1 = len(G2[0,:])
    m2 = len(G1[:,0])
    n2 = len(G2[:,0])
    A = A.reshape((m1*m2),(n1*n2))
    return A

def combine_graphs(G1, G2):
    Gx = sum(sum(G1))
    Gy = sum(sum(G2))
    for row in G1:
        row/=sum(row)
    for row in G2:
        row/=sum(row)
    #g1 = np.array([sum(row) for row in G1]).flatten()
    #g1/=sum(g1)
    #g1 = np.array([g1])
    #g2 = np.array([sum(row) for row in G2]).flatten()
    #g2/=sum(g2)
    #g2 = np.array([g2]).T
    G1 = np.array([G1.flatten()])
    G2 = G2.flatten()
    G2 = np.array([G2]).T
    A = np.matmul(G2, G1)
    #print(g1, g2)
    return A

G1 = build_graph()
G2 = build_graph()
A = combine_graphs(G1, G2)
#huge = huge_matrix(G1, G2)
#A = parse_huge(G1, G2, huge)

#for i in range(len(A)):
#print(A[1,:])
#print(sum(A[1,:]))

B = np.ones((25,1))

for i in range(40):
    B = np.matmul(A,B)
    B/=np.max(B)
B_new = np.matmul(A,B).flatten()
B = B.flatten()
T = np.dot(B,B_new)
denom = np.dot(B,B)
for row in B.reshape(5,5):
    print("{:.2f} {:.2f} {:.2f} {:.2f} {:.2f}".format(row[0],row[1],row[2],row[3],row[4]))

print(T/denom)
#row_dict={0:'a',1:'b',2:'c',3:'d',4:'e'}
#column_dict={0:'a\'',1:'b\'',2:'c\'',3:'d\'',4:'e\''}
#connections={'a':1,'b':2,'c':3,'d':1,'e':1}
