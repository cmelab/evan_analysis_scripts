'''
This file is meant to calculate the center of mass, in- and out-of plane 
orientations for molecules and write the data to a file.
'''
#Import the module files
import sys
import numpy as np
import mdtraj as md
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import helper_functions.e_helper as cg
from cme_utils.manip.pbc import plain_pbc as pbc
import re
import os
import pickle

#"""Check for hoomdxml file and create if necessary."""
#cg.check_hoomdxml()

class Molecule():
    def __init__(self, TRAJ_FILE = 'traj.dcd', TOP_FILE = 'restart.hoomdxml', final = True, frame = 0):
        self.t = md.load(TRAJ_FILE, top = TOP_FILE)
        self.final = final
        self.frame = int(frame)

    def average_position(self, p1, p2, p3):
        """ This function takes the points, calculates vectors so the periodic boundries can be tested then will average and append the cm list."""
        v1 = pbc(p3 - p1, self.axes)
        v2 = pbc(p3 - p2, self.axes)
        p1 = p3 - v1
        p2 = p3 - v2
        average = (p1+p2+p3) / 3.
        return average

    def cross_product(self, p1, p2, p3):
        """Calculates the cross product from three members of a ring"""
        v1 = pbc(p3 - p1, self.axes)
        v2 = pbc(p3 - p2, self.axes)
        cp = np.cross(v1, v2)
        cp /= np.linalg.norm(cp)
        return cp

    def in_plane(self, mean, p1):
        '''Calculate the in-plane orientation.'''
        vec = pbc(mean - p1, self.axes)
        vec /= np.linalg.norm(vec)
        return vec

    def identify_molecules(self, apm, i1, i2, i3):
        '''Identifies each molecule via the given indices.'''
        apm = int(apm)
        i1 = int(i1)
        i2 = int(i2)
        i3 = int(i3)
        if self.final == True:
            frames = (self.t.n_frames-1)#gets the final frame
        else:
            frames = self.frame
        #print("Your atom indicies are: {}, {} and {} with {} atoms per molecule".format(i1, i2, i3, apm))
        #Calculate the number of molecules based off of the total number
        #of atoms and atoms per molecule.
        n_mol = int(self.t.n_atoms/apm)
        #Get the length of the unit cell edges.
        self.axes = self.t.unitcell_lengths[self.t.n_frames-1]

        #Setting up the empty lists.
        #Lists to place points used for the averages in cm_list.
        self.cm_list = []#An empty list for the average positions/center of mass
        self.cp_list = [] #for the cross product.
        self.in_orient = [] #for the in-plane orientations

        p1_list = self.t.xyz[frames,i1:-1:apm,:]
        p2_list = self.t.xyz[frames,i2:-1:apm,:]
        p3_list = self.t.xyz[frames,i3:-1:apm,:]
        for i in range(n_mol):
            ave = self.average_position(p1_list[i], p2_list[i], p3_list[i])
            self.cm_list.append(ave)
            self.cp_list.append(self.cross_product(p1_list[i], p2_list[i], p3_list[i]))
            self.in_orient.append(self.in_plane(ave, p3_list[i]))

    def identify_neighbors(self, n_mol):
        COM, CPV, OV = np.array(self.cm_list), np.array(self.cp_list), np.array(self.in_orient)
        psi_list = [ [] for i in range(n_mol) ] #List for the neighbors.
        dot_list = []
        #Build the neighborlist
        for i in range(0,len(COM)-1):
            for j in range(i+1,len(COM)):
                a = COM[i,:]
                b = COM[j,:]
                vec = pbc(b - a, self.axes)
                d = np.linalg.norm(vec)
                if  d < 0.434:
                    cp1 = CPV[i,:]
                    cp2 = CPV[j,:]
                    dot = abs(np.dot(cp1, cp2))
                    if dot > 0.9745: 
                        psi_list[i].append(j)
                        psi_list[j].append(i)
                if d <= 0.16:
                    o1 = OV[i,:]
                    o2 = OV[j,:]
                    dot1 = abs(np.dot(o1, o2))
                    dot_list.append(dot1)
        self.calc_order(self.cluster(psi_list))
        if len(dot_list) <= 5 or self.order <= 0.2:
            self.dot_mean = 0.0
        else:
            self.dot_mean = np.mean(np.array(dot_list))
        #print(self.dot_mean)

    def write_as_array(self, filename):
        COM = np.array(self.cm_list)
        CP = np.array(self.cp_list)
        OV = np.array(self.in_orient)
        total = np.concatenate((COM, CP, OV), axis = 1)
        total.dump(filename)

    def calc_order(self, c_list):
        """Determine the overall order of the system"""
        order_list = []
        for i in range(0, np.amax(c_list)+1):
            """Print cluster sizes and cluster members."""
            order = float(c_list.count(i))/float(len(c_list)) 
            if order >= 0.2:#Chosen from a dummy system.
                order_list.append(order)
        self.order = np.sum(order_list)
        #print(self.order)

    def cluster(self, lst):
        """Create a cluster list from neighbor list."""
        c_list=[i for i in range(len(lst))]
        for i in range(len(c_list)):
            c_list = self.update_neighbors(i,c_list,lst)
        return c_list

    def update_neighbors(self, particle, cluster_list, neighbor_list):
        """Recursive function to convert neighborlist into cluster list"""
        for n in neighbor_list[particle]:
            if cluster_list[n]>cluster_list[particle]:
                cluster_list[n] = cluster_list[particle]
                cluster_list = self.update_neighbors(n,cluster_list,neighbor_list)
            elif cluster_list[n] < cluster_list[particle]: 
                cluster_list[particle] = cluster_list[n]
                cluster_list = self.update_neighbors(particle,cluster_list,neighbor_list)
        return cluster_list

    def plot_data(self):
        fig = plt.figure()
        ax = fig.add_subplot(111, projection = '3d')
        new_cm = 10.* np.array(self.cm_list)
        a = np.concatenate((new_cm, self.cp_list), axis = 1)
        b = np.concatenate((new_cm, self.in_orient), axis = 1)
        X, Y, Z, U, V, W = zip(*a)
        x, y, z, u, v, w = zip(*b)
        ax.quiver(X, Y, Z, U, V, W)
        ax.quiver(x, y, z, u, v, w, color = 'r')
        plt.show()

    def write_data(self):
        a = np.concatenate((self.cm_list, self.cp_list, self.in_orient), axis = 1)
        """Get temperature and phi"""
        path = os.getcwd().split('/')
        Temp = path[-1]
        phi = path[-2]
        phi = float(re.findall(r'phi(.*)', phi)[0])
        Temp = float(re.findall(r'T(.*)', Temp)[0])
        np.savetxt('/Users/evanmiller/Projects/Runs/pe_pt_phase/rigid_perylothiophene/text_files/'+'struc-phi-{:1.2f}-T-{:04.1f}.txt'.format(phi, Temp), a, fmt='%.4f')
        #np.savetxt('struc-phi-{:1.2f}-T-{:04.1f}.txt'.format(phi, Temp), a, fmt='%.4f')

if __name__ == "__main__":
    #Get values from trajectory and topologies
    TRAJ_FILE = "traj.dcd" #Trajectory file
    TOP_FILE = "restart.hoomdxml" #Topology file
    t = md.load(TRAJ_FILE, top = TOP_FILE)

    mol = Molecule()
    mol.identify_molecules(32, 4, 8, 11)
    mol.identify_neighbors(200)
    mol.write_as_array('position.dat')
    #mol.plot_data()
    #mol.write_data()
