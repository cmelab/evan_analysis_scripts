"""
Script to calculate the timesteps necessary to reach equilibrium
"""

import sys
import numpy as np
import mdtraj as md
import matplotlib.pyplot as plt

"""Takes input: python script restart traj log"""

Top = sys.argv[1]
Traj = sys.argv[2]
log = np.loadtxt(sys.argv[3], skiprows = 1)
t = md.load(Traj, top = Top)

Last_Frame = float((t.n_frames*1e6))
First_Frame = float(0)
Steps = float(5*1e5)

timesteps = log[:,0]
potential = log[:,6]

stack = np.vstack((timesteps,potential))

i = np.arange(First_Frame, Last_Frame, Steps)[:, None]
selections = np.logical_and(timesteps >=i, timesteps < i+Steps)[None]

new, selections = np.broadcast_arrays(stack[:, None], selections)
new = new.astype(float)
new[~selections] = np.nan

new = np.nanmean(new, axis=-1)

cut = np.mean(new[1,:][-20:-2])

for i in range(3,len(new[1,:])):
    if new[1,:][i] <= cut:
        Eq = new[0,:][i]
        print("%g" %int((new[0,:][i]/1e5)))
        print(Eq)
        break

plt.plot(new[0,:],new[1,:])
plt.axvspan(Eq, Eq, color = 'r', alpha = 0.3)
plt.savefig("potential.pdf")
#plt.show()
