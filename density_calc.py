import numpy as np
import mdtraj as md
import sys

#TOP = sys.argv[1]
#TRAJ = sys.argv[2]
TOP = "restart.hoomdxml"
TRAJ = "traj.dcd"
t = md.load(TRAJ, top=TOP)
D = 3.2000

#Assumes the Volume is the same for every timestep.
a = t.unitcell_lengths[0,0]*D*(1e-7)
b = t.unitcell_lengths[0,1]*D*(1e-7)
c = t.unitcell_lengths[0,2]*D*(1e-7)
alpha = np.deg2rad(t.unitcell_angles[0,0])
beta = np.deg2rad(t.unitcell_angles[0,1])
gamma = np.deg2rad(t.unitcell_angles[0,2])
print("You have lattice parameters: a = {:.2e} cm, b = {:.2e} cm, c ={:.2e} cm, alpha = {:.2f}, beta = {:.2f} and gamma = {:.2f}.".format(a,b,c,alpha,beta,gamma))
Volume = a*b*c*np.sqrt(1-np.cos(alpha)**2-np.cos(beta)**2-np.cos(gamma)**2+1*np.cos(alpha)*np.cos(beta)*np.cos(gamma))
print(Volume)

num_atoms = float(t.n_atoms)
print("Your simulation contains: {} particles.".format(num_atoms))
mol = 6.02e23

##For Perylene
atom = 'Perylothiophene'
if atom =='AA-Perylene':
    Mass = num_atoms/28.*252.32/mol
if atom =='UA-Perylene':
    Mass = num_atoms/20. * 252.32/mol # + num_atoms/20.*8.*mp
##For Perylothiophene
if atom =='Perylothiophene':
    Mass = num_atoms/21.*242.32/mol

Density = Mass/Volume

print("The density of the system is {:.2f} g/cm**3".format(Density))

#Actual Density: 1.286g/cm**3
#The loop to get Density for the different phis
"""
for i in phi*; do cd $i; echo $i $(python /home/evan/Projects/evan_analysis_scripts/density_calc.py *-T3.0/restart.hoomdxml *-T3.0/traj.dcd); cd ..; done
"""

#CH M=13, eps = 0.1100, D = 3.7500
#S M =320.060, eps = 0.2500, D = 3.5500
