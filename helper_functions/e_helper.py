import os.path
import shutil

"""Creates a tcl script for each identified cluster."""
def TCLscript(neighbors, clusters):
    tclLinesToWrite = ['mol delrep 0 0\n']
    tclLinesToWrite += ['pbc wrap -center origin\n'] 
    tclLinesToWrite += ['pbc box -color black -center origin -width 4\n']
    tclLinesToWrite += ['display resetview\n']
    tclLinesToWrite += ['color change rgb 9 1.0 0.29 0.5\n']
    tclLinesToWrite += ['color change rgb 16 0.25 0.25 0.25\n']
    count = 0
    for i in range(len(neighbors)):
        inclust = ""
        for j,c in enumerate(clusters):
            if c==i:
                inclust +=str(j)+" "
        if inclust !="":
            tclLinesToWrite +=['mol color ColorID '+str(count)+'\n']
            #tclLinesToWrite +=['mol representation VDW 1.0 8.0\n']
            tclLinesToWrite +=['mol representation QuickSurf 0.9 1.1 0.5 1.0\n']
            tclLinesToWrite += ['mol selection resid '+str(inclust)+'\n']
            tclLinesToWrite += ['mol material Transparent\n']
            tclLinesToWrite += ['mol addrep 0\n']
            count += 1
            if count >= 32:
                count -= 32
    tclFileName = './'+'c_color.tcl'
    with open(tclFileName, 'w+') as tclFile:
        tclFile.writelines(tclLinesToWrite)
    print('Tcl file written to', tclFileName)

"""Creates a tcl script for each identified cluster.
Works for the DPB molecule"""
def DPBTCLscript(neighbors, clusters):
    tclLinesToWrite = ['mol delrep 0 0\n']
    tclLinesToWrite += ['pbc wrap -center origin\n'] 
    tclLinesToWrite += ['pbc box -color black -center origin -width 4\n']
    tclLinesToWrite += ['display resetview\n']
    tclLinesToWrite += ['color change rgb 9 1.0 0.29 0.5\n']
    tclLinesToWrite += ['color change rgb 16 0.25 0.25 0.25\n']
    count = 0
    for i in range(len(neighbors)):
        inclust = ""
        for j,c in enumerate(clusters):
            if c==i:
                for k in range(j*5, j*5+5):
                    inclust +=str(k)+" "
        if inclust !="":
            tclLinesToWrite +=['mol color ColorID '+str(count)+'\n']
            tclLinesToWrite +=['mol representation VDW 1.0 8.0\n']
            tclLinesToWrite += ['mol selection resid '+str(inclust)+'\n']
            if len(inclust) >= 12:
                tclLinesToWrite += ['mol material AOEdgy\n']
            else:
                tclLinesToWrite += ['mol material Transparent\n']
            tclLinesToWrite += ['mol addrep 0\n']
            count += 1
            if count >= 32:
                count -= 32
    tclFileName = './'+'c_color.tcl'
    with open(tclFileName, 'w+') as tclFile:
        tclFile.writelines(tclLinesToWrite)
    print('Tcl file written to', tclFileName)

"""Checks to make sure there is a hoomdxml file 
and creates one if necessary."""
def check_hoomdxml():
    if os.path.isfile('restart.xml') == True:
        shutil.copy('restart.xml', 'restart.hoomdxml')
    if os.path.isfile('restart.xml') == False:
        if os.path.isfile('init.xml') == True:
            shutil.copy('init.xml', 'restart.hoomdxml')
        else:
            sys.exit("No xml file found!")

def update_neighbors(particle, cluster_list, neighbor_list):
    """Recursive function to convert neighborlist into cluster list"""
    for n in neighbor_list[particle]:
        if cluster_list[n]>cluster_list[particle]:
            cluster_list[n] = cluster_list[particle]
            cluster_list = update_neighbors(n,cluster_list,neighbor_list)
        elif cluster_list[n] < cluster_list[particle]: 
            cluster_list[particle] = cluster_list[n]
            cluster_list = update_neighbors(particle,cluster_list,neighbor_list)
    return cluster_list

