import numpy as np

def get_box_from_xml(xml):
    box = np.array([[xml['lx'], xml['ly'], xml['lz']]])
    return box

def pbc(positions, box):
    """
    Call the pbc code from cme_utils
    Requires:
        positions - array
        box - array
    """
    from cme_utils.manip import pbc as cmepbc
    return cmepbc.plain_pbc(positions, box[0])

def xml_filter(xml, apm, types):
    """
    Utilizes the Matty helper to parse the xml data
    and filter the topology data and return 
    the needed clustering information.
    Requires:
        xml - list of toplogy data
    Returns:
        t - class containing topology arrays.
    """
    class xml_only(object):
        xyz = []
        n_frames = 0
        unitcell_lengths = []
        n_residues = 0
        n_atoms = 0
    t = xml_only()
    #['CA', 'S', 'S1', 'S2'] for p3ht
    indices = [i for i, x in enumerate(xml['type']) if x in types]
    t.xyz = np.array([[xml['position'][x] for x in indices]])
    t.n_frames = 1
    t.n_residues = len(t.xyz[0,:,0])//apm
    t.unitcell_lengths = np.array([[xml['lx'], xml['ly'], xml['lz']]])
    t.n_atoms = len(t.xyz[0,:,0])
    return t

def get_centers(positions, box, apm, indices = [0,1,3]):
    """
    Parses the xyz data for the simulations to get atom positions
    and calculates the average positions of three points.
    Requires:
        positions - array of xyz coordinates
        box - array of unitcell
        apm - int, atoms per molecule
        indices - list of 3 atom indices overwhich to average
    Returns:
        averages - array the average of the 3 atom indices for the 
            molecules
        orientations - array of normal vectors describing the
            plane of the 3 atom indices
    """
    points = np.hstack((positions[indices[0]::apm,:],
        positions[indices[1]::apm,:],
        positions[indices[2]::apm,:]))
    averages = []
    orientations = []
    for point in points:
        center, normal = average_position(point, box)
        averages.append(center)
        orientations.append(normal)
    return np.array(averages), np.array(orientations)

def average_position(array, box):
    """ This function takes the points, calculates vectors so the periodic boundries can be tested then will average and append the cm list."""
    p1 = array[0:3]
    p2 = array[3:6]
    p3 = array[6:9]

    v1 = pbc((p1 - p3), box)
    v2 = pbc((p2 - p3), box)

    p1new = p3 + v1
    p2new = p3 + v2

    average = (p1new+p2new+p3) / 3.

    cross = np.cross(v1,v2)
    cross = cross/np.linalg.norm(cross)

    return average, cross

def check_distance(positions, i, j, dcut, orientations, ocut, box):
    """
    Function called by build_neighbors to check
    all the centers of mass between two molecules.
    If the required number of centers are within
    the cut off returns the molecule indicies to be
    added to the neighbor list.
    Requires:
        centers - array of positions
        i - indice for position #1
        j - indice for position #2
        dcut - distance cut off 
        (dcut may need to be multiplied by 10 if in nm)
        orientations - array of molecule normal vectors or None
        ocut - float for the orientation cut off or None
    Returns:
        i, j - the indices for two neighboring chains
    """
    com1 = positions[i]
    com2 = positions[j]
    d = np.linalg.norm(pbc(com1-com2, box))
    if ocut != None:
        orient1 = orientations[i]
        orient2 = orientations[j]
        o = np.dot(orient1, orient2)
    if d <= dcut and (abs(o) >= ocut or ocut == None):
        return [i,j]

def build_neighbors(positions, dcut, n_mol, box, orientations=None, ocut=None):
    """
    Uses a distance cut off to check the distances
    between two thiophene molecules in different 
    chains to determine if they are neighbors.
    Requires:
        positions - array of molecule centers
        dcut - distance cut off
        n_mol - integer for number of molecules in system
    Optional:
        orientations - array of molecule normal vectors or None
        ocut - float for the orientation cut off or None
    Returns: 
        n_list - neighbor list
    """
    print("Building neighbor list.")
    print("Your cut off is {:.2f} simga.".format(dcut))
    n_list = [ [] for i in range(n_mol) ]
    neighbors_found = []
    check_list = [ [j for j in range(i+1, n_mol)] for i in range(n_mol)]
    for i,val in enumerate(check_list):
        for j in val:
            neighbors_found.append(check_distance(positions, i, j, dcut, orientations, ocut, box))
    neighbors_found = [x for x in neighbors_found if x is not None]
    for x in neighbors_found:
        n_list[x[0]].append(x[1])
        n_list[x[1]].append(x[0])
    print("Done.")
    return n_list

def neighbors_with_freud(positions, box, cut, orientations=None, ocut=None):
    n_list = [ [] for i in range(positions.shape[0]) ]
    import freud
    fbox = freud.box.Box(Lx = box[0][0],
            Ly = box[0][1],
            Lz = box[0][2])
    lc = freud.locality.LinkCell(fbox, cut)
    lc.computeCellList(fbox, positions)
    for i in range(positions.shape[0]):
        cell = lc.getCell(positions[i])
        cellNeighbors = lc.getCellNeighbors(cell)
        for neighborCell in cellNeighbors:
            for neighbor in lc.itercell(neighborCell):
                if i != neighbor:
                    n_list[i].append(neighbor)
    for i, val in enumerate(n_list):
        remove_list = []
        for j in val:
            com1 = positions[i]
            com2 = positions[j]
            d = np.linalg.norm(pbc(com1-com2, box))
            if d > cut:
                remove_list.append(j)
            if len(orientations) > 0:
                o1 = orientations[i]
                o2 = orientations[j]
                o = abs(np.dot(o1, o2))
                if o < ocut and j not in remove_list:
                    remove_list.append(j)
        for pop in remove_list:
            n_list[i].remove(pop)
            #Will need to check the reverse remove.
            n_list[pop].remove(i)
    return n_list

def make_clusters(n_list):
    """
    Function to call for the creation
    of turning the neighbor list into a 
    cluster list.
    Requires:
        n_list - neighbor list
    Returns:
        c_list - cluster list
    """
    import sys
    sys.setrecursionlimit(int(5e4))
    print("Creating Clusters.")
    c_list = [i for i in range(len(n_list))]
    for i in range(len(c_list)):
        n_list, c_list = update_neighbors(i, c_list, n_list)
    print("Done.")
    return c_list

def update_neighbors(particle, cluster_list, neighbor_list):
    """Recursive function to convert neighborlist into cluster list"""
    for n in neighbor_list[particle]:
        if cluster_list[n]>cluster_list[particle]:
            cluster_list[n] = cluster_list[particle]
            neighbor_list, cluster_list = update_neighbors(n,cluster_list,neighbor_list)
        elif cluster_list[n] < cluster_list[particle]: 
            cluster_list[particle] = cluster_list[n]
            neighbor_list, cluster_list = update_neighbors(particle,cluster_list,neighbor_list)
    return neighbor_list, cluster_list

def print_clusters(nlist, clist):
    """
    Function to print the molecule clusters.
    Print is within a function so that it can 
    be more easily commented out.
    Requires:
        nlist - neighbor list
        clist - cluster list
    """
    print("Your clusters are:")
    for i in range(len(nlist)):
        inclust = ""
        for j,c in enumerate(clist):
            if c==i:
                inclust+=str(j)+" "
        if inclust !="":
            print(i,inclust)
    print(len(set(clist)), "clusters")

def get_statepoint(path):
    """
    Uses the path to extract the state point
    information.
    Requires: 
        path - path to statepoint directory
    Returns:
        phi - packing fraction
        Temp - Temperature
        e_factor - simulation stickiness
    Note: These must be specified in the simation
    directory name for this function to be 
    used.
    """
    import re
    path = path.split('/')[-2]
    print(path)
    state_point_dict = {'T':0.0,
            'phi':0.0,
            'e_factor':0.0}
    try:
        Temp = re.findall(r'-T(.*)', path)[0]
        state_point_dict['T'] = float(Temp)
    except:
        print("Failed to get T.")
        pass
    try:
        phi = re.findall(r'phi(.*)', path)[0]
        phi = phi.split('-')[0]
        state_point_dict['phi'] = float(phi)
    except:
        print("Failed to get phi.")
        pass
    try:
        e_factor = re.findall(r'e_factor-(.*)', path)[0]
        e_factor = e_factor.split('-')[0]
        state_point_dict['e_factor'] = float(e_factor)
    except:
        print("Failed to get e factor.")
        pass
    return state_point_dict

if __name__ == "__main__":
    print("Hello, this script contains helper functions.")
