import numpy as np
from operator import itemgetter
import matplotlib.pyplot as plt
from multiprocessing import Pool

def build_lattice(x, y):
    lst = []
    starting = np.array([[0.,0.],[1./2, np.sqrt(3.)/2]])
    repeat = np.array([1.0, np.sqrt(3.)])
    for i in range(1,x):
        for j in range(1,y):
            for point in starting:
                a = point[0]+i*repeat[0]
                b =  point[1]+j*repeat[1]
                pos = np.array([a,b])
                lst.append([a,b, int(a**2.+b**2.)])
    lst = remove_redundant(lst)
    return np.array(lst)

def remove_redundant(lst):
    lst = [[i[0], i[1], i[2]] for i in lst if int(i[2]) >=0]
    lst = sorted(lst, key=itemgetter(2))
    pop_list = []
    #for i in range(len(lst)-1):
    #    if lst[i][2] == lst[i+1][2]:
    #        pop_list.append(i)
    #for i in reversed(pop_list):
    #    lst.pop(i)
    return lst

def calc_theta(position):
    theta = np.arctan(position[1]/position[0])*180/np.pi
    return theta

def interference(positions, lengths, M, tolerance = 0.3):
    angles = []
    sites = []
    sites_angles = [[] for i in range(61)]
    for i, val in enumerate(lengths):
        single = False
    #    while not single:
        for integer in range(1,M):
            dist = np.sqrt(val)*integer
            if dist%1 <= tolerance or abs(dist%1-1) <= tolerance:
                theta = calc_theta(positions[i])
                to_print = "The sqrt({}) vector moves to the {:.1f} hole ".format(int(val), int(dist))
                to_print += "along the x-axis after {} duplications ".format(integer)
                to_print += "and a rotation of {:.2f} degrees.".format(theta)
                #print(to_print)
                if dist <=40.5 and theta <=60.5:
                    sites_angles[int(theta)].append(int(dist))
                    sites.append(dist)
                    angles.append(theta)
    #                single = True
#                    break
    return angles, sites, np.array(sites_angles)

def theta_histogram(angles):
    plt.hist(angles, bins=np.arange(0 , 30, 1))
    plt.xlabel(r"Angle rotated ($^{\circ}$)")
    plt.ylabel("# of ways empty")
    plt.show()

def site_histogram(angles):
    plt.hist(angles, bins=np.arange(0 , 30, 1))
    plt.xlabel("X-axis site")
    plt.ylabel("# of ways empty")
    plt.show()

def scatter(lst):
    print("Creating Scatter Plot.")
    colors = 100*['r', 'b', 'g', 'k', 'c', 'y']
    markers = 100*['v', '*', 'o', '^', 's', 'X']
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i, val in enumerate(lst):
        for site in val:
            if i >= 30:
                ic = abs(i-59)
            else: 
                ic = i
            plt.scatter(i, site, c=colors[ic], marker=markers[ic])
    plt.grid()
    plt.xlabel(r"$\theta (^{\circ})$")
    plt.xticks(np.arange(0,61, 2), fontsize = 12)
    plt.ylabel("Site away from origin")
    for label in ax.xaxis.get_ticklabels()[::2]:
            label.set_visible(False)
    #ax.set_xticklabels(np.arange(0,60,4))
    plt.savefig("theta-sites.pdf")

def bigger_lattice(x, y):
    #lst = []
    starting = np.array([[0.,0.],[1./2, np.sqrt(3.)/2]])
    repeat = np.array([1.0, np.sqrt(3.)])
    lst = [[point[0]+i*repeat[0], point[1]+j*repeat[1]] for point in starting for i in range(-x, x) for j in range(-y, y)]
    return np.array(lst)

def rotate(theta, array):
    theta = np.deg2rad(theta)
    rotator = [[np.cos(theta), -np.sin(theta)],[np.sin(theta), np.cos(theta)]]
    rotated = np.matmul(array, rotator)
    rotated += np.array([0.0, np.sqrt(3)/2])
    return rotated

def plot_lattice(lists, theta1, theta2=0, theta3=0):
    plt.close()
    for lst in lists[:-1]:
        plt.scatter(lst[:,0], lst[:,1], c = 'g', s = 2, alpha = 0.1)
    for lst in lists[-1:]:
        plt.scatter(lst[:,0], lst[:,1], c = 'b', s = 12)
    plt.ylim([-40, 40])
    plt.xlim([-40, 40])
    plt.xticks([])
    plt.yticks([])
#    plt.title(r"$\theta$={}-$\theta$={}-$\theta$={}".format(theta1, theta2, theta3))
    plt.savefig("pores/theta1_"+str(theta1).zfill(2)+"-theta2_"+str(theta2).zfill(2)+"-theta3_"+str(theta3).zfill(2)+".png")

def overlap(array1, array2, tolerance = 0.3):
    from scipy.spatial import distance
    dists = distance.cdist(array1, array2, 'euclidean')
    pores = [array1[i] for i, val in enumerate(dists) if np.min(val) < tolerance]
    return np.array(pores)

def second_layer(theta2):
    rotated2 = rotate(theta2, lattice)
    pores2 = overlap(pores1, rotated2)
    one_more = True
    if one_more:
        for theta3 in np.arange(8, 9, 1):
        #for theta3 in np.arange(theta2, 10, 1):
            third_layer(theta3, theta2, rotated2, pores2)
    else:
        plot_lattice(np.array([lattice, rotated1, rotated2, pores2]), theta1, theta2)

def third_layer(theta3, theta2, rotated2, pores2):
    rotated3 = rotate(theta3, lattice)
    pores3 = overlap(pores2, rotated3)
    plot_lattice(np.array([lattice, rotated1, rotated2, rotated3, pores3]), theta1, theta2=theta2, theta3=theta3)

if __name__== "__main__":
    #points = build_lattice(30,30)
    #angles, sites, sites_angles = interference(points[:,:2], points[:,2], 60)
    #theta_histogram(angles)
    #site_histogram(sites)
    #scatter(sites_angles)
    lattice = bigger_lattice(50,50)
    for theta1 in range(2, 6, 1):
        rotated1 = rotate(theta1, lattice)
        pores1 = overlap(lattice, rotated1)
        additional_layer = True
        if additional_layer:
            secondrotation = np.arange(theta1, 6, 1)
            with Pool() as p:
                p.map(second_layer, secondrotation)
        else:
            plot_lattice(np.array([lattice, rotated1, pores1]), theta1, 0)
            #rotated2 = rotate(theta2, lattice)
            #pores2 = overlap(pores1, rotated2)
            #plot_lattice(np.array([lattice, rotated1, rotated2, pores2]), theta1, theta2)
