import numpy as np
import math
import helper_functions.matty_helper as mh
import sys

def rotate(array, angle):
    lst = []
    for i in range(len(array)):
        point = array[i]
        x = math.cos(angle)*point[0] - point[1] * math.sin(angle)
        y = math.sin(angle)* point[0] + point[1] * math.cos(angle)
        z = point[2]
        lst.append([x, y, z])
    print "DONE ROTATING"
    return lst

def check(bottom, middle, middle_rot):
    for i,rowi in enumerate(bottom):
        print i
        for k,rowk in enumerate(middle):
            xb = rowi[0]
            yb = rowi[1]
            dxbt = abs(xb-rowk[0])
            dybt = abs(yb-rowk[1])
            if dxbt < 0.05 and dybt < 0.05:
                xml['type'][i] = 'O'
                xml['type'][k+cut] = 'O'
                xml['diameter'][i] = 2.0
                xml['diameter'][k+cut] = 2.0

xml = mh.loadMorphologyXML('stacked.xml')

middle_rot = int(sys.argv[1])

cut = len(xml['position'])/2

positions_bottom = np.array(xml['position'][:cut])
positions_middle = np.array(xml['position'][cut:])

mid_theta = float(middle_rot)*math.pi/180.

middle_move = rotate(positions_middle, mid_theta)

positions_bottom.tolist()

positions = []
for i in positions_bottom:
    positions.append(i)
for i in middle_move:
    positions.append(i)
xml['position'] = positions

check(positions_bottom, middle_move, middle_rot)

mh.writeMorphologyXML(xml, "{:02d}".format(middle_rot)+"rot.xml")
