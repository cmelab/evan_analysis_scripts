import numpy as np
import math
import helper_functions.matty_helper as mh

xml = mh.loadMorphologyXML("hex.xml")

def translate_x(vec):
    lst = []
    for i in range(-80,80):
        lst.append([vec[0][0]+float(i), vec[0][1], 0.0])
    return lst

def translate_y(array):
    lst = []
    for j in array:
        for i in np.arange(-80, 80, 2):
            lst.append([j[0], j[1]+ i*math.sqrt(3)/2., 0.0])
            print j[0], j[1]+ i*math.sqrt(3)/2., 0.0
    return lst

def translate_z(array):
    lst = []
    for j in array:
        for i in np.arange(0, 1, 1):
            lst.append([j[0], j[1], i * 1.3])
    return lst

def printlist(lis):
    for i in range(len(lis)):
        print lis[i][0], lis[i][1], lis[i][2]

p1 = [[0., 0., 0.]]
p2 = [[-.5,math.sqrt(3)/2.,0.]]

px1 = translate_x(p1)
px2 = translate_x(p2)
print len(px1), len(px2)
px = px1+px2
print px
print len(px)

py = translate_y(px)

xml['position'] = py
xml['natoms'] = len(xml['position'])
xml['type'] = len(xml['position'])*['C']
xml['diameter'] = len(xml['position'])*['0.7']

mh.writeMorphologyXML(xml, "hex.xml")
