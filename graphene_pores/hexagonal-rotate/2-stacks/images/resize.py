import os
import subprocess 

def resize_files(fname):
        # ffmpeg -i a.jpg -vf scale=640:480 a.png   
        out_fname = fname.replace('jpg','png')
        #print out_fname
        subprocess.call(['ffmpeg', '-i', fname, '-vf', 'scale=640:480', out_fname ])    

def listFiles(dir):
        basedir = dir
        subdirlist = []
        for item in os.listdir(dir):
                fullpath = os.path.join(basedir,item)
                if os.path.isdir(fullpath):
                    print  'dir item=',fullpath
                    subdirlist.append(fullpath)
                else:
                    print 'file item=',fullpath
                    if item.endswith('.jpg'):
                        resize_files(fullpath)
        for subdir in subdirlist:
                    listFiles(subdir)

if __name__ == '__main__':
    listFiles('.');
