import numpy as np
import math
import helper_functions.matty_helper as mh
import sys

def rotate(array, angle):
    lst = []
    for i in range(len(array)):
        point = array[i]
        x = math.cos(angle)*point[0] - point[1] * math.sin(angle)
        y = math.sin(angle)* point[0] + point[1] * math.cos(angle)
        z = point[2]
        lst.append([x, y, z])
    return lst

def check(bottom, middle, top, middle_rot, top_rot):
    for i,rowi in enumerate(bottom):
        for k,rowk in enumerate(top):
            xb = rowi[0]
            yb = rowi[1]
            dxbt = abs(xb-rowk[0])
            dybt = abs(yb-rowk[1])
            if dxbt < 0.1 and dybt < 0.2:
                check_mid(i, k, xb, yb, middle)

def check_mid(i, k, xb, yb, middle):
    for j,rowj in enumerate(middle):
        dxbm = abs(xb-rowj[0])
        dybm = abs(yb-rowj[1])
        if dxbm < 0.1 and dybm < 0.2:
            print rowj[0], rowj[1]
            xml['type'][i] = 'O'
            xml['type'][j+cut] = 'O'
            xml['type'][k+2*cut] = 'O'
            xml['diameter'][i] = 1.0
            xml['diameter'][j+cut] = 1.0
            xml['diameter'][k+2*cut] = 1.0

xml = mh.loadMorphologyXML('stacked.xml')

middle_rot = int(sys.argv[1])
top_rot = int(sys.argv[2])

cut = len(xml['position'])/3

positions_bottom = np.array(xml['position'][:cut])
positions_middle = np.array(xml['position'][cut:2*cut])
positions_top = np.array(xml['position'][2*cut:])

mid_theta = float(middle_rot)*math.pi/180.
top_theta = float(top_rot)*math.pi/180.

middle_move = rotate(positions_middle, mid_theta)

top_move = rotate(positions_top, top_theta)

positions_bottom.tolist()

positions = []
for i in positions_bottom:
    positions.append(i)
for i in middle_move:
    positions.append(i)
for i in top_move:
    positions.append(i)
xml['position'] = positions

check(positions_bottom, middle_move, top_move, middle_rot, top_rot)

mh.writeMorphologyXML(xml, "./10-percent/"+"{:02d}{:02d}".format(middle_rot, top_rot)+"rot.xml")
