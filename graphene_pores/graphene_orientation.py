"""The purpose of this script is to calculate 
the angles of the graphene sheets relative to the others."""

#Import the module files
import sys
import numpy as np
import mdtraj as md
from mpl_toolkits.mplot3d import Axes3D
import helper_functions.e_helper as cg
import math

def unit_vector(vec):
    """Will produce a normalized vector of the input vector."""
    vec /=np.linalg.norm(vec)
    return vec

def get_angle(vec):
    """Calculates the angle between one plane and 
    the one above it within the array of vectors."""
    lst = []
    for i in range(0, len(vec)-1):
        v1 = vec[i]
        v2 = vec[i+1]
        theta = np.arccos(np.clip(np.dot(v1, v2), 0., 1.))
        theta *= (180./math.pi)
        theta = int(round(theta,0))
        if theta > 0:
            while theta >= 60:
                theta -= 60
            angle = int(round(theta, 0))
        if theta < 0.0:
            while theta < -60:
                theta += 60
            angle = round(theta, -1)
        lst.append(angle)
        #print "NOW IS ANGLE:", angle
    return lst


"""Check for hoomdxml file and create if necessary."""
cg.check_hoomdxml()

#Get values from trajectory and topologies
TRAJ_FILE = "traj.dcd" #Trajectory file
TOP_FILE = "restart.hoomdxml" #Topology file
t = md.load(TRAJ_FILE, top = TOP_FILE)

residue = t.n_residues
num_atoms = t.n_atoms
axes = t.unitcell_lengths[t.n_frames-1]
apm = int(num_atoms/residue)

p1_list = t.xyz[-1,0:-1:apm,:]
p2_list = t.xyz[-1,apm-2:-1:apm,:]

v_list = []
for i in range(0,residue):
    v_list.append(unit_vector(cg.pbc(p1_list[i]-p2_list[i], axes)))
v_list = np.array(v_list)

angle_list = get_angle(v_list)

for i in angle_list:
    print i
