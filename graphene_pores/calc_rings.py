import numpy as np
import helper_functions.matty_helper as mh
import helper_functions.e_helper as eh
import mdtraj as md
import os
import re
import matplotlib.pyplot as plt

"""Check for hoomdxml file and create if necessary."""
eh.check_hoomdxml()

def get_positions(index):
    for i in range(len(index)):
        positions.append(t.xyz[1,[index[i]], :])
        print i, xml['type'][index[i]], index[i]
    return positions

#Get values from trajectory and topologies
TRAJ_FILE = "traj.dcd" #Trajectory file
TOP_FILE = "restart.hoomdxml" #Topology file
t = md.load(TRAJ_FILE, top = TOP_FILE)

#Get the length of the unit cell edges.
axes = t.unitcell_lengths[t.n_frames-1]
print axes

xml = mh.loadMorphologyXML("restart.xml")

types = xml['type']
index_list = []
positions = []
d_list = []

for i in range(len(types)):
    if types[i] == 'Br' or types[i] == 'O':
        index_list.append(i)

positions = get_positions(index_list)
n_list = [ [] for i in range(len(positions)) ]#List for the neighbors.
positions = np.array(positions)

d_list = []
for i in range(0, len(positions) - 1 ):
    for j in range(i+1, len(positions)):
        a = positions[i,:]
        b = positions[j,:]
        d = np.linalg.norm(b-a)
        d_list.append(d)

n_list = eh.determine_neighbors(positions, n_list, axes, 0.47, 0.46)

for i in range(len(n_list)):
    print i, n_list[i]

"""Create a cluster list from neighbor list."""
c_list=[i for i in range(len(n_list))]
for i in range(len(c_list)):
    c_list = eh.update_neighbors(i,c_list,n_list)

#for i in range(len(n_list)):
#    inclust = ""
#    for j,c in enumerate(c_list):
#        if c==i:
#            inclust+=str(j)+" "
#    if inclust !="":
#        print i,inclust
#print len(set(c_list)), "clusters"
