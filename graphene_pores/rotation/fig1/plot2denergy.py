import numpy as np
import matplotlib.pyplot as plt

E = np.loadtxt('charged.txt')

plt.plot(E[:,0]-60, E[:,1]*.3043)
plt.xlabel(r'Rotation ($^{\circ}$)')
plt.ylabel('Energy (kcal/mol)')
plt.xticks([-30, -15, 0, 15, 30])
plt.xlim([-30, 30])
plt.savefig('energy2sheets.png')
