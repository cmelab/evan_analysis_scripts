import numpy as np
import hoomd
from hoomd import md
from hoomd import deprecated
from cme_utils.manip import builder
from cme_utils.manip import ff 
from cme_utils.manip import hoomd_xml
from cme_utils.manip.initialize_system import interactions_from_xml_verbatim, interactions_from_xml_tabulated
from random import random, randint
from subprocess import check_output
import math
import helper_functions.matty_helper as mh
import sys

rot = sys.argv[1]
#theta = 0.087266
theta = float(rot)/5.*0.087266

xml = mh.loadMorphologyXML('stacked.xml')
positions = np.array(xml['position'])
cut = int(len(positions)/2)

move_list = []
for i in range(len(positions)):
    point = positions[i]
    x = math.cos(theta)* point[0] - point[1] * math.sin(theta)
    y = math.sin(theta)* point[0] + point[1] * math.cos(theta)
    z = point[2]
    move_list.append([x, y, z])

xml['position'] = positions.tolist()[:cut] + move_list[cut:]

#mh.writeMorphologyXML(xml, str(rot)+"-rot.xml")
mh.writeMorphologyXML(xml, "rot.xml")

hoomd.context.initialize()
system = deprecated.init.read_xml(filename = 'rot.xml')

nl_c = md.nlist.cell(check_period=1)

lj, bonds, angles, dihedrals = interactions_from_xml_verbatim(system,factor=1.0, model_file= 'model.xml', model_name= 'default')

grp = hoomd.group.all()

rigid = hoomd.group.rigid()
charged = hoomd.group.charged()

if len(charged)>0:
    pppm = hoomd.md.charge.pppm(group = charged, nlist = nl_c)
    pppm.set_params(Nx=32,Ny=32,Nz=32, order=6, rcut=2.5)

hoomd.md.integrate.mode_standard(dt=1e-6)
int_rig = hoomd.md.integrate.nvt(group = grp,kT=0.01,tau=0.1)
nl_c.reset_exclusions(exclusions=['bond','angle','dihedral','body'])
hoomd.md.force.active( seed=2**10, f_lst=[tuple([3,0,0]) for i in range(cut*2)], group = grp)

#tags = np.arange(0, cut*2,1, dtype = np.int32)
#lj.compute_energy(tags1 = tags[:cut], tags2 = tags[cut:])
#En = lj.compute_energy(tags1 = tags[:cut], tags2 = tags[cut:])
hoomd.compute.thermo(group = grp)
hoomd.analyze.log(filename = 'log', quantities = ['potential_energy', 'pair_lj_energy', 'pppm_energy'],  period = 1e0, header_prefix = '#', overwrite = False)
hoomd.run(1e4)

PE = np.loadtxt('log', skiprows = 1)[-1,1]
print PE

deg = float(rot)
deg = str(deg)

write = str(deg) + " " + str(PE)
data = './'+'charged.txt'
with open(data, 'a+') as textFile:
    textFile.writelines(write + "\n")

hoomd.deprecated.dump.xml(group = hoomd.group.all(), all = True, filename = '{:03d}'.format(int(rot))+'-rot.xml')
