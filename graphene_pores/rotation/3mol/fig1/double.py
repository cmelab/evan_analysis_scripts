import numpy
import helper_functions.matty_helper as mh

xml = mh.loadMorphologyXML("fig1.xml")

na = len(xml['position'])

positions = numpy.array(xml['position'])

for i in positions:
    i += [0.0, 0.0, 1.3]

positions = positions.tolist()

xml['position'] += positions

positions = numpy.array(xml['position'])[:len(xml['position'])/2]

for i in positions:
    i += [0.0, 0.0, 2.6]

positions = positions.tolist()

xml['position'] += positions

xml['type'] += 2*xml['type']
xml['mass'] += 2*xml['mass']
xml['diameter'] += 2*xml['diameter']
xml['body'] += 2*xml['body']
xml['charge'] += 2*xml['charge']

new_bonds = []
for i in xml['bond']:
    new_bonds.append([i[0], i[1]+na, i[2]+na])
for i in xml['bond']:
    new_bonds.append([i[0], i[1]+2*na, i[2]+2*na])
xml['bond'] += new_bonds

new_angles = []
for i in xml['angle']:
    new_angles.append([i[0], i[1]+na, i[2]+na, i[3]+na])
for i in xml['angle']:
    new_angles.append([i[0], i[1]+2*na, i[2]+2*na, i[3]+2*na])
xml['angle'] += new_angles

new_dihedrals = []
for i in xml['dihedral']:
    new_dihedrals.append([i[0], i[1]+na, i[2]+na, i[3]+na, i[4]+na])
for i in xml['dihedral']:
    new_dihedrals.append([i[0], i[1]+2*na, i[2]+2*na, i[3]+2*na, i[4]+2*na])
xml['dihedral'] += new_dihedrals

new_impropers = []
for i in xml['improper']:
    new_impropers.append([i[0], i[1]+na, i[2]+na, i[3]+na, i[4]+na])
for i in xml['improper']:
    new_impropers.append([i[0], i[1]+2*na, i[2]+2*na, i[3]+2*na, i[4]+2*na])
xml['improper'] += new_impropers

xml['natoms'] += 2*xml['natoms']

mh.writeMorphologyXML(xml, "stacked.xml")
