import numpy as np
import hoomd
from hoomd import md
from hoomd import deprecated
from cme_utils.manip import builder
from cme_utils.manip import ff 
from cme_utils.manip import hoomd_xml
from cme_utils.manip.initialize_system import interactions_from_xml_verbatim, interactions_from_xml_tabulated
from random import random, randint
from subprocess import check_output
import math
import helper_functions.matty_helper as mh
import sys

def rotate(array, angle):
    lst = []
    for i in range(len(array)):
        point = array[i]
        x = math.cos(angle)*point[0] - point[1] * math.sin(angle)
        y = math.sin(angle)* point[0] + point[1] * math.cos(angle)
        z = point[2]
        lst.append([x, y, z])
    return lst

xml = mh.loadMorphologyXML('stacked.xml')

middle_rot = int(sys.argv[1])
top_rot = int(sys.argv[2])

cut = len(xml['position'])/3

positions_bottom = np.array(xml['position'][:cut])
positions_middle = np.array(xml['position'][cut:2*cut])
positions_top = np.array(xml['position'][2*cut:])

mid_theta = float(middle_rot)*math.pi/180.
top_theta = float(top_rot)*math.pi/180.

middle_move = rotate(positions_middle, mid_theta)

top_move = rotate(positions_top, top_theta)

positions_bottom.tolist()

positions = []
for i in positions_bottom:
    positions.append(i)
for i in middle_move:
    positions.append(i)
for i in top_move:
    positions.append(i)
xml['position'] = positions

#mh.writeMorphologyXML(xml, "./rotation_xml/"+"{:02d}{:02d}".format(middle_rot, top_rot)+"rot.xml")
mh.writeMorphologyXML(xml, "rot.xml")

hoomd.context.initialize()
system = deprecated.init.read_xml(filename = 'rot.xml')

nl_c = md.nlist.cell(check_period=1)

lj, bonds, angles, dihedrals = interactions_from_xml_verbatim(system,factor=1.0, model_file= 'model.xml', model_name= 'default')

grp = hoomd.group.all()

rigid = hoomd.group.rigid()
charged = hoomd.group.charged()

if len(charged)>0:
    pppm = hoomd.md.charge.pppm(group = charged, nlist = nl_c)
    pppm.set_params(Nx=32,Ny=32,Nz=32, order=6, rcut=2.5)

hoomd.md.integrate.mode_standard(dt=1e-6)
int_rig = hoomd.md.integrate.nvt(group = grp,kT=0.01,tau=0.1)
nl_c.reset_exclusions(exclusions=['bond','angle','dihedral','body'])
hoomd.md.force.active( seed=2**10, f_lst=[tuple([3,0,0]) for i in range(cut*3)], group = grp)

#tags = np.arange(0, cut*2,1, dtype = np.int32)
#lj.compute_energy(tags1 = tags[:cut], tags2 = tags[cut:])
#En = lj.compute_energy(tags1 = tags[:cut], tags2 = tags[cut:])
hoomd.compute.thermo(group = grp)
hoomd.analyze.log(filename = 'log', quantities = ['potential_energy', 'pair_lj_energy', 'pppm_energy'],  period = 1e1, header_prefix = '#', overwrite = False)
hoomd.run(1e2)

PE = np.loadtxt('log', skiprows = 1)[-1,1]
print PE

write = "{:02d}{:02d}".format(middle_rot, top_rot)+ " " + str(PE)
data = './'+'energy.txt'
with open(data, 'a+') as textFile:
    textFile.writelines(write + "\n")

hoomd.deprecated.dump.xml(group = hoomd.group.all(), all = True, filename = "{:02d}{:02d}".format(middle_rot, top_rot)+'-rot.xml')
