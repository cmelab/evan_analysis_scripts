from sys import argv
import numpy as np
import matplotlib.pyplot as plt

#To skip a row: elimnates the name row.
data = np.loadtxt(argv[1], skiprows = 1)

#Splitting up the data:
timesteps = data[:,0]
potential = data[:,1]

#setting new array as: stack
stack = np.vstack((timesteps,potential))

#To set up limits from console:
print("What are your limits?")
start = eval(input('Start (*10^-6) > '))
end = eval(input('End (*10^-6) > '))
steps = eval(input('Step Size (*10^-5, ~5 is good) > '))

start = float(start)
end = float(end)
steps = float(steps)

#converts to the correct order of magnitude.
start = start*(10**6)
end = end*(10**6)
steps = steps*(10**5)

#syntax is (start, end, steps?)
i = np.arange(start, end, steps)[:, None]
selections = np.logical_and(timesteps >= i, timesteps < i+steps)[None]

new, selections = np.broadcast_arrays(stack[:, None], selections)
new = new.astype(float)
new[~selections] = np.nan,

new = np.nanmean(new, axis=-1)

#save averaged array to file.
#np.savetxt('averages.txt', new, delimiter=',')

#sets up axes:
t_ave = new[0,:]
p_ave = new[1,:]

#doing plot
plt.plot(t_ave,p_ave)
plt.ylabel('Average Potential Energy')
plt.xlabel('Timestep')
plt.title('Potential Energy over Time')
plt.savefig('a_vs_t.png')
