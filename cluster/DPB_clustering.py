import numpy as np
import mdtraj as md
import sys
import helper_functions.e_helper as cg
import helper_functions.matty_helper as mh
from multiprocessing import Pool
import matplotlib.pyplot as plt
import os
import re

def pbc(vec):
    """pbc is to account for the periodic boundry conditions. """
    axes = t.unitcell_lengths[t.n_frames-1]
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def check_off_set():
    xml = mh.loadMorphologyXML('restart.hoomdxml')
    first_C = xml['type'].index('C')
    if first_C == 0:
        off_set = 0
        return off_set
    else:
        print("Found DBP is not first, giving an off set.")
        num_DBP_bodies = xml['type'].count('C')//64*5
        total_bodies = np.amax(xml['body'])
        off_set = total_bodies - num_DBP_bodies +1
        return off_set

"""Creates a tcl script for each identified cluster.
Works for the DPB molecule"""
def DPBTCLscript(neighbors, clusters, filename = 'c_color.tcl'):
    off_set = check_off_set()
    forbidden1 = np.arange(10, 200, 32)
    forbidden2 = np.arange(1, 200, 32)
    forbidden = np.concatenate((forbidden1 , forbidden2))
    colors = [x for x in range(0, 200) if x not in forbidden]
    tclLinesToWrite = ['mol delrep 0 0\n']
    tclLinesToWrite += ['pbc wrap -center origin\n'] 
    tclLinesToWrite += ['pbc box -color black -center origin -width 4\n']
    tclLinesToWrite += ['display resetview\n']
    tclLinesToWrite += ['color change rgb 9 1.0 0.29 0.5\n']
    tclLinesToWrite += ['color change rgb 16 0.25 0.25 0.25\n']
    count = 0
    for i in range(len(neighbors)):
        inclust = ""
        for j,c in enumerate(clusters):
            if c==i:
                for k in range(off_set+j*5, off_set+j*5+5):
                    inclust +=str(k)+" "
        if inclust !="":
            tclLinesToWrite += ['mol material AOEdgy\n']
            tclLinesToWrite +=['mol color ColorID '+str(colors[count%32])+'\n']
            tclLinesToWrite +=['mol representation VDW 0.9 8.0\n']
            #tclLinesToWrite +=['mol representation QuickSurf 0.7 0.9 0.5 1.0\n']
            tclLinesToWrite += ['mol selection resid '+str(inclust)+'\n']
            tclLinesToWrite += ['mol addrep 0\n']
            count += 1
    tclLinesToWrite +=['mol color ColorID 10' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type CA' + '\n']
    tclLinesToWrite +=['mol material AOEdgy' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclLinesToWrite +=['mol color ColorID 10' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type CT' + '\n']
    tclLinesToWrite +=['mol material AOEdgy' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclLinesToWrite +=['mol color ColorID 1' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type O' + '\n'] 
    tclLinesToWrite +=['mol material AOEdgy' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclFileName = './'+'c_color.tcl'
    with open(tclFileName, 'w+') as tclFile:
        tclFile.writelines(tclLinesToWrite)
    print('Tcl file written to', tclFileName)

def average_position(array):
    """ This function takes the points, calculates vectors so the periodic boundries can be tested then will average and append the cm list."""
    indice = int(array[0])
    p1 = array[1:4]
    p2 = array[4:7]
    p3 = array[7:10]
    v1 = pbc(p3 - p1)
    v2 = pbc(p3 - p2)
    p1 = p3 - v1
    p2 = p3 - v2
    average = (p1+p2+p3) / 3.
    return [indice, average]

def filter_system(TRAJ_FILE="traj.dcd", TOP_FILE="restart.hoomdxml"):
    #Get values from trajectory and topologies
    print("Loading dcd and filtering to get DBP.")
    TRAJ_FILE = "traj.dcd" #Trajectory file
    TOP_FILE = "restart.hoomdxml" #Topology file
    t = md.load(TRAJ_FILE, top = TOP_FILE)
    topology = t.topology
    t = t.atom_slice(topology.select('name C'))
    print(t)
    print("Done!")
    return t

def get_centers(apm = 64, new = True):
    print("Getting Centers of Mass.")
    frames = (t.n_frames-1)#gets the final frame
    mols = np.arange(t.n_residues)
    if new:
        coms = np.array([[17, 18, 33], 
            [40, 42, 44], 
            [11, 9, 7], 
            [53, 55, 57], 
            [59, 61, 63], 
            [21, 23, 25], 
            [27, 29,31]])
    else:
        coms = np.array([[17, 18, 33]])
    cm_list = []
    for line in coms:
        atom1 = line[0]
        atom2 = line[1]
        atom3 = line[2]
        points = np.hstack((np.transpose([mols]), t.xyz[frames,atom1::apm,:],t.xyz[frames,atom2::apm,:],t.xyz[frames,atom3::apm,:]))
        temp = Pool().map(average_position, points)
        cm_list.append(temp)
    mols_list = [[] for i in range(t.n_residues)]
    for i in range(t.n_residues):
        for j in cm_list:
            for k in j:
                if k[0] == i:
                    mols_list[i].append(k[1])
    print("Done.")
    return mols_list

def check_distance(i, j, t):
    #print("Entering with {}:{}".format(i,j))
    #dcut = 0.14
    dcut = 0.22
    for a in range(len(mols_list[i])):
        for b in range(a, len(mols_list[j])):
            com1 = mols_list[i][a]
            com2 = mols_list[j][b]
            d = np.linalg.norm(pbc(com1-com2))
            if d <= dcut:
                return [i,j]
            break

def build_neighbors(mols_list, t):
    print("Building neighbor list.")
    n_list = [ [] for i in range(t.n_residues) ]
    neighbors_found = []
    check_list = [ [j for j in range(i+1, t.n_residues)] for i in range(t.n_residues-1)]
    for i,val in enumerate(check_list):
        for j in val:
            neighbors_found.append(check_distance(i, j, t))
    neighbors_found = [x for x in neighbors_found if x is not None]
    for x in neighbors_found:
        n_list[x[0]].append(x[1])
        n_list[x[1]].append(x[0])
    print("Done.")
    return n_list

def make_clusters(n_list):
    print("Creating Clusters.")
    c_list = [i for i in range(len(n_list))]
    for i in range(len(c_list)):
        c_list = update_neighbors(i, c_list, n_list)
    print("Done.")
    return c_list

def update_neighbors(particle, cluster_list, neighbor_list):
    """Recursive function to convert neighborlist into cluster list"""
    for n in neighbor_list[particle]:
        if cluster_list[n]>cluster_list[particle]:
            cluster_list[n] = cluster_list[particle]
            cluster_list = update_neighbors(n,cluster_list,neighbor_list)
        elif cluster_list[n] < cluster_list[particle]: 
            cluster_list[particle] = cluster_list[n]
            cluster_list = update_neighbors(particle,cluster_list,neighbor_list)
    return cluster_list

def print_clusters(c_list):
    for i in range(len(n_list)):
        inclust = ""
        for j,c in enumerate(c_list):
            if c==i:
                inclust+=str(j)+" "
        if inclust !="":
            print(i,inclust)
    print(len(set(c_list)), "clusters")

def plot_hist(lst):
    n, bins, patches = plt.hist(lst, np.amax(lst), normed = 1, rwidth = 0.8, facecolor = 'green', alpha = 0.9, align = 'mid')
    plt.xlabel("Cluster Size")
    plt.ylabel("Probability")
    plt.savefig("Cluster_Dist.pdf")

def build_cluster_dist(name=""):
    hist_list = []
    for i in range(len(n_list)):
        if c_list.count(i) != 0:
            hist_list.append(c_list.count(i))
    print(hist_list)
    plot_hist(hist_list)

def get_statepoint():
    """
    Uses the path to extract the state point
    information.
    Requires: 
        None
    Returns:
        phi - packing fraction
        Temp - Temperature
        e_factor - simulation stickiness
    Note: These must be specified in the simation
    directory name for this function to be 
    used.
    """
    #TODO: Change it to read solely from final directory
    path = os.getcwd().split('/')
    Temp = path[-1]
    phi = path[-2]
    e_factor = path[-3]
    phi = float(re.findall(r'phi(.*)', phi)[0])
    Temp = float(re.findall(r'-T(.*)', Temp)[0])
    e_factor = float(re.findall(r'e_fac_(.*)', e_factor)[0])
    return phi, Temp, e_factor

def write_order(order, phi, Temp, e_factor):
    """
    Write the summarized order data to a file.
    The directory will need to be changed for the
    various machines.
    Requires:
        order - float 
        phi - str or float
        Temp - str or float
        e_factor - str or float
    Returns:
        None
    These arguments are used for writing the statepoint
    to the text file for further parsing and plotting.
    """
    write_to_file = "{} {} {} {}".format(order, e_factor, phi, Temp)
    filename = "cluster_data.txt"
    direct = "/home/evanmiller/Projects/Runs/DBP/C70_sweep/"
    if os.path.exists(os.path.join(direct, filename)):
        print("Appending the file")
        with open(os.path.join(direct, filename), 'a') as textFile:
            textFile.write(write_to_file + "\n")
    else:
        print("File not found! Creating a new one!")
        with open(os.path.join(direct, filename), 'w+') as textFile:
            textFile.write(write_to_file + "\n")

def quantify_order(c_list):
    """
    Summarizes over the cluster list to count the 
    number of clusters and their sizes to calculate
    an order parameter 'order' for the simulation.
    Requires:
        c_list - list of clusters
    Returns:
        None
    """
    cluster_counter = []
    total_cluster = []
    for i in set(c_list):
        c_size = c_list.count(i)
        if c_size >= 5:
            cluster_counter.append(c_size)
        total_cluster.append(c_size)
    order = sum(cluster_counter)/sum(total_cluster)
    phi, Temp, e_factor = get_statepoint()
    write_order(order, phi, Temp, e_factor)

if __name__ == "__main__":
    cg.check_hoomdxml()

    t = filter_system()

    mols_list = get_centers(new = False)

    n_list = build_neighbors(mols_list, t)

    c_list = make_clusters(n_list)
    #quantify_order(c_list)

    ##print_clusters(c_list)
    #build_cluster_dist(name=sys.argv[1])

    tcl_script = DPBTCLscript(n_list, c_list)

