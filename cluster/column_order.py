"""This script is meant to calculate the order along columns"""
#Import the module files
import sys
import numpy as np
import mdtraj as md
import helper_functions.e_helper as cg
#from cme_utils.manip.pbc import plain_pbc as pbc

def n_unwrapped(positions,box):
    s = 0
    for i in range(3):
        s += np.sum(np.absolute(positions[i])>box[i]/2.)
    return s

def pbc(positions, box):
    '''Wraps particle positions into a periodic box.
    Arguments:
        positions = numpy array
        box = tuple or list, should have three elements
              assumes box goes from -L/2 to L/2.
    Returns: array of wrapped coordiantes
    '''
    p = np.copy(positions)
    for i in range(3):
        mask= np.absolute(p[i]) > box[i]/2. 
        if mask:
            p[i] -= np.sign(p[i])*box[i]
    if n_unwrapped(p,box)==0:
        return p
    else:
        return pbc(p,box)

def average_position(p1, p2, p3, p4):
    """ This function takes the points, calculates vectors so the periodic boundries can be tested then will average and append the cm list."""
    v1 = pbc(p3 - p1, axes)
    v2 = pbc(p3 - p2, axes)
    v3 = pbc(p3 - p4, axes)
    average = (p1+p2+p3+p4)/4.
    return average

def update_neighbors(particle, cluster_list, neighbor_list):
    """Recursive function to convert neighborlist into cluster list"""
#    print "calling function", particle
    for n in neighbor_list[particle]:
#        print "checking neighbor", particle,":", n
        if cluster_list[n]>cluster_list[particle]:
#            print "updating", n,"to" ,cluster_list[particle]
            cluster_list[n] = cluster_list[particle]
            cluster_list = update_neighbors(n,cluster_list,neighbor_list)
        elif cluster_list[n] < cluster_list[particle]: 
#            print "Updating", cluster_list[particle], "to", cluster_list[n]
            cluster_list[particle] = cluster_list[n]
            cluster_list = update_neighbors(particle,cluster_list,neighbor_list)
    return cluster_list

"""Check for hoomdxml file and create if necessary."""
cg.check_hoomdxml()

#Get values from trajectory and topologies
TRAJ_FILE = "traj.dcd" #Trajectory file
TOP_FILE = "restart.hoomdxml" #Topology file
t = md.load(TRAJ_FILE, top = TOP_FILE)
dist = 0.158
atom1 = 2
atom2 = 4
atom3 = 12
atom4 = 14

natoms = t.n_atoms
apm = 20
nresid = int(t.n_residues/20)
frames = t.n_frames-1
axes = t.unitcell_lengths[0]

cm_list = [] #An empty list for the average positions/center of mass
n_list = [ [] for i in range(nresid) ] #List for the neighbors.

p1_list = t.xyz[frames,atom1:-1:apm,:]
p2_list = t.xyz[frames,atom2:-1:apm,:]
p3_list = t.xyz[frames,atom3:-1:apm,:]
p4_list = t.xyz[frames,atom4:-1:apm,:]

for i in range(nresid):
    cm_list.append(average_position(p1_list[i], p2_list[i], p3_list[i], p4_list[i]))

cm_list = np.array(cm_list)

kick_list = []

for i in range(0,len(cm_list)-1):
    for j in range(i+1,len(cm_list)):
        a = cm_list[i,:]
        b = cm_list[j,:]
        d = np.linalg.norm(b-a)
        if  d < dist:
            n_list[i].append(j)
            n_list[j].append(i)

kick_list = list(set(kick_list))
kick_list.reverse()

"""Print neighbors for each molecule."""
#for i,neighbors in enumerate(n_list):
#    print i, neighbors

"""Create a cluster list from neighbor list."""
c_list=[i for i in range(len(n_list))]
for i in range(len(c_list)):
    c_list = update_neighbors(i,c_list,n_list)

"""Show which residues are in which clusters."""
#for i in range(len(n_list)):
#    inclust = ""
#    for j,c in enumerate(c_list):
#        if c==i:
#            inclust+=str(j)+" "
#    if inclust !="":
#        print i,inclust
#print len(set(c_list)), "clusters"

#for p in kick_list:
#    for i,j in enumerate(n_list):
#        if p == i:
#            del n_list[i]

"""Print neighbors for each molecule."""
#for i,neighbors in enumerate(n_list):
#    print i, neighbors

tcl_script = cg.TCLscript(n_list, c_list)
