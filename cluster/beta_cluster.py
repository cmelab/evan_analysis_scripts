"""Calculate orientation distribution of beta perylene"""

from sys import argv
import numpy as np
import mdtraj as md
import helper_functions.e_helper as cg
from cme_utils.manip.pbc import plain_pbc as pbc
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import interactive
import os
import re

def calculate_orientations(lst1, lst2, lst3, axes):
    """Calculates the three orientation vectors
    that define the molecule.
    'Out sources' the calculation to the
    det_a, det_b and det_c functions.
        Takes:

        lst1-List of particle positions for atom 1
        lst2-List of particle positions for atom 2
        lst3-List of particle positions for atom 3
        axes-Array of periodic box.

        Returns:

            3: 3 x n arrays for each orientational 
            vectors.
        """
    N = len(lst1)
    if len(lst1) != len(lst2) or len(lst1) != len(lst3):
        print("ERROR: The number of positions given do no match!")
        exit(0)
    a_orient = []
    b_orient = []
    c_orient = []
    for molid in range(N):
        p1 = lst1[molid]
        p2 = lst2[molid]
        p3 = lst3[molid]
        a_orient.append(det_a(p1, p2, axes))
        b_orient.append(det_b(p1, p2, p3, axes))
        c_orient.append(det_c(p1, p2, p3, axes))
    a_orient = np.array(a_orient)
    b_orient = np.array(b_orient)
    c_orient = np.array(c_orient)
    return a_orient, b_orient, c_orient

def det_a(p1, p2, axes):
    """Determines the 'a' orientation.
    Requires lists for point 1
    and point 2 and axes for the periodic
    boundry conditions.
    Returns 3 membered list of the orientation.
    """
    a_dir = pbc(p1 -p2, axes)
    a_dir /= np.linalg.norm(a_dir)
    return a_dir

def det_b(p1, p2, p3, axes):
    """Determines the 'b' orientation by
    averaging p1 and p2 and calculating
    the vector from the averaged position to p3
    Requires positions lists for the three
    atoms and axes for the periodic
    boundry conditions.
    Returns 3 membered list of the orientation.
    """
    v1 = pbc(p2 - p1, axes)
    p1 = p2 - v1
    ave = (p1 + p2) /2
    b_dir = pbc(ave-p3, axes)
    b_dir /= np.linalg.norm(b_dir)
    return b_dir

def det_c(p1, p2, p3, axes):
    """Determines the 'c' orientation by
    calculating the cross product between
    vectors of p1 to p3 and p2 to p3.
    Requires positions lists for the three
    atoms and axes for the periodic
    boundry conditions.
    Returns 3 membered list of the orientation.
    """
    v1 = pbc(p3 - p1, axes)
    v2 = pbc(p3 - p2, axes)
    c_dir = np.cross(v1, v2)
    c_dir /= np.linalg.norm(c_dir)
    return c_dir

def plot_c(lst):
    zeros = np.zeros(len(lst))
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.quiver(zeros, zeros, zeros, lst[:,0], lst[:,1], lst[:,2], pivot = 'tail')
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    ax.set_zlim(-1, 1)
    plt.show()
    plt.savefig("vectors.pdf")
    plt.close()

def calc_alignment(lst):
    """Calculates the alignment of molecules based
    off orientation given in the list.
    Argument:
        Array or coordinate positions
    Returns:
        List of dot product calculations.
    """
    d_list = []
    N = len(lst)
    for i in range(0,N-1):
        for j in range(i+1,N):
            v1 = lst[i]
            v2 = lst[j]
            dot = abs(np.dot(v1, v2))
            d_list.append(dot)
    return d_list

def plot_dist(lst1, lst2, lst3):
    '''
    Plot distributions of the dot products
    on one figure.
    Arguments:
        Lists of the calculated dot products
        in the three directions.
    Returns:
        Nothing
    '''
    plt.hist(lst1, bins = 90, histtype = 'stepfilled', normed = True, label = 'A-Direction', color = 'r', stacked = True, alpha = 0.6)
    plt.hist(lst2, bins = 90, histtype = 'stepfilled', normed = True, label = 'B-Direction', color = 'b', stacked = True, alpha = 0.6)
    plt.hist(lst3, bins = 90, histtype = 'stepfilled', normed = True, label = 'C-Direction', color = 'g', stacked = True, alpha = 0.6)
    plt.xlim([-0.03, 1.03])
    plt.xlabel('Dot Product')
    plt.legend(fontsize = 15, loc = 'best')
    loc = os.getcwd()
    #titl = re.findall(r'mol1-(.*)-P', loc)[0]
    #plt.title(titl)
    plt.savefig("Orientation_distribution.pdf")
    #plt.show()

def build_clusters(lst1, lst2, lst3, dot_cut = 0.97):
    N = len(lst1)
    if len(lst1) != len(lst2) or len(lst1) != len(lst3):
        print("ERROR: The number of positions given do no match!")
        exit(0)
    for i in range(0, N-1):
        for j in range(i+1, N):
            v1a = lst1[i]
            v2a = lst1[j]
            v1b = lst2[i]
            v2b = lst2[j]
            v1c = lst3[i]
            v2c = lst3[j]
            dota = abs(np.dot(v1a, v2a))
            dotb = abs(np.dot(v1b, v2b))
            dotc = abs(np.dot(v1c, v2c))
            if dota >= dot_cut and dotb >= dot_cut and dotc >= dot_cut:
                n_list[i].append(j)
                n_list[j].append(i)

"""Check for hoomdxml file and create if necessary."""
cg.check_hoomdxml()

#Get values from trajectory and topologies
TRAJ_FILE = "traj.dcd" #Trajectory file
TOP_FILE = "restart.hoomdxml" #Topology file
t = md.load(TRAJ_FILE, top = TOP_FILE)

apm = 20
print("You have {} atoms per molecule.\nI assume this is perylene".format(apm))
natoms = t.n_atoms
nresid = natoms//apm
print("I found {} molecules in your system.".format(nresid))
frames = t.n_frames-1
axes = t.unitcell_lengths[0]
atom1, atom2, atom3 = 4, 8, 11
print("Atom indices are: {}, {}, and {}.".format(atom1, atom2, atom3))
n_list = [ [] for i in range(nresid) ] #List for the neighbors.

"""The indices over which you want to loop."""
p1_list = t.xyz[frames,atom1:-1:apm,:]
p2_list = t.xyz[frames,atom2:-1:apm,:]
p3_list = t.xyz[frames,atom3:-1:apm,:]

a_list, b_list, c_list = calculate_orientations(p1_list, p2_list, p3_list, axes)
#plot_c(c_list)
adist = calc_alignment(a_list)
bdist = calc_alignment(b_list)
cdist = calc_alignment(c_list)
plot_dist(adist, bdist, cdist)

build_clusters(a_list, b_list, c_list)

"""Create a cluster list from neighbor list."""
cluster_list=[i for i in range(len(n_list))]
for i in range(len(cluster_list)):
    cluster_list = cg.update_neighbors(i,cluster_list,n_list)

#"""Show which residues are in which clusters."""
#for i in range(len(n_list)):
#    inclust = ""
#    for j,c in enumerate(cluster_list):
#        if c==i:
#            inclust+=str(j)+" "
#    if inclust !="":
#        print(i,inclust)

tcl_script = cg.TCLscript(n_list, cluster_list)
