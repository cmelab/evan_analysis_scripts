import numpy as np
import mdtraj as md
import sys
from multiprocessing import Pool
import matplotlib.pyplot as plt
import argparse
import re 
import os
import sys

"""These helper functions can be found in
evan_analysis_scripts."""
import helper_functions.e_helper as cg
import helper_functions.matty_helper as mh

def pbc(vec):
    """
    pbc is to account for the periodic boundry conditions. 
    Calculates the distance between two points and if more
    than half the simulation volume, will add or subtract
    the simulation volume to unwrap coordinates.
    Requires:
        vec - array
    Returns:
        vec - array
    """
    axes = t.unitcell_lengths[t.n_frames-1]
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def check_off_set():
    """
    Uses Matty's xml parser to find the indices for
    CA atoms and determine if the thiophene rings
    are first in the simulation.
    Then gives the off_set: 0 if thiophene rings are first
    or the number of bodies until thiophene rings occur.
    Requires:
        None
    Returns:
        off_set - integer
    """
    xml = mh.loadMorphologyXML('restart.hoomdxml')
    first_C = xml['type'].index('CA')
    if first_C == 0:
        off_set = 0
        return off_set
    else:
        num_p3ht_bodies = 5*xml['type'].count('C')//64
        off_set = num_p3ht_bodies
        return off_set

def TCLscript(neighbors, clusters, scalar):
    """
    Creates a tcl script for each identified cluster.
    Works for the P3HT molecule.
    """
    off_set = check_off_set()
    print('offset = ', off_set)
    if off_set != 0:
        print("Detecting that p3ht isn't first in the system.")
        print("WARNING! this hasn't been tested!")
    forbidden = np.arange(0, 1000, 16)
    colors = [x for x in range(0, 1000) if x not in forbidden]
    tclLinesToWrite = ['mol delrep 0 0\n']
    tclLinesToWrite += ['pbc wrap -center origin\n'] 
    tclLinesToWrite += ['pbc box -color black -center origin -width 4\n']
    tclLinesToWrite += ['display resetview\n']
    tclLinesToWrite += ['color change rgb 9 1.0 0.29 0.5\n']
    tclLinesToWrite += ['color change rgb 16 0.25 0.25 0.25\n']
    count = 0
    for i in range(len(neighbors)):
        inclust = ""
        for j,c in enumerate(clusters):
            if c==i:
                for k in range(off_set+j*scalar, off_set+scalar*(j +1)):
                    inclust +=str(k)+" "
        if inclust !="":
            if len(inclust) >= 12:
                tclLinesToWrite += ['mol material AOEdgy\n']
                tclLinesToWrite += ['mol color ColorID '+str(colors[count%16])+'\n'] 
                tclLinesToWrite +=['mol representation QuickSurf 0.9 1.1 0.5 1.0\n']
                #tclLinesToWrite +=['mol representation VDW 0.9 8.0\n']
                tclLinesToWrite += ['mol selection resid '+str(inclust)+'\n']
                tclLinesToWrite += ['mol addrep 0\n']
                count += 1
    tclLinesToWrite +=['mol color ColorID 0' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type C' + '\n']
    tclLinesToWrite +=['mol material Transparent' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclLinesToWrite +=['mol color ColorID 10' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type CT' + '\n']
    tclLinesToWrite +=['mol material Transparent' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclLinesToWrite +=['mol color ColorID 1' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type O' + '\n'] 
    tclLinesToWrite +=['mol material Transparent' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclFileName = './'+'c_color.tcl'
    with open(tclFileName, 'w+') as tclFile:
        tclFile.writelines(tclLinesToWrite)
    print('Tcl file written to', tclFileName)

def average_position(array):
    """ This function takes the points, calculates vectors so the periodic boundries can be tested then will average and append the cm list."""
    indice = int(array[0])
    p1 = array[1:4]
    p2 = array[4:7]
    v1 = pbc(p2 - p1)
    p1 = p2 - v1
    average = (p1+p2) / 2.
    return [indice, average]

def xml_filter(xml, apm):
    """
    Utilizes the Matty helper to parse the xml data
    and filter the topology data and return 
    the needed clustering information.
    Requires:
        xml - list of toplogy data
    Returns:
        t - class containing topology arrays.
    """
    class xml_only(object):
        xyz = []
        n_frames = 0
        unitcell_lengths = []
        n_residues = 0
    t = xml_only()
    indices = [i for i, x in enumerate(xml['type']) if x == 'FCA']
    t.xyz = np.array([[xml['position'][x] for x in indices]])
    t.xyz *= 0.1
    t.n_frames = 1
    t.n_residues = len(t.xyz[0,:,0])//apm
    t.unitcell_lengths = np.array([[xml['lx'], xml['ly'], xml['lz']]])
    return t

def filter_system(apm):
    """
    Creates a new trajectory from the given trajectory
    where only atoms with type CA or S are present.
    Requires:
        None
    Returns:
        t - mdtraj array with trajectory information.
        or 
        t - arrays with requisite data.
    """
    if args.xml_file:
        print("NOTICE: Reading data from xml_file rather than trajectory information!")
        xml = mh.loadMorphologyXML(args.xml_file)
        t = xml_filter(xml, apm)
    #Get values from trajectory and topologies
    else:
        print("Loading dcd and filtering to get P3HT backbones.\nThis may take a moment")
        TRAJ_FILE = "traj.dcd" #Trajectory file
        TOP_FILE = "restart.hoomdxml" #Topology file
        t = md.load(TRAJ_FILE, top = TOP_FILE)
        topology = t.topology
        t = t.atom_slice(topology.select('name CA'))
        print("Done! Your new trajectory is:",t)
    return t

def build_center_list(apm, index1, index2):
    """
    Creates an array with the atom indices required
    to calculate the center of masses of the thiophene
    rings.
    Assumes the indices 0, 1 and 3 are the indices
    necessary for calculating center of mass.
    Requires:
        apm - int for atoms per molecule 
    (NOTE: apm does not include side chains.)
    Returns:
        coms - array with the indices for the
            complete chain.
    """
    a = np.array([np.arange(index1, apm, apm)]).T
    b = np.array([np.arange(index2, apm, apm)]).T
    coms = np.concatenate((a,b), axis = 1)
    return coms

def get_centers(apm, index1, index2):
    """
    Parses the xyz data for the simulations to get atom positions
    and calculates the center of mass for each thiophene ring.
    Requires:
        apm - int atoms per molecule, this is defaulted to
            75 which fits for 15mers.
    Returns:
       mols_list - list(s) for each center of mass for
        each chain.
    """
    #NOTE: apm (atoms per molecule does not include the aliphatic sidechains)
    print("Getting Centers of Mass.")
    frames = (t.n_frames-1)#gets the final frame
    mols = np.arange(t.n_residues)
    coms = build_center_list(apm, index1, index2)
    cm_list = []
    for line in coms:
        atom1 = line[0]
        atom2 = line[1]
        points = np.hstack((np.transpose([mols]), t.xyz[frames,atom1::apm,:],t.xyz[frames,atom2::apm,:]))
        temp = Pool().map(average_position, points)
        #Pool parallelizes the calculation
        cm_list.append(temp)
    mols_list = [[] for i in range(t.n_residues)]
    #Loop used to ensure the ordering of the molecules and
    #chains is correct.
    for i in range(t.n_residues):
        for j in cm_list:
            for k in j:
                if k[0] == i:
                    mols_list[i].append(k[1])
    print("Centers of mass for each fullerene ring determined.")
    return mols_list

def check_distance(i, j, dcut, req_neighbors):
    """
    Function called by build_neighbors to check
    all the centers of mass between two chains 
    and create a list of the number of thiophene
    centers within the cut off.
    If the required number of centers are within
    the cut off returns the chain indices to be added to the neighbor list.
    Requires:
        i - chain indice for chain #1
        j - chain indice for chain #2
        dcut - distance cut off (in sigma*0.1)
        req_neighbors - the number of chains that
            must be within the cut off to be considered
            a neighbor
    Returns:
        i, j - the indices for two neighboring chains
    """
    count_list = []
    for a in range(len(mols_list[i])):
        for b in range(a, len(mols_list[j])):
            com1 = mols_list[i][a]
            com2 = mols_list[j][b]
            d = np.linalg.norm(pbc(com1-com2))
            if d <= dcut:
                count_list.append(0)
    if len(count_list) >= req_neighbors:
        #print(i,j, len(count_list))
        return [i,j]

def build_neighbors(mols_list):
    """
    Uses a distance cut off to check the distances
    between two thiophene molecules in different 
    chains to determine if they are neighbors.
    Requires:
        mols_list - list of thiophene centers
    Returns: 
        n_list - neighbor list
    """
    print("Building neighbor list.")
    if args.molecule == 'PC70BM' or args.molecule == 'C70':
        dcut = 0.268
    if args.molecule == 'PC60BM' or args.molecule == 'C60':
        dcut = 0.243
    req_neighbors = 1
    print("Your cut off is {:.2f} simga, and fullerenes need".format(dcut*10))
    print("{} neighbors to be considered the same cluster.".format(req_neighbors))
    n_list = [ [] for i in range(t.n_residues) ]
    neighbors_found = []
    check_list = [ [j for j in range(i+1, t.n_residues)] for i in range(t.n_residues-1)]
    for i,val in enumerate(check_list):
        for j in val:
            neighbors_found.append(check_distance(i, j, dcut, req_neighbors))
    neighbors_found = [x for x in neighbors_found if x is not None]
    for x in neighbors_found:
        n_list[x[0]].append(x[1])
        n_list[x[1]].append(x[0])
    print("Done.")
    return n_list

def make_clusters(n_list):
    """
    Function to call for the creation
    of turning the neighbor list into a 
    cluster list.
    Requires:
        n_list - neighbor list
    Returns:
        n_list - neighbor list (likely unnecessary)
        c_list - cluster list
    """
    print("Creating Clusters.")
    c_list = [i for i in range(len(n_list))]
    for i in range(len(c_list)):
        n_list, c_list = update_neighbors(i, c_list, n_list)
    print("Done.")
    return n_list, c_list

def update_neighbors(particle, cluster_list, neighbor_list):
    """Recursive function to convert neighborlist into cluster list"""
    for n in neighbor_list[particle]:
        if cluster_list[n]>cluster_list[particle]:
            cluster_list[n] = cluster_list[particle]
            neighbor_list, cluster_list = update_neighbors(n,cluster_list,neighbor_list)
        elif cluster_list[n] < cluster_list[particle]: 
            cluster_list[particle] = cluster_list[n]
            neighbor_list, cluster_list = update_neighbors(particle,cluster_list,neighbor_list)
    return neighbor_list, cluster_list

def print_clusters(c_list):
    """
    Function to print the molecule clusters.
    Print is within a function so that it can 
    be more easily commented out.
    Requires:
        c_list - cluter list
    """
    print("Your clusters are:")
    for i in range(len(n_list)):
        inclust = ""
        for j,c in enumerate(c_list):
            if c==i:
                inclust+=str(j)+" "
        if inclust !="":
            print(i,inclust)
    print(len(set(c_list)), "clusters")

def plot_hist(lst):
    """
    Function to create a histogram of the 
    cluster sizes.
    Requires:
        lst - hist_list
    """
    n, bins, patches = plt.hist(lst, np.amax(lst), normed = 1, rwidth = 0.8, facecolor = 'green', alpha = 0.9, align = 'mid')
    plt.xlabel("Cluster Size")
    plt.ylabel("Probability")
    plt.savefig("Cluster_Dist.pdf")

def build_cluster_dist():
    """
    Function called to create histogram.
    Function call can be commented out if
    histogram is not desired.
    Requires:
        None
    Returns:
        None
    """
    hist_list = []
    for i in range(len(n_list)):
        if c_list.count(i) != 0:
            hist_list.append(c_list.count(i))
    plot_hist(hist_list)

def plot_coms(mols_list):
    """
    Creates a 3D scatter plot for the center
    of masses for each thiophene ring.
    Is used to trouble shoot the center of mass
    calculations.
    Requires:
        mols_list - list containing the centers
            of mass in each chain.
    Returns:
        None
    """
    mols_list = np.array(mols_list)
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    for mol in range(len(mols_list)):
        ax.scatter(mols_list[mol][:,0], mols_list[mol][:,1],mols_list[mol][:,2], linewidths = 8)
    axes = t.unitcell_lengths[t.n_frames-1]
    ax.set_xlim3d([-axes[0]/2, axes[0]/2])
    ax.set_ylim3d([-axes[0]/2, axes[0]/2])
    ax.set_zlim3d([-axes[0]/2, axes[0]/2])
    plt.show()

def get_statepoint():
    """
    Uses the path to extract the state point
    information.
    Requires: 
        None
    Returns:
        phi - packing fraction
        Temp - Temperature
        e_factor - simulation stickiness
    Note: These must be specified in the simation
    directory name for this function to be 
    used.
    """
    #TODO: Change it to read solely from final directory
    path = os.getcwd()
    phi = float(re.findall('-phi(\d\.\d)', path)[0])
    Temp = float(re.findall('-T(\d\.\d)', path)[0])
    e_factor = float(re.findall('-e(\d\.\d)', path)[0])
    return phi, Temp, e_factor

def write_order(order, phi, Temp, e_factor):
    """
    Write the summarized order data to a file.
    The directory will need to be changed for the
    various machines.
    Requires:
        order - float 
        phi - str or float
        Temp - str or float
        e_factor - str or float
    Returns:
        None
    These arguments are used for writing the statepoint
    to the text file for further parsing and plotting.
    """
    write_to_file = "{} {} {} {}".format(order, e_factor, phi, Temp)
    filename = "/home/pchery/Documents/runs/cluster_data.txt"
    direct = os.getcwd()
    if os.path.exists(os.path.join(direct, filename)):
        print("Appending the file")
        with open(os.path.join(direct, filename), 'a') as textFile:
            textFile.write(write_to_file + "\n")
    else:
        print("File not found! Creating a new one!")
        with open(os.path.join(direct, filename), 'w+') as textFile:
            textFile.write(write_to_file + "\n")

def quantify_order(c_list):
    """
    Summarizes over the cluster list to count the 
    number of clusters and their sizes to calculate
    an order parameter 'order' for the simulation.
    Requires:
        c_list - list of clusters
    Returns:
        None
    """
    cluster_counter = []
    total_cluster = []
    for i in set(c_list):
        c_size = c_list.count(i)
        if c_size >= 4:
            cluster_counter.append(c_size)
        total_cluster.append(c_size)
    order = sum(cluster_counter)/sum(total_cluster)
    print(order)
    #phi, Temp, e_factor = get_statepoint()
    #write_order(order, phi, Temp, e_factor)

if __name__ == "__main__":
    """Parser data allows using the xml file to do
    clustering rather than loading the dcd."""
    parser = argparse.ArgumentParser()
    parser.add_argument("-xml", "--xml_file", help="This file provides the simulation topology information.")
    parser.add_argument("-mol", "--molecule", help="The molecule for the calculation.")
    args = parser.parse_args()
    print(args)

    """Check for hoomdxml file and create if necessary."""
    cg.check_hoomdxml()
    molecules = {'PC60BM': [66, 13, 41, 2],\
             'C60': [60, 13, 41, 1],\
             'C70': [70, 3, 63, 1],\
             'PC70BM': [76, 15, 75, 1]}

    apm = molecules[args.molecule][0]
    index1 = molecules[args.molecule][1]
    index2 = molecules[args.molecule][2]

    t = filter_system(apm)

    mols_list = get_centers(apm, index1, index2)


    n_list = build_neighbors(mols_list)

    n_list, c_list = make_clusters(n_list)

    """Primarily optional functions to return the
    data that is wanted."""
    #plot_coms(mols_list)
    print_clusters(c_list)
    #build_cluster_dist()
    quantify_order(c_list)
    scalar = molecules[args.molecule][3]
    tcl_script = TCLscript(n_list, c_list, scalar)
