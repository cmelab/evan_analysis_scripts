import sys

sys.path.append("/Users/evanmiller/Projects/old_morphct/code")
import helperFunctions as HF

sys.path.append("/Users/evanmiller/Projects/evan_analysis_scripts/DBP")
import DBP_Helper as DH

import numpy as np

if __name__ == "__main__":
    xml = HF.loadMorphologyXML(sys.argv[1])

    DBP, Fullerene  = DH.filter_system(xml)
    DBP.centers, DBP.orientations = DH.get_centers(DBP, orientation_too = True)

    nlist = DH.build_neighbors(DBP.centers, DBP.orientations, DBP, dcut = 3.3, dotcut = 0.9)
    clist = DH.make_clusters(nlist)
    DH.print_clusters(nlist, clist)
    DH.TCL(nlist, clist, "./")
