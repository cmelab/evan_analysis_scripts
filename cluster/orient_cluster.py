"""Calculate orientation of perylene"""

from sys import argv
import numpy as np
import mdtraj as md
import helper_functions.e_helper as cg

"""Check for hoomdxml file and create if necessary."""
cg.check_hoomdxml()

Top_file = "restart.hoomdxml"
Traj_file = "traj.dcd"
t = md.load(Traj_file, top = Top_file)
axes = t.unitcell_lengths[0]

apm = int(argv[1])

n_mol = int(t.n_atoms/apm)
dot_cut = 0.90
r_cut = 0.16
#r_cut = 0.12

p1_list = []
p2_list = []
p3_list = []
p4_list = []
mid_list1 = []
mid_list2 = []
mid_list3 = []
mid_list4 = []
cm_list = []
mol_list = []
mol_list_orth = []
n_list = [ [] for i in range(n_mol) ]#List for the neighbors.
dot_list = []

def pbc(vec):
    """pbc is to account for the periodic boundry conditions. """
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def average_point(p1, p2):
    v1 = pbc(p1 - p2)
    point = p1 - v1
    average = np.mean([point, p1], axis = 0)
    return average

def total_vector(p1, p2):
    orient = pbc(p1 - p2)
    orient /= np.linalg.norm(orient)
    return orient

def update_neighbors(particle, cluster_list, neighbor_list):
    """Recursive function to convert neighborlist into cluster list"""
#    print "calling function", particle
    for n in neighbor_list[particle]:
#        print "checking neighbor", particle,":", n
        if cluster_list[n]>cluster_list[particle]:
#            print "updating", n,"to" ,cluster_list[particle]
            cluster_list[n] = cluster_list[particle]
            cluster_list = update_neighbors(n,cluster_list,neighbor_list)
        elif cluster_list[n] < cluster_list[particle]: 
#            print "Updating", cluster_list[particle], "to", cluster_list[n]
            cluster_list[particle] = cluster_list[n]
            cluster_list = update_neighbors(particle,cluster_list,neighbor_list)
    return cluster_list

"""Create a cluster list from neighbor list."""
c_list=[i for i in range(len(n_list))]
for i in range(len(c_list)):
    c_list = update_neighbors(i,c_list,n_list)

"""The indices over which you want to loop."""
p1_list = t.xyz[-1,4:-1:apm,:]
p2_list = t.xyz[-1,10:-1:apm,:]
p3_list = t.xyz[-1,8:-1:apm,:]
p4_list = t.xyz[-1,12:-1:apm,:]
for i in range(n_mol):
    mid_list1.append(average_point(p1_list[i], p2_list[i]))
    mid_list2.append(average_point(p3_list[i], p4_list[i]))
    mid_list3.append(average_point(p1_list[i], p3_list[i]))
    mid_list4.append(average_point(p2_list[i], p4_list[i]))
mid_list1 = np.array(mid_list1)
mid_list2 = np.array(mid_list2)
mid_list3 = np.array(mid_list3)
mid_list4 = np.array(mid_list4)

#for i in mid_list:
#    print i

for i in range(n_mol):
    mol_list.append(total_vector(mid_list1[i], mid_list2[i]))
    mol_list_orth.append(total_vector(mid_list3[i], mid_list4[i]))
    cm_list.append(average_point(mid_list1[i], mid_list2[i]))
mol_list = np.array(mol_list)
mol_list_orth = np.array(mol_list_orth)
cm_list = np.array(cm_list)

for i in cm_list:
    print(i)

#"""For orientation and distance"""
#for i in range(len(mol_list)-1):
#    for j in range(i,len(mol_list)):
#        a = mol_list[i,:]
#        b = mol_list[j,:]
#        e = cm_list[i,:]
#        f = cm_list[j,:]
#        dot = abs(np.dot(a, b))
#        d = np.linalg.norm((e-f))
#        if i != j:
#            if d <= r_cut:
#                if dot >= dot_cut: 
#                    dot_list.append(dot)
#                    n_list[i].append(j)
#                    n_list[j].append(i)
"""For only distance"""
for i in range(len(mol_list)-1):
    for j in range(i+1,len(mol_list)):
        a = mol_list[i,:]
        b = mol_list[j,:]
        e = cm_list[i,:]
        f = cm_list[j,:]
        d = np.linalg.norm((e-f))
        if i != j:
            if d <= r_cut:
                print(i,j)
                dot_list.append(abs(np.dot(a,b)))
                n_list[i].append(j)
                n_list[j].append(i)
"""If you want only orientation"""
#for i in range(len(mol_list)-1):
#    for j in range(i,len(mol_list)):
#        a = mol_list[i,:]
#        b = mol_list[j,:]
#        c = mol_list_orth[i,:]
#        d = mol_list_orth[j,:]
#        dot_1 = abs(np.dot(a, b))
#        dot_2 = abs(np.dot(c, d))
#        if i != j:
#            if dot_1 > dot_cut and dot_2 > dot_cut:
#                dot_list.append(dot_1)
#                n_list[i].append(j)
#                n_list[j].append(i)

c_list=[i for i in range(len(n_list))]
for i in range(len(c_list)):
    c_list = update_neighbors(i,c_list,n_list)
#print n_list
#for i in dot_list:
#    print(i)
ave_dot = np.mean(np.array(dot_list))
print(ave_dot)

for i in range(len(n_list)):
    inclust = ""
    for j,c in enumerate(c_list):
        if c==i:
            inclust+=str(j)+" "
#    if inclust !="":
#        print i,inclust

column_dot = []
for i in range(len(c_list)):
    b = c_list[i] 
    if i > b:
        column_dot.append(abs(np.dot(mol_list[i],mol_list[b])))
print(np.mean(column_dot))

#tcl_script = cg.TCLscript(n_list, c_list)

"""Print the average dot product for the whole column."""
#print np.mean(np.array(column_dot))
