import sys
import numpy
import matplotlib.pyplot as plt

f = numpy.loadtxt(sys.argv[1])
x = numpy.arange(0., 1.1, 0.1)
plt.hist(f, bins=100)
plt.xticks(x)
plt.savefig('hist.pdf')
plt.xlabel('dot product')
plt.ylabel('occurences')
plt.show()
