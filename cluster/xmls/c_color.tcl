mol delrep 0 0
pbc wrap -center origin
pbc box -color black -center origin -width 6
display resetview
color change rgb 9 1.0 0.29 0.5
mol color ColorID 0
mol representation VDW 1.0 8.0
mol selection residue 0 14 17 20 21 24 26 30 31 40 43 46 48 61 69 71 75 79 83 87 96 99 100 105 113 114 117 120 128 133 136 141 144 149 153 158 159 164 165 172 181 182 184 185 188 190 191 193 197 199 
mol material AOEdgy
mol addrep 0
mol color ColorID 1
mol representation VDW 1.0 8.0
mol selection residue 1 2 3 6 7 9 11 13 18 22 25 33 36 38 42 54 59 60 65 68 76 77 80 84 85 86 89 94 95 101 102 103 109 110 111 115 118 123 126 127 129 131 132 134 139 142 146 148 150 155 156 160 171 175 176 177 179 180 186 187 195 
mol material AOEdgy
mol addrep 0
mol color ColorID 2
mol representation VDW 1.0 8.0
mol selection residue 4 5 8 10 12 15 16 19 23 27 28 29 32 34 35 39 41 44 47 49 50 51 52 53 56 57 58 62 63 66 67 70 72 73 74 78 81 82 88 90 91 92 93 97 98 104 106 108 112 116 119 121 122 124 125 130 135 137 138 143 145 151 152 154 157 161 162 163 166 167 168 169 170 173 174 178 183 189 192 194 
mol material AOEdgy
mol addrep 0
mol color ColorID 3
mol representation VDW 1.0 8.0
mol selection residue 37 64 140 
mol material AOEdgy
mol addrep 0
mol color ColorID 4
mol representation VDW 1.0 8.0
mol selection residue 45 
mol material AOEdgy
mol addrep 0
mol color ColorID 5
mol representation VDW 1.0 8.0
mol selection residue 55 107 147 196 198 
mol material AOEdgy
mol addrep 0
