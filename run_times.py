"""This script is meant to take the log and order_over_time files
and calculate the relaxtion and sampling times."""
import numpy as np
import os
import re

def get_energy_eq(array):
    size = len(array)
    end = size - int(0.1*size)
    mean = np.mean(array[end:])
    for i in range(5, size):
        if array[i] <= mean:
            eq = i
            break
    sampling = len(array)- eq
    return eq, sampling

def get_order_eq(array):
    for i in range(2, len(array)-1):
        if abs(array[-1]-array[i]) <= 0.1 and abs(array[i]-np.mean(array[i+1:i+5])) <= 0.1:
            eq = i
            break
    sampling = len(array)- eq
    return eq, sampling

"""Get temperature and phi"""
path = os.getcwd().split('/')
Temp = path[-1]
phi = path[-2]
phi = re.findall(r'phi(.*)', phi)[0]
Temp = re.findall(r'T(.*)', Temp)[0]

PE = np.loadtxt('log', skiprows = 1)[:,1]
LJ = np.loadtxt('log', skiprows = 1)[:,6]
oot = np.loadtxt('Order_over_time.txt')[:,1]

PEeq, PEsampling = get_energy_eq(PE)
LJeq, LJsampling = get_energy_eq(LJ)
Ordereq, Ordersampling = get_order_eq(oot)

dt = 1.56e-6

PEeq *= 1e5*dt
PEsampling *= 1e5*dt
LJeq *= 1e5*dt
LJsampling *= 1e5*dt
Ordereq *= 1e6*dt
Ordersampling *=1e6*dt

write_to_file = phi + " " + Temp + " "
write_to_file += str(PEeq) + " " + str(PEsampling) + " " + str(LJeq) + " "
write_to_file += str(LJsampling) + " " + str(Ordereq) + " " + str(Ordersampling)

if oot[-1] >= 0.5:
    data = '../../'+'run-time-data.txt'
    with open(data, 'a+') as textFile:
        textFile.writelines(write_to_file + "\n")
