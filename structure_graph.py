import numpy as np
import mdtraj as md
import time
import random
import itertools

def gen_neighbors():
    neighbor_list = []
    for atom in range(N):
        n = np.array([[atom,i] for i in range(N) if i != atom])
        d = md.compute_distances(t, n, periodic = True)[0]
        six = d.argsort()[:6]
        cut = np.mean([d[val] for val in six])*(1+np.sqrt(2))/2
        neighbors = sorted([i for i,val in enumerate(d) if val <= cut])
        for neigh in range(len(neighbors)):
            if neighbors[neigh] >= atom:
                neighbors[neigh]+=1
        neighbor_list.append(neighbors)
    return np.array(neighbor_list)

#Get values from trajectory and topologies
TRAJ_FILE = "traj.dcd" #Trajectory file
TOP_FILE = "restart.hoomdxml" #Topology file
t = md.load_frame(TRAJ_FILE, index = 0, top = TOP_FILE)
N = t.n_atoms

def make_move(index, used, lst, allowed):
    long_list = []
    possible = [i for i in lst[index] if i not in used and i in allowed]
    #print(lst[allowed.index(index)], allowed.index(index))
    trans = list(set(lst[index]).intersection(possible))
#    print("currently on", index, "\nbeen on:", used, "\npossible", possible, "\ntrans", trans)
    if len(trans) > 0:
        for val in trans:
            other = []
            other += used
            other.append(val)
            longl = make_move(val, other, lst, allowed)
            return longl
    else:
        longl = used
        path_length = len(longl)
        if len(allowed) > len(used):
            path_length-=1
        long_list.append(path_length)
        return long_list[0]

def calc_path(allowed, lst):
    steps_list = []
    iterations = []
    for val in allowed:
        used = []
        used.append(val)
        iterations.append(make_move(val, used, lst, allowed))
    #print(iterations)
    path = np.amax(iterations)
    return path

neighbor_list = gen_neighbors()
only = 804
for i,val in enumerate(neighbor_list):
#for i,val in enumerate([neighbor_list[only]]):
    n_neighbors = len(val)
    print(i,val)
    mat = np.zeros((n_neighbors, n_neighbors), dtype = int)
    row_tracker = []
    for row in range(len(val)):
        column_tracker = []
        for column in range(len(val)):
            #print(val[row])
            #print(val[column], neighbor_list[val[column]])
            if val[row] in neighbor_list[val[column]]:
                column_tracker.append(column)
                mat[row][column] = 1
                #mat[row][column] = val[column]
        row_tracker.append(column_tracker)
    CNA = []
    for row in row_tracker:
        if len(row) == 0:
            path = 0
        else:
            path = calc_path(row, row_tracker)
        row_total = []
        for item in row:
            row_total.append(len(list(set(row).intersection(row_tracker[item]))))
        CNA.append([len(row_total), sum(row_total), path])
    un = [list(item) for item in set(tuple(row) for row in CNA)]
    print_list = []
    for unique in un:
        print_list.append("{} x {}".format(CNA.count(unique), unique))
    print(i, print_list)

