"""This script is meant to take in an xml file and shrink
the molecule by a unit of distance: sigma."""

import numpy as np
import helper_functions.matty_helper as mh

def center(array):
    '''Will center the molecule at the origin.
    NOTE: Centering will be done only on 
    position, no weighting for mass is done.'''
    middle =  np.mean(array, axis = 0)
    if abs(middle[0]) <= 1e-6:
        middle[0] = 0.0
    if abs(middle[1]) <= 1e-6:
        middle[1] = 0.0
    if abs(middle[2]) <= 1e-6:
        middle[2] = 0.0
    if np.mean(middle) <= 1e-6:
        print("Already centered!\nDoing Nothing!")
    else:
        print("Centering!")
        array -= middle
    return array

def shrink(array):
    array /= sigma
    return array

xml = mh.loadMorphologyXML("pcbm.xml")
sigma = 3.8
print("You are shrinking your system by: {}".format(sigma))

positions = np.array(xml['position'])

mid = True
if mid:
    a = center(positions)

positions = shrink(positions).tolist()

xml['position'] = positions

mh.writeMorphologyXML(xml, "shrunk.xml")
