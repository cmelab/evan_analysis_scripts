#The purpose of this script is to reverse the potential energies
#of a system, calculate average values and error bars over
#chunks of the potential energy then average that final range.

#import files
from sys import argv
import numpy as np
import matplotlib.pyplot as plt

#import array, split, and turn it around
raw_array=np.loadtxt(argv[1],comments = '#')
raw_x_values =raw_array[:,0]/1e7
raw_y_values = raw_array[:,1]
flipped_x_values = raw_x_values[::-1]
flipped_y_values = raw_y_values[::-1]

#Chunk the arrays
chunk_x_values = np.array_split(flipped_x_values, 10)
chunk_y_values = np.array_split(flipped_y_values, 10)

#Average Array chunsks
average_x_windows = [i.mean() for i in chunk_x_values]
average_y_windows = [i.mean() for i in chunk_y_values]
#Do the standard deviation over the chunks.
deviation_x_windows = [i.std() for i in chunk_x_values]
deviation_y_windows = [2.5*i.std() for i in chunk_y_values]#XX*i.std(),
#the XX is whatever factor is desired to multiple standard deviation by
#for the upper and lower limits.

#Get the last average and range.
final_y_average = average_y_windows[0]
final_y_deviation = deviation_y_windows[0]
y_upper_limit = final_y_average + final_y_deviation
y_lower_limit = final_y_average - final_y_deviation

#Making new array for values that are in the accepted range
within_limits_array=[]
for i in raw_y_values:
    if i < y_upper_limit and i > y_lower_limit:
        within_limits_array.append([i])
"""
I want this to create a new array in which the values are within
the proper range. My worry is that this will also get values
outside of the limits. I want it to go the edge of the limit 
(between y_upper_limit and y_lower_limit) then stop adding more values
via the for loop to the within_limits_array. 
"""
#Average over the 'good' array
potential_average = np.mean(within_limits_array)
standard_potential = np.std(within_limits_array)
print(potential_average, standard_potential)

#Plot the functions if desired
plt.plot(raw_x_values,raw_y_values, label='PE')#Plots the raw data under the error/averages.
plt.errorbar(average_x_windows,average_y_windows,deviation_y_windows, linestyle='None', label='Std. Dev.') #Adds the points for the averages with the associated error bars
plt.ylabel('Potential Energy')
plt.xlabel('Timestep (1x10$^{7}$)')
#plt.title('Potential Energy Over Time')
plt.axvspan(1.92, 1.92, color ='r', alpha = 1.0, label = r"$\tau_{r}$")
plt.xlim([raw_x_values[0], raw_x_values[-1]])
plt.legend(loc='upper right', fontsize = 18, handlelength = 0.3)
plt.savefig('potential.pdf')
