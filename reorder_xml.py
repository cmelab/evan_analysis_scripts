import helper_functions.matty_helper as mh

xml = mh.loadMorphologyXML('restart.xml')

def reorder_list(lst):
    for line in range(len(lst)-1):
        if lst[line][0][0:3] != 'C-C':
            lst+=[lst.pop(0)]
    return lst

def recast(lst):
    for line in lst:
        for index in range(1, len(line)):
            line[index] -= off_set 
            if line[index] < 0:
                line[index] += len(lst)
    reorder_list(lst)
    return lst

off_set = xml['type'].index('C')
for i in range(len(xml['type'])):
    if xml['type'][0] != 'C':
        xml['type']+=[xml['type'].pop(0)]
        xml['position']+=[xml['position'].pop(0)]
        xml['image']+=[xml['image'].pop(0)]
        xml['mass']+=[xml['mass'].pop(0)]
        xml['diameter']+=[xml['diameter'].pop(0)]
        xml['body']+=[xml['body'].pop(0)]
        xml['charge']+=[xml['charge'].pop(0)]

xml['bond'] = recast(xml['bond'])
xml['angle'] = recast(xml['angle'])
xml['dihedral'] = recast(xml['dihedral'])
xml['improper'] = recast(xml['improper'])

mh.writeMorphologyXML(xml, 'reorderd.xml')
