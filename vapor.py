import numpy
import glob
import matplotlib.pyplot as plt
import re
import os

"""This script is meant to take in a series of rdfvalues and determine at which
point the RDF for other temperatures
is no longer distinguishable
from the highest temperature.
This should work tell the vapor transition
if the highest temperature run is vapor"""


data = []  # Place RDF data here.
temp = []  # To store the temperature of the run.

"""Phi value for these RDFs"""
directory = os.getcwd().split('/')[-1]
phi = re.findall(r'phi(.*)',directory)
phi = phi[0]
colormap = plt.cm.hsv
rdf_num = len(glob.glob("rdf-*.txt"))
print(rdf_num)
plt.gca().set_color_cycle([colormap(i) for i in numpy.linspace(0, 0.9, rdf_num)])

"""Loop over all the RDF files in the directory"""
for run in glob.glob("rdf-*.txt"):
    stage = re.findall(r'rdf-(.*).txt', run)[0]
    data.append(numpy.loadtxt(run))
    """num is to extract the run temperature from naming scheme"""
    num = run[4:8]
    num = float(num)
    temp.append(num)
    """Plot the data if desired."""
    plt.plot(data[-1][:,0],data[-1][:,1], label = stage)
    """Get the data to compare over the desired range.
    2.9 to 3.4 is chosen due to not too much pi stacking
    and the intracolumnar distances. """
    record_a = True
    for i, line in enumerate(data[-1]):
        val = line[0]
        if val >= 2.9 and record_a == True:
            a = i
            record_a = False
        if val >= 3.4:
            b = i
            break

"""Create image."""
plt.legend(fontsize = 8, ncol = 3)
plt.savefig("rdf.pdf")

"""Set up comparison."""
cut_off = numpy.mean(data[-1][a:b][:,1])
limit = numpy.std(data[-1][a:b][:,1])
maximum = numpy.mean(data[1][a:b][:,1])*0.05

"""Make comparison and print if distinguishable."""
for i in range(len(data)):
    if abs(numpy.amax(data[i][a:b][:,1])-cut_off) >= maximum:
        print(phi, temp[i], numpy.amax(data[i][:,1]), "1")
    else:
        print(phi, temp[i], numpy.amax(data[i][:,1]), "0")
