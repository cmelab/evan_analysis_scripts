#!/Users/evanmiller/anaconda/bin/python
import sys 
import numpy 
import matplotlib.pyplot as plt

data = []
pfit = []
temp = []
n = len(sys.argv)
for i in range(1,n):
    data.append(numpy.loadtxt(sys.argv[i], skiprows = 1 ))
    name = sys.argv
    l = len(data[-1])
   # x = numpy.array(data[-1][:l,0])
    x = numpy.array(data[-1][:l/2,0])
   # y = numpy.array(data[-1][:l,1])
    y = numpy.array(data[-1][:l/2,1])
    lab= 17-(i-1)/1.
    temp.append(lab)
    plt.plot(x,y,label = lab)
    #pfit.append(numpy.polyfit(x,y,1))
    plt.title('MSD')
    plt.ylabel('Mean Square Displacement')
    plt.xlabel('Timestep')
    plt.legend(loc = 'center left',bbox_to_anchor=(0.9,0.5))
#print pfit[0][0]
#timestep = t[:,0]
#msd = t[:,1]
#plt.plot(timestep,msd)
#plt.plot(temp,pfit)
plt.savefig("msd.png")
plt.show()
