"""This script is used to calculate the standard deviation between molecule dispacements between frames"""
import mdtraj as md
import numpy as np
import sys

TOP = sys.argv[1]
TRAJ = sys.argv[2]
t = md.load(TRAJ, top = TOP)

#NOTE This is meant to work with the unwrapped coordinates
def calc_displacements(traj):
    d = []
    num_frames = t.n_frames
    num_atoms = t.n_atoms
    for i in range(100):
        for j in range(num_atoms):
            d.append(abs(traj[i+1,j,:]-traj[i,j,:]))
    deviation = np.std(d)
    print deviation

disp = calc_displacements(t.xyz)
