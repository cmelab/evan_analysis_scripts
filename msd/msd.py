#!/Users/evanmiller/anaconda/bin/python
import numpy as np
import mdtraj as md
import sys
import matplotlib.pyplot as plt
import math

TRAJ_FILE= sys.argv[2] 
TOP_FILE= sys.argv[1] 
t = md.load(TRAJ_FILE, top=TOP_FILE)
dcd_write_time = 1e0

def unwrap_dcd(traj,axes):
    """Unwrap the positions from the wrapped trajectories."""
    unwrapped = np.zeros(traj.shape)
    """For this set only doing first 100 timesteps."""
    d = traj[1:-1]-traj[1]
    #d = traj[1:-1]-traj[1]
    #assuming all frames have same box lengths:
    #NOTE there is an issue with this line.
    for i in range(3):
        d[ np.absolute(d[:,:,i]) > axes[i]/2.] = d[ np.absolute(d[:,:,i]) > axes[i]/2.]- np.sign( d[np.absolute(d[:,:,i]) > axes[i]/2.])*axes[i]
	#alternative if frames differ in box lengths
        #loop over timesteps and then get individual step axes. not so bad :)
    #actually unwrap coordinates
    """Some test statements"""
#    keep_going = True
#    while keep_going:
#        print "Still going..."
#        keep_going = False
#    for i in range(3):
#        d[ d[:,:,i] < -axes[i]/2.] += axes[i]
#        d[ d[:,:,i] > axes[i]/2.] -= axes[i]
#            if np.sum(d[:,:,i] < -axes[i]/2.):
#                keep_going = True
#            if np.sum(d[:,:,i] > axes[i]/2.):
#                keep_going = True
#            print i, np.sum(d[:,:,i] > axes[i]/2.)
#            print i, np.sum(d[:,:,i] < -axes[i]/2.)
    for i in range(len(d)):
        unwrapped[i] = unwrapped[i]+d[i]
    return unwrapped
        
def hoomd_msd(xyz):
    '''Takes in an unwrapped trajectory and outputs the MSD as a function of step'''
    d = xyz - xyz[0]#This doesn't affect anything
    return np.mean(np.square(np.linalg.norm(d,axis=2)),axis=1)

def get_delta_ones(a,start):
    ''' Calculate the displacements for each particle for each set of adjacent frames'''
    delta_ones = []
    for i in range(start,t.n_frames-1):
        delta_ones.append(np.linalg.norm(abs(a[i+1]-a[i])))
    return np.array(delta_ones)

def calc_larger_deltas(delta_ones):
    """Takes delta_ones to build up full msd."""
    all_deltas = {}
    all_deltas[1]=delta_ones
    for delta_t in range(2,1+len(delta_ones)):
        #want to add consecutive deltas from delta_ones together to make larger deltas
        delta_x = []
        last = len(delta_ones)-delta_t+1
        for t0 in range(last):
            delta_x.append(all_deltas[delta_t-1][t0]+delta_ones[t0+delta_t-1])
        all_deltas[delta_t] = np.array(delta_x)
    return all_deltas

def calc_msd(all_deltas):
    """Creates list of all different sizes of timesteps"""
    msd = {}
    for k,v in all_deltas.iteritems():
        msds = []
        for frame in v:
            msds.append(frame.mean())
        msds = np.array(msds)
        msd[k] = (msds.mean(),msds.std())
    return msd

"""Uses unwrap_dcd and hoomd_msd functions to calculate displacment"""
a = unwrap_dcd(t.xyz, t.unitcell_lengths[0])
HOOMD_MSD = hoomd_msd(a)
"""i will be the timestep when timesed by dcd_write_time"""
for i in range(len(HOOMD_MSD)-2):
    print dcd_write_time*i, HOOMD_MSD[i]
#plt.plot(msd)
#plt.show()

"""Uses get_delta_ones, calc_larger_deltas, and calc_msd"""
#delta_ones = get_delta_ones(HOOMD_MSD,0)
#all_deltas = calc_larger_deltas(delta_ones)
#msd = calc_msd(all_deltas)

"""Can be used for plotting and getting slope of MSD"""
#x_list = []
#y_list = []
#for k,v in msd.iteritems():
#    print k, v[0],v[1] 
#    x_list.append(k)
#    y_list.append(v[0])
#plt.plot(x_list, y_list)
#best_fit = np.polyfit(x_list, y_list, 1)
#for i in range(len(x_list)):
#    print x_list[i], y_list[i]
#print best_fit[0]
#plt.show()
