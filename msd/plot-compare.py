import sys
import numpy as np
import matplotlib.pyplot as plt


hoomdmsd1 = np.loadtxt(sys.argv[1],skiprows = 1)
#hoomdmsd = np.loadtxt(sys.argv[3],skiprows = 1)
regmsd1 = np.loadtxt(sys.argv[2], skiprows = 1)
#regmsd = np.loadtxt(sys.argv[4], skiprows = 1)
#hoomdmsd = np.loadtxt(sys.argv[5],skiprows = 1)

scaler = 1# For raw msd
#scaler = 10# For Delta ones
plt.plot(hoomdmsd1[:,0],hoomdmsd1[:,1],label = 'hoomd1e0')
#plt.plot(hoomdmsd[:,0],hoomdmsd[:,1],label = 'hoomd1e5')
plt.plot(regmsd1[:,0], 100*regmsd1[:,1], label='unwrapped1e0,y*100')
#plt.plot(10.*float(scaler)*regmsd[:,0], 1000.*regmsd[:,1], label='unwrapped1e5,y*1000')
#plt.plot(hoomdmsd[:,0],hoomdmsd[:,1],label = 'hoomd1e6')
plt.legend(loc = 2)
plt.xlabel('timesteps')
plt.ylabel('msd')
plt.savefig('hoomd-v-reg.png')
plt.show()
