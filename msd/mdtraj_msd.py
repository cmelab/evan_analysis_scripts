import numpy as np
import mdtraj as md
import sys
import matplotlib.pyplot as plt

TRAJ = sys.argv[1]
TOP_FILE = sys.argv[2]
START_FRAME = 0
DT = int(1e6)
t = md.load(TRAJ, top=TOP_FILE)

def calc_larger_deltas(delta_ones):
    all_deltas = {}
    all_deltas[1]=delta_ones
    for delta_t in range(2,1+len(delta_ones)):
        delta_x = []
        last = len(delta_ones)-delta_t + 1
        for t0 in range(last):
            delta_x.append(all_deltas[delta_t-1][t0]+delta_ones[t0+delta_t-1])
        all_deltas[delta_t] = np.array(delta_x)
    return all_deltas

def calc_msd(all_deltas):
    msd = {}
    for k,v in all_deltas.iteritems():
        msds = []
        for frame in v:
            msds.append(frame.mean())
        msds = np.array(msds)
        msd[k] = (msds.mean(),msds.std())
    return msd

delta_ones = md.rmsd(t, t, 0)**2
all_deltas = calc_larger_deltas(delta_ones)
msd = calc_msd(all_deltas)

x_list = []
y_list = []
for k,v in msd.iteritems():
    x_list.append(k)
    y_list.append(v[0])
    #print k, v[0], v[1]
#print x_list, y_list
plt.plot(x_list, y_list)
best_fit = np.polyfit(x_list, y_list, 1)
print best_fit[0]
#plt.show()
