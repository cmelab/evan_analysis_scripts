import numpy as np
import hoomd
from hoomd import md
from hoomd import deprecated
import helper_functions.matty_helper as mh
from cme_utils.manip import builder

def plot_coms(mols_list):
    """
    Creates a 3D scatter plot for the center
    of masses for each thiophene ring.
    Is used to trouble shoot the center of mass
    calculations.
    Requires:
        mols_list - list containing the centers
            of mass in each chain.
    Returns:
        None
    """
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    for mol in mols_list:
        ax.scatter(mol[0], mol[1],mol[2], linewidths = 8, color = 'k')
    #ax.set_xlim3d([-axes[0]/2, axes[0]/2])
    #ax.set_ylim3d([-axes[0]/2, axes[0]/2])
    #ax.set_zlim3d([-axes[0]/2, axes[0]/2])
    plt.xlabel('X')
    plt.ylabel('Y')
    plt.show()

def calc_theta(vec1, vec2):
    theta = np.arccos(np.dot(vec1, vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2)))
    return theta

def build_z_matrix(theta):
    matrix = np.array([[1, 0.0, 0.0],
            [0, np.cos(theta), -np.sin(theta)],
            [0, np.sin(theta), np.cos(theta)]])
    return matrix

def build_y_matrix(theta):
    matrix = np.array([[np.cos(theta), 0.0, np.sin(theta)],
            [0.0, 1.0, 0.0],
            [-np.sin(theta), 0.0, np.cos(theta)]])
    return matrix

def rot_matrix(theta1, theta2):
    z = build_z_matrix(theta1)
    y = build_y_matrix(theta2)
    matrix = np.matmul(z, y)
    return matrix

def norm_matrix(array):
    array1 = array[0]
    array2 = array[1]
    array3 = array[2]
    vec1 = array1 - array2
    vec2 = array1 - array3
    cross = np.cross(vec1, vec2)/np.linalg.norm((vec1-vec2))
    theta = calc_theta(cross, np.array([0., 0., 1.]))
    matrix = build_z_matrix(theta)
    updated2 = []
    for atom in array:
        updated2.append(np.matmul(matrix,np.array([np.array(atom)]).T).flatten())
    updated2 = np.array(updated2)
    return updated2

def rotate_to_axis(molA, array):
    dist = np.linalg.norm(array)
    new_point = np.array([dist, 0.0, 0.0])
    theta1 = calc_theta(array[:2], new_point[:2])
    theta2 = calc_theta(array[0:3:2], new_point[0:3:2])
    rotation_mat = rot_matrix(theta1, theta2)
    updated = []
    for atom in molA:
        updated.append(np.matmul(rotation_mat,np.array([np.array(atom)]).T).flatten())
    updated = np.array(updated)
    new = norm_matrix(updated)
    return new

def rotate_around(molA, theta1, theta2):
    theta1 = theta1*np.pi/180
    theta2 = theta2*np.pi/180
    rotation_mat = rot_matrix(theta1, theta2)
    updated = []
    for atom in molA:
        updated.append(np.matmul(rotation_mat,np.array([np.array(atom)]).T).flatten())
    updated = np.array(updated)
    return updated

mol = np.array([
[0.943255, 1.932, 1.836985],
[2.272255, 1.927, 2.305985],
[3.128255, 0.965, 1.858985],
[2.694255, -0.044, 0.929985],#move this to axis
[3.587255, -1.027, 0.481985],
[3.153745, -1.963, -0.425985],
[1.838745, -1.946, -0.876985],
[0.922745, -1.01, -0.455985],
[1.349255, -0.026, 0.468985],
[0.461255, 0.992, 0.942985],
[-0.943255, -1.932, -1.836985],
[-2.272255, -1.927, -2.305985],
[-3.128255, -0.965, -1.858985],
[-2.694255, 0.044, -0.929985],
[-3.587255, 1.027, -0.481985],
[-3.153745, 1.963, 0.425985],
[-1.838745, 1.946, 0.876985],
[-0.922745, 1.01, 0.455985],
[-1.349255, 0.026, -0.468985],
[-0.461255, -0.992, -0.942985]
])/3.905

molA = rotate_to_axis(mol, np.array([2.694255, -0.044, 0.929985]))

molA_diam = 20*[1.0]
molA_types = 20*['C']
molA = tuple(map(tuple, molA))

hoomd.context.initialize()
#system = hoomd.init.create_lattice(unitcell = hoomd.lattice.unitcell(N = 1, a1 = list(axis1), a2 = list(axis2), a3 = list(axis3), type_name='A'), n = [3, 3, 3])
system = hoomd.init.create_lattice(unitcell = hoomd.lattice.unitcell(N = 1, a1 = [2.8, 0.0, 0], a2 = [0.0, 2.2, 0], a3 = [0, 0.0, 1.0], type_name='A'), n = [1, 1, 27])
system.particles.types.add('C')

center = hoomd.group.rigid_center()
rigid = hoomd.md.constrain.rigid()

rigid.set_param('A', positions=molA, types = molA_types, diameters = molA_diam)
rigid.create_bodies()
hoomd.deprecated.dump.xml(hoomd.group.all(), "stacks.xml", all=True)

def purge_centers(xml):
    print("Trying to purge the center atoms!")
    bodies = xml['type'].count('A')
    for section in ['position', 'image', 'mass', 'charge', 'diameter', 'type', 'body']:
        xml[section] = xml[section][bodies:]
    xml['natoms'] = len(xml['type'])
    return xml

def wrap(xml):
    print('Wrapping')
    positions = np.array(xml['position'])
    images = np.array(xml['image'])
    box = [xml['lx'], xml['ly'], xml['lz']]
    for atom in zip(positions, images):
        for axis in range(3):
            if abs(atom[0][axis]) > abs(box[axis]):
                atom[0][axis] += np.sign(atom[1][axis])*box[axis]
                #atom[1][axis] = np.sign(atom[0][axis])
    xml['position'] = list(positions)
    xml['image'] = list(images)
    return xml

def unwrap(xml):
    positions = np.array(xml['position'])
    images = np.array(xml['image'])
    box = [xml['lx'], xml['ly'], xml['lz']]
    for atom in zip(positions, images):
        for axis in range(3):
            atom[0][axis]+=atom[1][axis]*box[axis]
    xml['position'] = list(positions)
    xml['image'] = list(images)
    return xml

def build_bonds(array, xml):
    bonds = [["{}-{}".format(xml['type'][i],xml['type'][j]), i, j] for i in range(0, len(array[:,0])-1) for j in range(i+1, len(array[:,0])) if np.linalg.norm((array[i]-array[j])) <= 0.4]
    bonds = replicate_bonds(bonds, xml)
    xml['bond'] = bonds
    return xml

def replicate_bonds(bonds, xml):
    bonds = [[bond[0], bond[1]+xml['body'].count(0)*body, bond[2]+xml['body'].count(0)*body] for body in list(set(xml['body'])) for bond in bonds ]
    return bonds

def rotate_system(xml):
    positions = np.array(xml['position'])
    #x_shift = np.min(positions[:,0])
    #y_shift = np.min(positions[:,1])
    #z_shift = np.min(positions[:,2])
    #positions-=np.array([x_shift, y_shift, z_shift])
    rot = rotate_around(positions, 0, 0)
    return rot

axis1 = rotate_around(np.array([[2.8, 0.0, 0]]), 0, 60)[0]
axis2 = rotate_around(np.array([[0.0, 2.2, 0]]), 0, 60)[0]
axis3 = rotate_around(np.array([[0, 0.0, 1.0]]), 0, 60)[0]
print(axis1, axis2, axis3)

print("Now doing magic")
xml = mh.loadMorphologyXML('stacks.xml')

lx, ly, lz = xml['lx'], xml['ly'], xml['lz']

xml = purge_centers(xml)

xml = build_bonds(mol, xml)

rot = rotate_system(xml)

xml['position'] = list(rot)

#xml = wrap(xml)

#plot_coms(np.array(xml['position'])[41:60,:])

mh.writeMorphologyXML(xml, 'new_stacks.xml')
exit()

b = builder.building_block()
b.from_xml('new_stacks.xml')
b.generate_all_constraints()
b.write('new_stacks.xml')

xml = mh.loadMorphologyXML('new_stacks.xml')
xml['lx'], xml['ly'], xml['lz'] = lx, ly, lz
mh.writeMorphologyXML(xml, 'new_stacks.xml')
