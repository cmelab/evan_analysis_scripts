"""Script to build a monoclinic unit cell for beta perylene. 
Written to be used to create a 'boiler plate' for the
positions of the A and B particles which will have substituents
added to them in beta.py."""
import numpy as np
import math
import helper_functions.matty_helper as mh
import time

class gen_beta():
    """Class designed to be called by beta.py
    to initialize a monoclinic system of A and B
    particles."""
    def __init__(self, **kwargs):
        self.sigma = 1.0
        self.C_diam = 1.2
        self.rep = [2, 3, 2]
        self.a_lattice = [9.763, 0.0, 0.0]
        self.b_lattice = [0.0, 5.843, 0.0]
        self.c_lattice = [1.25, 0, 10.534]
        self.molA_com = [-1.250255, 5.843, 10.534015]
        self.molB_com = [-0.625005, 2.9215, 5.267065]
        self.total_positions = []
        for k,v in kwargs.items():
            self.__dict__[k] = v

    def write_input(self):
        '''Writes the xml file and also acts as a 
        convinience function to build the whole of 
        the input file for beta perylene.'''
        start_time = time.time()
        xml = {'natoms':[], 'lx':[], 'ly':[], 'lz':[], 'xy':[], 'xz':[], 'yz':[], 'position':[], 'image':[], 'mass':[], 'diameter':[], 'type':[], 'body':[], 'bond':[], 'angle':[], 'dihedral':[], 'improper':[], 'charge':[]}
        self.translate_atoms()
        self.center()
        xml['position'] = self.total_positions
        self.build_xml(xml)
        self.unit_cell(xml)
        mh.writeMorphologyXML(xml, "init.xml")
        print("THANK YOU FOR GENERATING SOME BETA PERYLENE! ENJOY!")
        print("IT TOOK: --- %s seconds ---" % (time.time() - start_time))

    def translate_atoms(self):
        '''Calls translate x, y and z to copy
        the unit cell in all three dimensions.'''
        self.translate_x(com = self.molA_com)
        self.translate_x(com = self.molB_com)

    def translate_x(self, com):
        lst = []
        for rep in range(self.rep[0]):
            lst.append(np.array(com) + float(rep)*np.array(self.a_lattice))
        self.translate_y(lst)

    def translate_y(self, lst):
        y_list = []
        for rep in range(self.rep[1]):
            for molecule in lst:
                y_list.append(np.array(molecule) + float(rep)*np.array(self.b_lattice))
        self.translate_z(y_list)

    def translate_z(self, y_list):
        z_list = []
        for rep in range(self.rep[2]):
            for molecule in y_list:
                z_list.append(np.array(molecule) + float(rep)*np.array(self.c_lattice))
        self.total_positions += z_list

    def center(self):
        """Moves the particles to the center of the simulation,
        this is necessary for hoomd to initialize."""
        final_pos = []
        com = np.mean(self.total_positions, axis = 0)
        pos = (np.array(self.total_positions) - com)
        for updated in pos:
            final_pos.append(updated/self.sigma)
        self.total_positions = final_pos

    def build_xml(self, xml):
        '''Puts in the xml data for the A and B particles.
        Assumes that self.translate_atoms conducted the
        calculation with particle A then particle B.'''
        length = len(xml['position'])
        xml['natoms'] = length
        xml['mass'] = length*[0.01]
        xml['diameter'] = length*[0.01]
        xml['body'] = np.arange(0, length)
        xml['charge'] = length*[0.0]
        xml['type'] = length//2*['A'] + length//2*['B'] 
        return xml

    def unit_cell(self, xml):
        """Calculates the lx, ly, lz of the simulation
        volume with the given repeated unitcells.
        Requires the xml dictionary.
        The xml['xy'], xz, yz are not necessarily
        used from helper_functions.matty_helper. 
        The adjusted version must be used."""
        a = np.array(self.a_lattice)/self.sigma
        b = np.array(self.b_lattice)/self.sigma
        c = np.array(self.c_lattice)/self.sigma
        ma = np.linalg.norm(a)
        mb = np.linalg.norm(b)
        mc = np.linalg.norm(c)
        lx = self.rep[0]*ma
        ly = self.rep[1]*mb
        lz = self.rep[2]*mc
        xy = math.pi/2-np.arccos(np.dot(a, b)/(ma*mb))
        xz = math.pi/2-np.arccos(np.dot(a, c)/(ma*mc))
        yz = math.pi/2-np.arccos(np.dot(b, c)/(mb*mc))
        xml['lx'] = lx
        xml['ly'] = ly
        xml['lz'] = lz
        xml['xy'] = xy
        xml['xz'] = xz
        xml['yz'] = yz
        return xml
