import numpy as np
import hoomd
from hoomd import md
from hoomd import deprecated
from cme_utils.manip.initialize_system import interactions_from_xml_verbatim, interactions_from_xml_tabulated
import generate_system

normalization = 3.8
sigma = normalization
a = generate_system.gen_beta(rep = [5, 6, 5], sigma = normalization)
a.write_input()

charges = False

diam = 1.0

molA = np.array([
[0.943255, 1.932, 1.836985],
[2.272255, 1.927, 2.305985],
[3.128255, 0.965, 1.858985],
[2.694255, -0.044, 0.929985],
[3.587255, -1.027, 0.481985],
[3.153745, -1.963, -0.425985],
[1.838745, -1.946, -0.876985],
[0.922745, -1.01, -0.455985],
[1.349255, -0.026, 0.468985],
[0.461255, 0.992, 0.942985],
[-0.943255, -1.932, -1.836985],
[-2.272255, -1.927, -2.305985],
[-3.128255, -0.965, -1.858985],
[-2.694255, 0.044, -0.929985],
[-3.587255, 1.027, -0.481985],
[-3.153745, 1.963, 0.425985],
[-1.838745, 1.946, 0.876985],
[-0.922745, 1.01, 0.455985],
[-1.349255, 0.026, -0.468985],
[-0.461255, -0.992, -0.942985]
])/sigma
molA_types = []
molA_diam = 20*[diam]
if charges == False:
    molA_types += 20*['C']
else:
    molA_types += 3*['CP'] +1*['CN']+ 3*['CP'] +3*['CN'] +3*['CP'] +1*['CN']+ 3*['CP'] + 3*['CN']
molA = tuple(map(tuple, molA))

molB = np.array([
[-0.942995, 1.9315, -1.837065],
[-2.272995, 1.9275, -2.306065],
[-3.127995, 0.9655, -1.859065],
[-2.693995, -0.0445, -0.930065],
[-3.587995, -1.0275, -0.482065],
[-3.153505, -1.9635, 0.425965],
[-1.838505, -1.9455, 0.877965],
[-0.922505, -1.0105, 0.455965],
[-1.349995, -0.0255, -0.469065],
[-0.460995, 0.9925, -0.943065],
[0.942495, -1.9315, 1.836965],
[2.272495, -1.9275, 2.305965],
[3.128495, -0.9655, 1.858965],
[2.693495, 0.0445, 0.929965],
[3.587495, 1.0275, 0.481965],
[3.154005, 1.9635, -0.426065],
[1.839005, 1.9455, -0.877065],
[0.923005, 1.0105, -0.456065],
[1.349495, 0.0255, 0.468965],
[0.461495, -0.9925, 0.942965]
])/sigma
molB_types = []
molB_diam = 20*[diam]
if charges == False:
    molB_types += 20*['C']
else:
    molB_types += 3*['CP'] +1*['CN']+ 3*['CP'] +3*['CN'] +3*['CP'] +1*['CN']+ 3*['CP'] + 3*['CN']
molB = tuple(map(tuple, molB))

hoomd.context.initialize()

system = hoomd.deprecated.init.read_xml(filename = 'init.xml')
system.particles.types.add('C')
system.particles.types.add('CN')
system.particles.types.add('CP')
print("\n\nYOUR BOX IS:")
print("{} \n\n".format(system.box))
print("PARTICLE #1 = {}".format(system.particles[0]))

nl = md.nlist.cell()
lj, bonds, angles, dihedrals = interactions_from_xml_verbatim(system,factor=1.0, model_file= 'model.xml', model_name= 'default')
lj.pair_coeff.set('CN', 'CN', epsilon = 1, sigma = 1.0)
lj.pair_coeff.set('CP', 'CN', epsilon = 1, sigma = 1.0)
lj.pair_coeff.set('CP', 'CP', epsilon = 1, sigma = 1.0)
lj.pair_coeff.set('A', 'CP', epsilon = 0, sigma = 1.0)
lj.pair_coeff.set('B', 'CP', epsilon = 0, sigma = 1.0) 
lj.pair_coeff.set('A', 'CN', epsilon = 0, sigma = 1.0)
lj.pair_coeff.set('B', 'CN', epsilon = 0, sigma = 1.0)
lj.pair_coeff.set('C', 'CN', epsilon = 1, sigma = 1.0)
lj.pair_coeff.set('C', 'CP', epsilon = 1, sigma = 1.0)
center = hoomd.group.rigid_center()
rigid = hoomd.md.constrain.rigid()
grp = hoomd.group.all()

rigid.set_param('A', positions=molA, types = molA_types, diameters = molA_diam)
rigid.set_param('B', positions=molB, types = molB_types, diameters = molB_diam)
rigid.create_bodies()

if charges == True:
   groupN = hoomd.group.type(name='negatives', type='CN') 
   groupP = hoomd.group.type(name='Positive', type='CP') 
   for atom in groupN:
       atom.charge = -5.82
   for atom in groupP:
       atom.charge = 3.88

hoomd.md.integrate.mode_standard(dt=1e-4, aniso = True)
int_rig = hoomd.md.integrate.nvt(group = center,kT=0.5,tau=2.)

hoomd.dump.dcd(filename = "traj.dcd", period = 1e2, overwrite = True)
hoomd.analyze.log(filename = "log", quantities = ['potential_energy'], period = 1e3, overwrite = True)
hoomd.deprecated.dump.xml(grp, "init.xml", all=True)
hoomd.run(1e3)
print("\nYOUR BOX IS:")
print(system.box)
print("\n \nI just ran the simulation!\n \n")
hoomd.deprecated.dump.xml(grp, "post.xml", all=True)
