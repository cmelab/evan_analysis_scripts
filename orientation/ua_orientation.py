import numpy as np
import mdtraj as md
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

top_F = "restart.hoomdxml"
traj = "traj.dcd"
t = md.load(traj, top = top_F)
axes = t.unitcell_lengths[0]

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

test1 = np.array([[-1,1,0],[0,1,0],[1,1,0],[2,1,0]])
test2 = np.array([[1,-1,0],[2,-1,0],[3,-1,0],[4,-1,0]])
test3 = np.array([[1,1,0],[2,1,0],[3,1,0],[4,1,0]])

residues=np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199])

p4 = []
p10 = []
pS = []
ave_poi = []
t_vec = []

def pbc(vec):
    """pbc is to account for the periodic boundry conditions. """
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def average_point(p1, p2):
    v1 = pbc(p1 - p2)
    point = p1 - v1
    average = np.mean([point, p1], axis = 0)
    return average

def total_vector(p1, p2):
    orient = pbc(p1 - p2)
    orient /= np.linalg.norm(orient)
    ax.quiver(p1[0],p1[1],p1[2],orient[0],orient[1],orient[2], length = 1.)
    return orient

def dot_product(v_list):
    dot_list = []
    for i in range(len(v_list)):
        for j in range(len(v_list)):
            if i != j and j > i:
                dot_list.append(np.dot(v_list[i], v_list[j]))
    dot_list = np.array(dot_list)
    ave_dot = np.mean(abs(dot_list))
    return ave_dot

for i in range(len(residues)):
    p4.append(t.xyz[-1,4+20*i,:])
    p10.append(t.xyz[-1,8+20*i,:])
    pS.append(t.xyz[-1,11+20*i,:])
p4 = np.array(p4)
p10 = np.array(p10)
pS = np.array(pS)

#for i in range(len(test1)):
#    ave_poi.append(average_point(test1[i], test2[i]))

for i in range(len(p4)):
    ave_poi.append(average_point(p4[i], p10[i]))

ave_poi = np.array(ave_poi)

#for i in ave_poi:
#    print i

for i in range(len(ave_poi)):
    t_vec.append(total_vector(ave_poi[i], pS[i]))

#for i in range(len(ave_poi)):
#    t_vec.append(total_vector(ave_poi[i], test3[i]))

dp = dot_product(t_vec)
print dp
ax.set_xlim([-2,2])
ax.set_ylim([-2,2])
ax.set_zlim([-2,2])
plt.show()
