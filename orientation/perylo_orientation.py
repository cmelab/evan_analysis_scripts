import numpy as np
import mdtraj as md
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

top_F = "restart.hoomdxml"
traj = "traj.dcd"
t = md.load(traj, top = top_F)
axes = t.unitcell_lengths[0]

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

test1 = np.array([[-1,1,0],[0,1,0],[1,1,0],[2,1,0]])
test2 = np.array([[1,-1,0],[2,-1,0],[3,-1,0],[4,-1,0]])
test3 = np.array([[1,1,0],[2,1,0],[3,1,0],[4,1,0]])

anneal_residues = np.array([169, 105, 65, 7, 100, 10, 101, 129, 59, 13, 157, 112, 159])
nanneal_residues = np.array([38, 54, 138, 48, 128, 163, 194, 183, 135, 199, 89, 117])

p4 = []
p10 = []
pS = []
ave_poi = []
t_vec = []
def pbc(vec):
    """pbc is to account for the periodic boundry conditions. """
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def average_point(p1, p2):
    v1 = pbc(p1 - p2)
    point = p1 - v1
    average = np.mean([point, p1], axis = 0)
    return average

def total_vector(p1, p2):
    orient = pbc(p1 - p2)
    orient /= np.linalg.norm(orient)
    ax.quiver(p1[0],p1[1],p1[2],orient[0],orient[1],orient[2], length = 1.)
    return orient

def dot_product(v_list):
    dot_list = []
    for i in range(len(v_list)):
        for j in range(len(v_list)):
            if i != j and j > i:
                dot_list.append(np.dot(v_list[i], v_list[j]))
    dot_list = np.array(dot_list)
    ave_dot = np.mean(dot_list)
    return ave_dot

for i in range(len(nanneal_residues)):
    p4.append(t.xyz[-1,4+21*nanneal_residues[i],:])
    p10.append(t.xyz[-1,10+21*nanneal_residues[i],:])
    pS.append(t.xyz[-1,20+21*nanneal_residues[i],:])
p4 = np.array(p4)
p10 = np.array(p10)
pS = np.array(pS)

#for i in range(len(test1)):
#    ave_poi.append(average_point(test1[i], test2[i]))

for i in range(len(p4)):
    ave_poi.append(average_point(p4[i], p10[i]))

ave_poi = np.array(ave_poi)

#for i in ave_poi:
#    print i

for i in range(len(ave_poi)):
    t_vec.append(total_vector(ave_poi[i], pS[i]))

#for i in range(len(ave_poi)):
#    t_vec.append(total_vector(ave_poi[i], test3[i]))

dp = dot_product(t_vec)
print dp
ax.set_xlim([-2,2])
ax.set_ylim([-2,2])
ax.set_zlim([-2,2])
plt.show()
