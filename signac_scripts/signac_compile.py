import numpy as np
import signac
import sys
import pickle
import re
from multiprocessing import Pool
from cme_utils.analyze import autocorr
import rdf_com as gr
from get_mol_data import Molecule as Mol
import os
import time

def calc_frame(frame):
    sp = {'molecule':molecule, 
        'model':model, 
        'schedule':schedule,
        'phi':phi,
        'kT':T,
        'frame': int(frame)}
    job = project.open_job(sp)

    try:
        mol = Mol(TRAJ_FILE = cwd +'/traj.dcd', TOP_FILE = cwd+'/restart.hoomdxml', final=False, frame = frame)
        if molecule == 'Perylothiophene':
            mol.identify_molecules(apm = 21, i1 = 4, i2 = 8, i3 = 11)
        if molecule == 'Perylene':
            mol.identify_molecules(apm = 20, i1 = 4, i2 = 8, i3 = 11)
        mol.identify_neighbors(200)

        job.document['psi'] = "{:.2f}".format(mol.order)
        job.document['xi'] = "{:.2f}".format(mol.dot_mean)

        rdf_class = gr.RDF()
        if molecule == 'Perylothiophene':
            r, rdf = rdf_class.calculate(cwd +'/traj.dcd', cwd+ '/restart.hoomdxml', [int(21), int(4), int(8), int(11)], frame = int(frame), one_frame = True)
        if molecule == 'Perylene':
            r, rdf = rdf_class.calculate(cwd +'/traj.dcd', cwd+ '/restart.hoomdxml', [int(20), int(4), int(8), int(11)], frame = int(frame), one_frame = True)
        with job:
            f1 = job.fn('position.dat')
            f2 = job.fn('rdf.dat')
            mol.write_as_array(f1)
            rdf_class.write_data(filename = f2, as_array = True)
    except Exception:
        print(cwd)
        print("Error:"+str(IOError))
        pass

start_time = time.time()
project = signac.init_project('NeuralNetwork', '/home/erjank_project/evanmiller326/Neural_Network')
with open('/home/erjank_project/evanmiller326/Neural_Network/Transfer/directs.txt') as f:
    lines = f.read().splitlines()
size = len(lines)
count = 0
for line in lines:
    cwd = line
    path_list = line.split(os.sep)
    molecule = path_list[6]
    schedule = path_list[7]
    model = path_list[8]
    phi = float(path_list[9][3:])
    T = float(re.findall("-T(\d+.\d+)", path_list[10])[0])
    t = autocorr.autocorrelation(log_file = line+'/log')
    frames = np.arange(t[0]+1, t[1]*t[2]+t[0], t[1])
    with Pool() as pool:
        pool.map(calc_frame, frames)
    count+=1
    print("{:.2f} % done!".format(count/size*100))
print("Run time {:.2f} s".format(time.time()- start_time))
