import numpy as np
import signac
import os
import time
import tensorflow as tf
import pickle
from multiprocessing import Pool
import sys
import random

def NN_calls(syst):
    pos, ooplane, inplane, botho, reorder_pos = parse_struc()
    run_all(pos, ooplane, inplane, botho, reorder_pos, syst)

def run_all(pos, ooplane, inplane, botho, reorder_pos, sysr):
    """
    Function to call the NN for each metric and result.
    Requires:
        Arrays:
            Center-of-Mass
            Out of Plane Orientation
            In Plane Orientation
            Out of and In Plane Orientations
            A reordering of Center-of-Mass or orienations
        String:
            The system being investigated (e.g. "rpe")
    """
    results = {'the_Five_Phases': phase,
            'the_Four_Phases' : phase4,
            'the_Three_Phases' : phase3,
            'Ordered_Disordered_phases' : od
            }
    metrics = {'Radial_Distribution_Function' : rdf,
            'Center-of-Mass_and_Orientation' : posit,
            #'Reorganized-Center-of-Mass_and_Orientation' : reorder_pos,
            'Center-of-Mass' : pos,
            #'Out_of_Plane_Orientation' : ooplane,
            #'In_Plane_Orientation' : inplane,
            'Molecule_Orientations' : botho
            }
    for metric in metrics:
        for result in results:
            acc, predicted_data, test_labels, test_phi, test_T = simple(plots = metrics[metric], 
                    labels = results[result], 
                    n=len(metrics[metric][0]), 
                    n_classes=len(results[result][0]))
            print("The NN had an accuracy of {} when using the {} metric to distinguish between {}".format(acc, metric, result))
            graphs = zip(predicted_data, test_labels.tolist(), test_phi.tolist(), test_T.tolist())
            write(syst, acc, metric, result)
            write_graphs(graphs, metric, result, syst)

def write(system, acc, metric, result):
    """
    Writes the data to compare accuracies between
    systems.

    Requires:
        System - String
        Accuracy - Float
        Metric - String (e.g. "Radial Distribution Function")
        Result - String (e.g. "Five Phases")

    Returns:
        None

    """
    with open(folder+f_steps+'comparison.txt', "a") as myfile:
        myfile.write("{} {} {} {}\n".format(system, acc, metric, result))

def write_graphs(lst, metric, result, system):
    """
    Creates a .txt document to be read for plotting the 
    phase diagram of the evaluation data.
    Requires:
        List - containing [Actual phase, NN predicted phase, phi, T]
        Metric - String (e.g. "Radial Distribution Function")
        Result - String (e.g. "Five Phases")
        System - String (e.g. "rpe")
    Returns:
        None
    """
    if os.path.isfile(folder+f_steps+system+"_"+metric+"_"+result+'.txt'):
        os.remove(folder+f_steps+system+"_"+metric+"_"+result+'.txt')
    with open(folder+f_steps+system+"_"+metric+"_"+result+'.txt', "a") as myfile:
        for line in lst:
            #print("{} {} {} {}".format(line[0], line[1].index(1), line[2][0], line[3][0]))
            myfile.write("{} {} {} {}\n".format(line[0], line[1].index(1), line[2][0], line[3][0]))

def parse_struc():
    """
    Parses the raw position data to separate it into
    Center of Mass, In/Out of Plane orientation,
    and rearrange the structural data.
    Requires:
        No arguments, but global variable 'posit'
        containing sturctural data must be set.
    Returns:
        Arrays:
            Center-of-Mass
            Out of Plane Orientation
            In Plane Orientation
            Out and In Plane Orientations
            Rearranged positional and orientational data.
    """
    pos, ooplane, inplane, botho = [], [], [], []
    for row in posit:
        pos.append(np.array([row[i:i+3] for i in range(0, len(row), 9)]).flatten())
        ooplane.append(np.array([row[i:i+3] for i in range(3, len(row), 9)]).flatten())#Out of Plane
        inplane.append(np.array([row[i:i+3] for i in range(6, len(row), 9)]).flatten())# In Plane
        botho.append(np.array([row[i:i+6] for i in range(3, len(row), 9)]).flatten())# Both Out of and In Plane Orientations
    reorder_pos = np.hstack((np.array(pos), np.array(ooplane), np.array(inplane))) # Reorganized positional/orienational data.
    return np.array(pos), np.array(ooplane), np.array(inplane), np.array(botho), np.array(reorder_pos)

def collate(job):
    """
    Opens a signac job and reads job document to
    select position and g(r) data and create arrays
    to represent state points.
    Requires:
        signac job
    Returns:
        List of arrays
    Will only return data if it is for the system
    specified by the 'check' global variable.
    """
    model_dic = {'fpe':'Flexible', 'rpe':'Rigid', 'fpt':'Flexible', 'rpt':'Rigid', 'all':'all'}
    molecule_dic = {'fpe':'Perylene', 'rpe':'Perylene', 'fpt':'Perylothiophene', 'rpt':'Perylothiophene', 'all':'all'}
    if job.statepoint()['molecule'] == molecule_dic[check] or check == 'all':
        if job.statepoint()['model'] == model_dic[check] or check == 'all':
            #Gets the positions and rdf from the job.
            posit = np.load(job.fn('position.dat')).flatten()
            rdf = np.load(job.fn('rdf.dat'))[:100,1]
            '''Parse the job.document and create arrays with 
            decreasing phase detail where the position
             of the 1 in the array represents the phase.'''
            phase_labels = np.zeros(5)
            phase_labels[int(job.document['phase'])] = 1.
            phase4_labels = np.zeros(4)
            phase4_labels[int(job.document['phase4'])] = 1.
            phase3_labels = np.zeros(3)
            phase3_labels[int(job.document['phase3'])] = 1.
            od_labels = np.zeros(2)
            od_labels[int(job.document['ord_v_dis'])] = 1.
            #Get T and phi for the phase diagram construction/comparison.
            T = float(job.statepoint()['kT'])
            phi = float(job.statepoint()['phi'])
            if check != 'all':
                if phi >= 0.1 and phi <= 0.7:
                    phi = phi + (random.random()-0.5) * 0.15
                else:
                    phi = phi + (random.random()-0.5) * 0.11
                if job.statepoint()['model'] == 'Rigid':
                    T =  T + (random.random()-0.50)
                if job.statepoint()['model'] == 'Flexible':
                    if T <= 7 and job.statepoint()['molecule'] =='Perylene':
                        T = T + (random.random()-0.50)
                    else:
                        T = T + (random.random()-0.50)*0.5
            #print([posit, rdf, phase_labels, od_labels, phase4_labels, phase3_labels, T, phi])
            return [posit, rdf, phase_labels, od_labels, phase4_labels, phase3_labels, T, phi]

def simple(plots, labels,n,n_classes):
    #create model
    x = tf.placeholder(tf.float32,[None,n])
    W = tf.Variable(tf.zeros([n,n_classes]))
    b = tf.Variable(tf.zeros([n_classes]))
    y = tf.matmul(x,W)+b

    #define loss and optimizer
    y_ = tf.placeholder(tf.float32,[None,n_classes])
    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))
    train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

    #start session
    sess = tf.Session()
    init = tf.global_variables_initializer()
    sess.run(init)

    #train
    n = len(plots)
    print("Attempting to train from {} points.".format(n))
    h = n//2
    #f = .8
    f = float(sys.argv[1])
    d = int(n*f/2)
    print("Starting to train")
    training_plots = plots[h-d:h+d]
    training_labels = labels[h-d:h+d]
    steps = int(sys.argv[2])
    print(f, steps)
    for _ in range(steps):
        sess.run(train_step, feed_dict={x:training_plots,y_:training_labels})

    #test trained model
    print("Starting to test")
    test_plots = np.vstack((plots[:h-d],plots[h+d:]))
    test_phi =  np.vstack((phis[:h-d],phis[h+d:]))
    test_T =  np.vstack((Ts[:h-d],Ts[h+d:]))
    test_labels = np.vstack((labels[:h-d],labels[h+d:]))
    correct = tf.equal(tf.argmax(y,1),tf.argmax(y_,1))
    accuracy = tf.reduce_mean(tf.cast(correct,tf.float32))
    T = sess.run(accuracy,feed_dict={x:test_plots,y_:test_labels})
    prediction = tf.argmax(y,1)
    predicted_data = prediction.eval(feed_dict={x:test_plots}, session = sess) 
    return T, predicted_data, test_labels, test_phi, test_T

def write_outputs(array, system):
    """
    Separates the data for the various metrics and results
    created during the signac data collation. 
        Takes in:
            Array - containing collated data.
            System name: Rigid/Flexible, Perylene/Perylothiophene.
        Returns:
            None - But dumps '.dat' file in the folder
            specified globally.
    """
    #Separate the data by turning the list into an array
    #Then using array slicing to create separate arrays.
    results = np.array(array)
    posit = np.array(results[:,0])
    rdf = np.array(results[:,1])
    phase_labels = np.array(results[:,2])
    od_labels = np.array(results[:,3])
    phase4_labels = np.array(results[:,4])
    phase3_labels = np.array(results[:,5])
    Ts = np.array(results[:,6])
    phis = np.array(results[:,7])

    #Uses pickle? to dump data as binary array.
    posit.dump(folder+system+'_signac_struc.dat')
    rdf.dump(folder+system+'_signac_rdf.dat')
    phase_labels.dump(folder+system+'_signac_phase.dat')
    od_labels.dump(folder+system+'_signac_od.dat')
    phase4_labels.dump(folder+system+'_signac_phase4.dat')
    phase3_labels.dump(folder+system+'_signac_phase3.dat')
    Ts.dump(folder+system+'_signac_T.dat')
    phis.dump(folder+system+'_signac_phis.dat')
    
if __name__ == '__main__':
    project = signac.get_project(root = '/home/erjank_project/evanmiller326/Neural_Network/')#used by signac to find data.
    systems = ['fpe', 'rpe', 'fpt', 'rpt', 'all']
    folder = '/home/erjank_project/evanmiller326/Neural_Network/T_phi_data_files/'#gives path to where data should be saved.
    if os.path.isfile(folder+'comparison.txt'):#comparision.txt stores the actual and NN predicted evaluation data.
        os.remove(folder+'comparison.txt')#removes the comparison data to start a new plot.
    f = sys.argv[1]
    steps = sys.argv[2]
    f_steps = 'f{}_steps{}/'.format(f, steps)
    if os.path.isdir(folder + f_steps) is False:
        os.mkdir(folder + f_steps)
    new_run = False #Used if data needs to be compiled for the first time.
    for syst in systems:#iterates through all of the systems.
        check = syst #Creates global variable so it can be seen by functions.
        re_run = False #Will overwrite existing data.
        if new_run or re_run: #Data will be recompiled if either are true.
            start_time = time.time() #Helps keep track of run time.
            results = Pool().map(collate, project) #Runs the collation of data in parallel.
            filtered = [x for x in results if x is not None]
            random.shuffle(filtered) #Shuffle the list or only some state points are tested.
            #If the system isn't the  current on of interest it will 
            #return 'None'. filtered removes those from the list.
            write_outputs(filtered, syst)
            #Separates data and saves it into its own array so this loop doesn't need
            #to be done for subsequent runs.
            run_time = (time.time()-start_time)/60
            print("It took {:.2f} minutes to compile the {} data.".format(run_time, syst))
            #Prints how long it took to do the data compilation.

        start_time = time.time()
        #Start the timer again for the NN portion.
        '''Reload in the data for the Neural Network to read
        Creates them as global variables so they can be 
        utilized by the Neural Network.'''
        posit = np.vstack(np.load(folder+syst+'_signac_struc.dat'))
        rdf = np.vstack(np.load(folder+syst+'_signac_rdf.dat'))
        phase = np.vstack(np.load(folder+syst+'_signac_phase.dat'))
        od = np.vstack(np.load(folder+syst+'_signac_od.dat'))
        phase4 = np.vstack(np.load(folder+syst+'_signac_phase4.dat'))
        phase3 = np.vstack(np.load(folder+syst+'_signac_phase3.dat'))
        Ts = np.vstack(np.load(folder+syst+'_signac_T.dat'))
        phis = np.vstack(np.load(folder+syst+'_signac_phis.dat'))

        NN_calls(syst) #Call the Neural Network.

        run_time = (time.time()-start_time)/60
        print("It took {:.2f} minutes to run the NN script.".format(run_time))
