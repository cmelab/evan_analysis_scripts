import numpy
import signac
import random as rd
from multiprocessing import Pool
import os
import time

"""This script is meant to take in a series 
of rdfvalues and determine at which
point the RDF for other temperatures
is no longer distinguishable
from the highest temperature.
This should work tell the vapor transition
if the highest temperature run is vapor"""

def get_edges(array):
    record_a = True
    for i, line in enumerate(array):
        val = line[0]
        if val >= 0.20 and record_a == True:
            a = i
            record_a = False
        if val >= 0.50:
            b = i
            break
    return a, b

def calc_vap(job, phi, T):
    rdf = numpy.load(job.fn('rdf.dat'))
    a, b = get_edges(rdf)
    cut_off = numpy.mean(rdf[a:b][:,1])
    cut_dev = numpy.std(rdf[a:b][:,1])
    job.document['rdf_cutoff'] = "{:.2f}".format(cut_off)
    job.document['rdf_dev'] = "{:.2f}".format(cut_dev)
    if (cut_off-1.0) <= 0.05+0.10/phi:
        vap = 0
    else:
        vap = 1
    job.document['vapor'] = vap
    return vap

def det_vap(array):
    if array[2] == 1:
        phase, od, phase4 = 4., 1., 2.0
        return phase, od, phase4
    else:
        phase, od, phase4 = 3., 1., 3.
        return phase, od, phase4

def det_den(array):
    if array[3] <= 0.5:
        phase, od, phase4 = det_vap(array)
        return phase, od, phase4
    else:
        phase, od, phase4 = 2., 1., 2.
        return phase, od, phase4

def det_order(array):
    if array[1] < 0.4:
        phase, od, phase4 = det_den(array)
        if od == 2.0:
            print("Out of bounds in order")
        return phase, od, phase4
    else:
        phase, od, phase4 = 1., 0., 1.
        return phase, od, phase4

def det_ecl(array):
    if array[0] < 0.85:
        phase, od, phase4 = det_order(array)
        if od == 2.0:
            print("Out of bounds in ecl")
        return phase, od, phase4
    else: 
        phase, od, phase4 = 0., 0., 0.
        return phase, od, phase4

datum = {0.:'mo', 1.:'ro', 2.:'co', 3.:'bo', 4.:'go'}

def write_plot(array):
    with open("/home/erjank_project/evanmiller326/Neural_Network/test.txt", "a") as myfile:
            myfile.write("{} {} {} {} {}\n".format(array[0]
                , array[1]
                , array[2]
                , array[3]
                , array[4]))

def calc_phase(job):
    phi = float(job.statepoint()['phi'])
    T = float(job.statepoint()['kT'])
    psi = float(job.document['psi'])
    xi = float(job.document['xi'])
    if overwrite == True or 'phase' not in job.document:
        vap = calc_vap(job, phi, T)
        params = numpy.array([xi, psi, vap, phi])
        phase, od, phase4 = det_ecl(params)
        job.document['phase'] = phase
        job.document['ord_v_dis'] = od
        job.document['phase4'] = phase4
        phase, od, phase4 = float(job.document['phase']), float(job.document['ord_v_dis']),  float(job.document['phase4'])
    else:
        phase, od, phase4 = float(job.document['phase']), float(job.document['ord_v_dis']),  float(job.document['phase4'])
    if phase == 2. or phase == 3. or phase == 4.:
        job.document['phase3'] = 2.
    else: 
        job.document['phase3'] = phase
    if od == 2.0:
        print("Help! I'm out of bounds!")
    if phi >= 0.1 and phi <= 0.7:
        upphi = phi + (rd.random()-0.5) * 0.2
    else:
        upphi = phi + (rd.random()-0.5) * 0.15
    if job.statepoint()['model'] == 'Rigid':
        upT =  T + (rd.random()-0.50)
    if job.statepoint()['model'] == 'Flexible':
        if T <= 7 and job.statepoint()['molecule'] =='Perylene':
            upT =  T + (rd.random()-0.50)
        else:
            upT =  T + (rd.random()-0.50)*0.5
    write_plot([upphi
        , upT
        , phase
        , job.statepoint()['molecule']
        , job.statepoint()['model']])
    #state_points.append([phi, T])
    return upphi, upT, phase

def count_points(array):
    dt = numpy.dtype((numpy.void, array.dtype.itemsize * array.shape[1]))
    b = numpy.ascontiguousarray(array).view(dt)
    unq, cnt = numpy.unique(b, return_counts=True)
    unq = unq.view(array.dtype).reshape(-1, array.shape[1])
    for i, val in enumerate(unq):
        print(val, cnt[i])

start_time = time.time()
project = signac.get_project(root = '/home/erjank_project/evanmiller326/Neural_Network/')
state_points = []

if os.path.isfile('/home/erjank_project/evanmiller326/Neural_Network/test.txt'):
    os.remove('/home/erjank_project/evanmiller326/Neural_Network/test.txt')

overwrite = True
with Pool(12) as pool:
    pool.map(calc_phase, project)

run_time = (time.time()-start_time)/60
print("It took {:.2f} minutes to run the phase script.".format(run_time))
