"""Takes in a file with data in columns and plots the x,y, and error bars."""
import numpy as numpy
import matplotlib.pyplot as plt
from sys import argv

data = numpy.loadtxt(argv[1], skiprows = 10)

x = numpy.arange(0, len(data[:,6]))
#x = data[:,0]
y = (data[:,6] - numpy.mean(data[:,6]))/numpy.std(data[:,6])

plt.plot(x,y)
#xlab = input("X-label ->  ")
#ylab = input("Y-label ->  ")
plt.xlabel("Timesteps")
plt.ylabel("Potential Energy")
plt.savefig('plot.pdf')
