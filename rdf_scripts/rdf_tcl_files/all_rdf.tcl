set start [lindex $argv 0]
set step [lindex $argv 1]
set rdffile [open "carbon.txt" w]

set sel [atomselect top "all"]

set five [measure gofr $sel $sel usepbc 1 first $start last -1 step $step delta 0.05 rmax 10.0]

set alpha [lindex $five 0] 
set beta [lindex $five 1]
foreach a $alpha b $beta {
    puts $rdffile "$a $b" }

exit
