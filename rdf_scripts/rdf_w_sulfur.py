#!/usr/bin/python
from sys import argv
from numpy import *
import numpy as np
import matplotlib.pyplot as plt

text = loadtxt(argv[1])

#To skip a row: elimnates the name row.
text = loadtxt(argv[1], skiprows = 1)

#Splitting up the data:
r = text[:,0]
row1 = text[:,1]
row2 = text[:,2]
row3 = text[:,3]
row4 = text[:,4]
row5 = text[:,5]
row6 = text[:,6]
row7 = text[:,7]

average = np.mean([row1,row2,row3,row4,row5,row6],axis = 0)
deviation = np.std([row1,row2,row3,row4,row5,row6],axis = 0)

#Doing the Graph
plt.plot(r,average,label='C-ring')
plt.plot(r,row7,label = 'Sulfurs')
plt.ylabel('r')
plt.xlabel('g(r)')
plt.legend()
plt.title('Radial Distribution Function')
plt.savefig('rdf.png')
