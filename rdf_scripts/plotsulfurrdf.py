from sys import argv
import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt(argv[1])

rdf = data[:,0]
r = data[:,1]

d2 = np.loadtxt(argv[2])

fun = d2[:,0]
funr = d2[:,1]
funner = d2[:,2]
average = (np.array(funr) + np.array(funner))/2

plt.plot(rdf,r)
plt.plot(fun,average)
plt.ylabel('g(r)')
plt.xlabel('r')
plt.title('Radial Distribution Function')
plt.show()
