#!/Users/evanmiller/anaconda/bin/python
import sys 
import numpy 
import matplotlib.pyplot as plt
import re

data = []
pfit = []
temp = []
n = len(sys.argv)
for i in range(1,n):
    rdf_argv = str(sys.argv[i])
    lab = re.findall(r'rdf-(.*)', rdf_argv)
    data.append(numpy.loadtxt(sys.argv[i], skiprows = 1 ))
    l = len(data[-1])
    x = numpy.array(data[-1][:l/2,0])
    y = numpy.array(data[-1][:l/2,1])
    plt.plot(x,y, label=lab[0])
    #plt.title('rdf')
    plt.ylabel('g(r)')
    plt.xlabel('r ($\sigma$)')
    plt.xlim([0,5])
    plt.legend(loc = '1', ncol = 3, fontsize = "xx-small")
    #plt.legend(loc = 'upper left', bbox_to_anchor=(0.9,0.5))
plt.savefig("rdf.pdf")
plt.show()

#Bash loop that helps:
"""
ARGS=""; while read  l; do ARGS="$ARGS *-T$l/rdf.txt"; done < temps.txt; python ~/Projects/evan_analysis_scripts/rdfplt.py $ARGS
"""
