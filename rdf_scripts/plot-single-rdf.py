import numpy
import matplotlib.pyplot as plt
import os
import re

rdf = numpy.loadtxt("carbon.txt")

rdf[:,0] = rdf[:,0]*3.2

record_a = True
record_b = True
for i, line in enumerate(rdf):
    val = line[0]
    if val <= 3.3:
        a = i
        record_a = False
    if val <= 3.7:
        b = i
        record_b = False

pi = numpy.amax(rdf[a:b][:,1])
for i in range(len(rdf)):
    if rdf[i,1] == pi:
        coords = [rdf[i,0], rdf[i,1]]

directory = os.getcwd().split('/')[-1]
phi = re.findall(r'phi(.*?)-',directory)
phi = phi[0]
temp = directory.rpartition('T')[-1]
title = phi + "-T" + temp
plt.plot(rdf[:,0], rdf[:,1])
plt.annotate("%s, %.2f" %(coords[0], coords[1]), xy=(coords[0], coords[1]), xytext = (coords[0]+1, coords[1]+1), arrowprops=dict(facecolor='black', width = 1, headwidth = 5, shrink=0.08))
plt.title("$\phi$"+title)
plt.xlabel('r (${\AA})$')
plt.ylabel('g(r)')
plt.xlim([0,12])
plt.savefig("all_rdf.pdf")
