"""This is meant to parse the text file and separate phi values.
Then plot them together."""

import numpy
import matplotlib.pyplot as plt

colormap = plt.cm.brg
#phi_values=[0.01, 0.12, 0.37, 0.61, 0.85, 1.04, 1.22, 1.40, 1.59, 1.77]
plt.gca().set_color_cycle([colormap(i) for i in numpy.linspace(0, 0.9, len(phi_values))])
total_phi = numpy.loadtxt("column_order.txt")
for value in range(len(phi_values)):
    parse_phi = []
    for line in range(len(total_phi)):
        if phi_values[value] == total_phi[line][0]:
            parse_phi.append([float(total_phi[line][1]), float(total_phi[line][2])])
    parse_phi = numpy.array(parse_phi)
    parse_phi.view('f8,f8').sort(order=['f0'],axis=0)
    for line in range(1,len(parse_phi)):
        if parse_phi[-line,1] >= 0.90:
  #          print parse_phi[-line,0]
            break
  #  print parse_phi
    plt.plot(parse_phi[:,0], parse_phi[:,1], label = r'$\rho$='+str(phi_values[value]))

legend = plt.legend(fontsize=11, bbox_to_anchor = (0.40,0.42), ncol = 2, handlelength = 0.5, title = r'($\rho$ in g/cm$^{3}$)')
plt.setp(legend.get_title(),fontsize=11)
#plt.legend(fontsize=11, loc = 'lower left', ncol = 2)
plt.axhspan(0.90, 0.90, color = 'k', linestyle = 'dashed', alpha = 0.7)
plt.xlabel("Temperature (Unitless)")
plt.ylabel("${\\xi}$")
#plt.title("Intracolumnar Order")
plt.savefig("column.pdf", bbox_inches='tight')
#plt.show()

