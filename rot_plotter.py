#!/home/evan/miniconda2/bin/python
"""Takes in a file with data in columns and plots the x,y, and error bars."""
import numpy as numpy
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from sys import argv
import scipy.interpolate

data = numpy.loadtxt(argv[1])
#data = numpy.loadtxt(argv[1], skiprows=1)

x = data[:,0]
y = data[:,1]
z = data[:,2]

#X, Y = numpy.meshgrid(X, Y)
xi = numpy.linspace(x.min(), x.max(), 255)
yi = numpy.linspace(y.min(), y.max(), 255)
zi = scipy.interpolate.griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic')

v = numpy.linspace(-.1, 1.0, 10, endpoint=False)
plt.contourf(xi,yi,zi, 40, cmap=plt.cm.jet)
#plt.title('C$_{5.4}$N$_{2}$Br', y = 1.02)
#plt.title('C$_{3.2}$NO', y = 1.02)
plt.xlabel('Middle Sheet $(^{\circ})$')
plt.ylabel('Top Sheet $(^{\circ})$')
plt.xticks(numpy.linspace(0, 30, 7))
#plt.colorbar(shrink = 0.8)
plt.savefig('potential.pdf')
plt.show()
