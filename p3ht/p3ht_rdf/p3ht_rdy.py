import numpy as np
from freud import box, density
import matplotlib.pyplot as plt

import sys

sys.path.append("/Users/evanmiller/Projects/morphct/morphct/code")
sys.path.append("/Users/evanmiller/Projects/evan_analysis_scripts/helper_functions")
sys.path.append("/Users/evanmiller/Projects/evan_analysis_scripts/p3ht")

import helperFunctions as hf
import utilities as util
import p3ht_helper as ph3lp

import time

def calc_rdf(centers, unitcell, index, nmonomers):
    start = np.array([centers[index]])
    start = start.astype(np.float32)
    if index >= 15:
        first_part = centers[:determine_same_chain(nmonomers, index)-15]
        end = centers[determine_same_chain(nmonomers, index):]
        enc = np.vstack((first_part, end))
    else:
        end = centers[determine_same_chain(nmonomers, index):]
    end = end.astype(np.float32)
    rdf = density.RDF(rmax = 10.0, dr = 0.2)
    fbox = box.Box(Lx = float(unitcell[0][0]), Ly = float(unitcell[0][1]), Lz = float(unitcell[0][2]))
    rdf.compute(fbox, start, end)
    r = rdf.getR()
    y = rdf.getRDF()
    return tuple(r), tuple(y)

def determine_same_chain(num_com, index):
    pair = index+15-index%15
    #if pair >= 15:
    #    print(pair-15, pair)
    return pair

def combine_iterations(data):
    all_grs = np.array(data)
    rs = all_grs[:,0]
    gofrs = all_grs[:,1]
    gr = np.mean(gofrs, axis = 0)
    r = np.mean(rs, axis = 0)
    print(np.std(gofrs, axis=0)/len(gofrs[:,0]))
    return r, gr

def check_neighbors(index, rdf, step):
    left_ave = np.mean(rdf[index-3:index])
    right_ave = np.mean(rdf[index+1:index+4])
    point = rdf[index]
    left_check = abs(left_ave-point)/point
    right_check = abs(right_ave-point)/point
    if left_ave < point and right_ave < point:
        if left_check > 0.05 and right_check > 0.01: 
            return "Maxima"
    if left_ave > point and right_ave > point:
        if left_check > 0.05 and right_check > 0.01: 
            return "Minima"

def add_extrema(r, rdf, fig):
    step = 5
    extremes = []
    for index in range(5,len(rdf)-step-1):
        is_extrema = check_neighbors(index, rdf, step)
        if is_extrema != None:
            extremes.append([r[index], rdf[index], is_extrema])
    return extremes

def plot_rdf(r, rdf, show = False, extrema = False):
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(r,rdf) #10x is nm->Angstrom unit conversion.
    ax.set_xlabel('r ($\sigma$)')
    ax.set_xlim([np.min(r), np.amax(r)])
    ax.set_ylabel('g(r)')
    if extrema:
        extremes = add_extrema(r, rdf, ax)
        print(extremes)
        for ex in extremes:
            ax.plot(ex[0], ex[1], 'o', label = "r={:.2f}".format(ex[0]), markersize = 6)
    ax.legend(fontsize = 10, ncol = 2, loc = 'upper right')
    #plt.savefig("rdf.pdf")
    if show == True:
        plt.show()

def write(r, gr):
    towrite = ""
    for line in zip(r, gr):
        towrite +="{:.2f},{:.2f}\n".format(line[0],line[1])
    with open('rdf.txt', 'w') as f:
        f.writelines(towrite)


#def plot_rdf(r, y):
#    plt.plot(r*3.905, y)
#    plt.xlabel(r"r ($\AA$)")
#    plt.ylabel("g(r)")
#    plt.show()

if __name__ == "__main__":
    start = time.time()
    infile = sys.argv[1]
    xml = hf.loadMorphologyXML(infile)
    unitcell = util.get_box_from_xml(xml)
    positions = np.array(xml['position'])
    centers, orientations = util.get_centers(positions, unitcell, 11)
    ncenters = len(centers)
    grs = []
    for i in range(ncenters-15):
        grs.append(calc_rdf(centers, unitcell, i, ncenters))
    r, gr = combine_iterations(grs)
    print(time.time()-start)
    write(r, gr)
    #plot_rdf(r, gr, show=True, extrema=True)
