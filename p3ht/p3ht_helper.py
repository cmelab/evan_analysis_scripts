import numpy as np

def check_alphabetical(constraint):
    reorder = False
    first_type = constraint[0].split("-")[0]
    last_type = constraint[0].split("-")[-1]
    if first_type > last_type:
        reorder = True
    if len(constraint[0].split("-")) == 4 and first_type == last_type:
        second_type = constraint[0].split("-")[1]
        third_type = constraint[0].split("-")[2]
        if second_type > third_type:
            reorder = True

    if reorder:
        purged = [val for i, val in enumerate(constraint) if i > 0]
        purged = list(reversed(purged))
        types = constraint[0].split("-")
        types = list(reversed(types))
        if len(types) == 2:
            new_constraint = ["{}-{}".format(types[0], types[1])]+purged
        if len(types) == 3:
            new_constraint = ["{}-{}-{}".format(types[0], types[1], types[2])]+purged
        if len(types) == 4:
            new_constraint = ["{}-{}-{}-{}".format(types[0], types[1], types[2], types[3])]+purged
        return new_constraint
    else: 
        return constraint

def updateBonds(xml):
    newBonds = []
    for bond in xml['bond']:
        index1 = bond[1]
        index2 = bond[2]
        typeA = xml['type'][index1]
        typeB = xml['type'][index2]
        newBond = "{}-{}".format(typeA, typeB)
        bond = check_alphabetical([newBond, index1, index2])
        newBonds.append(bond)
    xml['bond'] = newBonds
    return xml

def updateAngles(xml):
    newAngles = []
    for angle in xml['angle']:
        index1 = angle[1]
        index2 = angle[2]
        index3 = angle[3]
        typeA = xml['type'][index1]
        typeB = xml['type'][index2]
        typeC = xml['type'][index3]
        newAngle = "{}-{}-{}".format(typeA, typeB, typeC)
        angle = check_alphabetical([newAngle, index1, index2, index3])
        newAngles.append(angle)
    xml['angle'] = newAngles
    return xml

def updateDihedrals(xml):
    newDihedrals = []
    for dihedral in xml['dihedral']:
        index1 = dihedral[1]
        index2 = dihedral[2]
        index3 = dihedral[3]
        index4 = dihedral[4]
        typeA = xml['type'][index1]
        typeB = xml['type'][index2]
        typeC = xml['type'][index3]
        typeD = xml['type'][index4]
        newDihedral = "{}-{}-{}-{}".format(typeA, typeB, typeC, typeD)
        dihedral = check_alphabetical([newDihedral, index1, index2, index3, index4])
        newDihedrals.append(dihedral)
    xml['dihedral'] = newDihedrals
    return xml

def updateConstraints(xml):
    xml = updateBonds(xml)
    xml = updateAngles(xml)
    xml = updateDihedrals(xml)
    return xml

def rewriteTypes(lst, typeMap):
    import regex as re

    def remove_numbers(name):
        return re.search('^[A-Z]*', name).group(0)

    newTypes = [remove_numbers(name) for name in lst]
    return newTypes

def check_off_set(xml):
    """
    Uses Matty's xml parser to find the indices for
    CA atoms and determine if the thiophene rings
    are first in the simulation.
    Then gives the off_set: 0 if thiophene rings are first
    or the number of bodies until thiophene rings occur.
    Requires:
        None
    Returns:
        off_set - integer
    """
    first_C = xml['type'].index('CA')
    thiophene_bodies = list(set([xml['body'][i] for i,val in enumerate(xml['type']) if val in ['CA', 'S1', 'S2', 'S']]))
    off_set = min(thiophene_bodies)
    return off_set

def TCLscript(neighbors, clusters, xml, location, name = None):
    """
    Creates a tcl script for each identified cluster.
    Works for the P3HT molecule.
    """
    off_set = check_off_set(xml)
    if off_set != 0:
        print("Detecting that p3ht isn't first in the system.")
        print("WARNING! this hasn't been tested!")
    forbidden = np.arange(10, 1000, 32)
    colors = [x for x in range(0, 1000) if x not in forbidden]
    tclLinesToWrite = ['mol delrep 0 0\n']
    tclLinesToWrite += ['pbc wrap -center origin\n'] 
    tclLinesToWrite += ['pbc box -color black -center origin -width 4\n']
    tclLinesToWrite += ['display resetview\n']
    tclLinesToWrite += ['color change rgb 9 1.0 0.29 0.5\n']
    tclLinesToWrite += ['color change rgb 16 0.25 0.25 0.25\n']
    count = 0
    smalls = []
    for i in range(len(neighbors)):
        clust = []
        inclust = ""
        for j,c in enumerate(clusters):
            if c==i:
                for k in range(off_set+j, off_set+j+1):
                    inclust +=str(k)+" "
                    clust.append(k)
        if inclust !="" and len(inclust.split(" ")) >= 60:
            tclLinesToWrite += ['mol material AOEdgy\n']
            tclLinesToWrite +=['mol color ColorID '+str(colors[count%32])+'\n']
            tclLinesToWrite +=['mol representation VDW 0.9 8.0\n']
            tclLinesToWrite += ['mol selection resid '+str(inclust)+'\n']
            tclLinesToWrite += ['mol addrep 0\n']
            count += 1
        else:
            for single in clust:
                smalls.append(clust)
    smalls = list(set([item for sublist in smalls for item in sublist]))
    inclust = ""
    for small in smalls:
        inclust +=str(small)+" "
    tclLinesToWrite += ['mol material Glass1\n']
    tclLinesToWrite +=['mol color ColorID 15\n']
    tclLinesToWrite +=['mol representation VDW 0.9 8.0\n']
    tclLinesToWrite += ['mol selection resid '+str(inclust)+'\n']
    tclLinesToWrite += ['mol addrep 0\n']
    tclLinesToWrite +=['mol color ColorID 10' + '\n']
    #
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type FCA' + '\n']
    tclLinesToWrite +=['mol material Transparent' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclLinesToWrite +=['mol color ColorID 10' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type FCT' + '\n']
    tclLinesToWrite +=['mol material Transparent' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclLinesToWrite +=['mol color ColorID 1' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type FO' + '\n'] 
    tclLinesToWrite +=['mol material Transparent' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    if name == None:
        tclFileName = location+'/c_color.tcl'
    else:
        tclFileName = location+'/{}.tcl'.format(name)
    with open(tclFileName, 'w+') as tclFile:
        tclFile.writelines(tclLinesToWrite)
    print('Tcl file written to', tclFileName)

if __name__ == "__main__":
    print("Helper functions specifically for the p3ht system.")
