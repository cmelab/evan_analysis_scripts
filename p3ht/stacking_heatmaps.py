import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import griddata

#data = np.loadtxt('fullerene_cluster.txt')
from sys import argv
try:
    infile = argv[1]
    data = np.loadtxt(infile)
    name = argv[2]
except:
    print("No infile supplied, reverting to cluster_data.txt")
    data = np.loadtxt('cluster_data.txt')
    name = ""
    pass

def calculate_density(phi):
    MW = (15*(10*12.011+32.07+14*1.008)+2)/6.022e23*100
    V = 0.524*(16500)/float(phi)
    V*=((3.905e-8)**3)
    #V /=6.02223e23
    #float(phi)/((3.905e-8)**3)
    return MW/V

def calc_T(T):
    T *=0.32*4184/8.314
    return T

def add_noise_to_zero_values(array):
    return np.array([value + np.random.ranf()*0.01 for value in array])


for phi in set(data[:,2]):
    rho = calculate_density(phi)
    new_list = []
    for run in data:
        if run[2] == phi:
            new_list.append(run)
    new_list = np.array(new_list)
    x = new_list[:,1]
    y = calc_T(new_list[:,3])
    z = new_list[:,0]
    z = add_noise_to_zero_values(z)
    #xi = np.linspace(x.min(), x.max(), 20)
    #yi = np.linspace(y.min(), y.max(), 20)
    xi, yi = np.mgrid[x.min():x.max():20j, y.min():y.max():20j]
    zi = griddata((x,y), z, (xi, yi), method = 'linear')

    plt.subplot(111)
    plt.scatter(x, y, c = 'k', marker = 'x', zorder = 2)
    #plt.contourf(xi, yi, zi, 20, cmap = plt.cm.get_cmap('rainbow'), zorder = 1)
    plt.contourf(xi, yi, zi, 20, cmap = plt.cm.get_cmap('rainbow'), vmax = 1.0, vmin = 0.0, clim=[0,1], zorder = 1)
    plt.xlabel(r'$\varepsilon_s$')
    plt.ylabel('Temperature (K)')
    plt.title(r'$\rho$={:.2f} g/cm$^3$'.format(rho), y=1.01)
    #plt.title(r'$\phi$={}'.format(phi))
    plt.xlim([np.min(x), np.max(x)])
    plt.xticks([0.2,0.4,0.6,0.8,1.0,1.2])
    plt.ylim([np.min(y), np.max(y)])
    bar = plt.colorbar()
    bar.set_ticks([0, 0.2, 0.4, 0.6, 0.8, 1.0])
    bar.draw_all()
    plt.savefig('{}phi_{}.pdf'.format(name, phi))
    plt.savefig('{}phi_{}.png'.format(name, phi), transparent=True)
    plt.close()
