import argparse
import numpy as np

#TODO Add species logic and morphct compatibility.

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import fresnel
import matplotlib
import PIL

import sys
sys.setrecursionlimit(10000)

def plot_bars_comparison():
    plt.close()

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    yticks = np.linspace(0.2, 0.8, 6)

    for cut_off in yticks:
        cryst, cryst_pos = get_TIs_from_external('cryst_frame_10.npz')
        cryst_n = build_neighbors(cryst, cut_off)
        cryst_c = make_clusters(cryst_n)
        cryst_sizes = [cryst_c.count(i) for i in range(len(cryst_c)) if cryst_c.count(i) > 0]
        new_list = np.array([[cryst_sizes.count(i), i] for i in list(set(cryst_sizes))])
        cryst_c_counts = new_list[:,0]
        cryst_c_sizes = new_list[:,1]/np.amax(new_list[:,1])

        semicry, semicry_pos = get_TIs_from_external('semicry_frame_10.npz')
        semicry_n = build_neighbors(semicry, cut_off)
        semicry_c = make_clusters(semicry_n)
        semicry_counts, semicry_bins = np.histogram(semicry_c)

        disordered, disordered_pos = get_TIs_from_external('disordered_frame_10.npz')
        disordered_n = build_neighbors(disordered, cut_off)
        disordered_c = make_clusters(disordered_n)
        disordered_counts, disordered_bins = np.histogram(disordered_c)

        ax.bar(cryst_c_sizes, cryst_c_counts, zs=cut_off, color='r', zdir='y')
    ax.set_xlabel("Mers per crystal")
    ax.set_zlabel("Occurances")
    ax.set_ylabel('Cut Off')
    plt.show()

def TCLscript(neighbors, clusters):
    """
    Creates a tcl script for each identified cluster.
    Works for the P3HT molecule.
    """
    forbidden = np.arange(10, 1000, 32)
    colors = [x for x in range(0, 1000) if x not in forbidden]
    tclLinesToWrite = ['mol delrep 0 0\n']
    tclLinesToWrite += ['pbc wrap -center origin\n'] 
    tclLinesToWrite += ['pbc box -color black -center origin -width 4\n']
    tclLinesToWrite += ['display resetview\n']
    tclLinesToWrite += ['color change rgb 9 1.0 0.29 0.5\n']
    tclLinesToWrite += ['color change rgb 16 0.25 0.25 0.25\n']
    count = 0
    smalls = []
    for i in range(len(neighbors)):
        clust = []
        inclust = ""
        for j,c in enumerate(clusters):
            if c==i:
                for k in range(j, j+1):
                    inclust +=str(k)+" "
                    clust.append(k)
        if inclust !="" and len(inclust.split(" ")) >= 31:
            tclLinesToWrite += ['mol material AOEdgy\n']
            tclLinesToWrite +=['mol color ColorID '+str(colors[count%32])+'\n']
            tclLinesToWrite +=['mol representation VDW 3.0 8.0\n']
            tclLinesToWrite += ['mol selection resid '+str(inclust)+'\n']
            tclLinesToWrite += ['mol addrep 0\n']
            count += 1
        else:
            for single in clust:
                smalls.append(clust)
    #smalls = list(set([item for sublist in smalls for item in sublist]))
    #inclust = ""
    #for small in smalls:
    #    inclust +=str(small)+" "
    #tclLinesToWrite += ['mol material Glass1\n']
    #tclLinesToWrite +=['mol color ColorID 15\n']
    #tclLinesToWrite +=['mol representation VDW 0.9 8.0\n']
    #tclLinesToWrite += ['mol selection resid '+str(inclust)+'\n']
    #tclLinesToWrite += ['mol addrep 0\n']
    #tclLinesToWrite +=['mol color ColorID 10' + '\n']
    #
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type FCA' + '\n']
    tclLinesToWrite +=['mol material Transparent' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclLinesToWrite +=['mol color ColorID 10' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type FCT' + '\n']
    tclLinesToWrite +=['mol material Transparent' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclLinesToWrite +=['mol color ColorID 1' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type FO' + '\n'] 
    tclLinesToWrite +=['mol material Transparent' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclFileName = './c_color.tcl'
    with open(tclFileName, 'w+') as tclFile:
        tclFile.writelines(tclLinesToWrite)
    print('Tcl file written to', tclFileName)
def replot_clusters(centers, neighbors, clusters):
    device = fresnel.Device()
    scene  = fresnel.Scene(device = device)
    colors = plt.cm.rainbow(np.linspace(0,1.0, len((set(clusters)))))

    mapper = matplotlib.cm.ScalarMappable(norm = matplotlib.colors.Normalize(vmin=0, vmax=1, clip=True),
                                      cmap = matplotlib.cm.get_cmap(name='rainbow'))

    v = np.linspace(0,1,len(set(clusters)))

    counter = 0

    geometries = {}

    for i in range(len(neighbors)):
        plotPoints = []
        for j, c in enumerate(clusters):
            if c == i:
                plotPoints.append(centers[j])
        if len(plotPoints) > 0:
            geometries[str(i)] = fresnel.geometry.Sphere(scene, position = plotPoints, radius=1.0)
            geometries[str(i)].material = fresnel.material.Material(solid=0.0, color=fresnel.color.linear(colors[counter][:3])*0.9)
            geometries[str(i)].outline_width = 0.2
            counter+=1

    scene.camera = fresnel.camera.fit(scene, view='isometric', margin=0.2)
    tracer = fresnel.tracer.Preview(device=device, w=600, h=600, aa_level=3)
    out = tracer.render(scene)
    picture = tracer.output

    image = PIL.Image.fromarray(picture[:,:,0:4], mode='RGBA')

    image.save("./cluster.png")

def plot_clusters(centers, neighbors, clusters):
    plt.close()
    nclusters = len(set(clusters))
    colors = plt.cm.nipy_spectral(np.linspace(0,1.0, nclusters))
    fig = plt.figure()
    clusterPlot = fig.add_subplot(111, projection='3d')
    counter = 0
    for i in range(len(neighbors)):
        plotPoints = []
        for j, c in enumerate(clusters):
            if c == i:
                plotPoints.append(centers[j])
        if len(plotPoints) > 0:
            plotPoints = np.array(plotPoints)
            clusterPlot.scatter(xs = plotPoints[:,0], ys = plotPoints[:,1], zs = plotPoints[:,2], c = colors[counter], s=4, alpha=1.0)
            counter+=1
    plt.axis('off')
    plt.show()
    return fig, clusterPlot

def make_clusters(neighbor_list):
    """
    Function to call for the creation
    of turning the neighbor list into a 
    cluster list.
    Requires:
        n_list - neighbor list
    Returns:
        c_list - cluster list
    """
    print("Creating Clusters.")
    cluster_list = [i for i in range(len(neighbor_list))]
    for i in range(len(cluster_list)):
        neighbor_list, cluster_list = update_neighbors(i, cluster_list, neighbor_list)
    print("Done.")
    return cluster_list

def update_neighbors(particle, cluster_list, neighbor_list):
    """Recursive function to convert neighborlist into cluster list"""
    for n in neighbor_list[particle]:
        if cluster_list[n]>cluster_list[particle]:
            cluster_list[n] = cluster_list[particle]
            neighbor_list, cluster_list = update_neighbors(n,cluster_list,neighbor_list)
        elif cluster_list[n] < cluster_list[particle]: 
            cluster_list[particle] = cluster_list[n]
            neighbor_list, cluster_list = update_neighbors(particle,cluster_list,neighbor_list)
    return neighbor_list, cluster_list

def build_neighbors(data, cut_off):
    last_chromophore = int(np.amax(data[:,1]))
    neighbor_list = [[] for i in range(last_chromophore+1)]
    for pair in data:
        cA = int(pair[0])
        cB = int(pair[1])
        TI = pair[2]
        if TI >= cut_off:
            neighbor_list[cA].append(cB)
            neighbor_list[cB].append(cA)
        else:
            break
    return neighbor_list

def get_TIs_from_pickle(morphct_pickle, array_file, write_external):

    from morphct.code import helper_functions as hf

    AA_morphology_dict, CG_morphology_dict, CG_to_AAID_master, parameter_dict, chromophore_list = hf.load_pickle(morphct_pickle)

    data = []
    positions = np.array([chromophore.posn for chromophore in chromophore_list])

    for i, chromophore in enumerate(chromophore_list):
        for neighbor in zip(chromophore.neighbours, chromophore.neighbours_TI):
            index1 = i
            index2 = neighbor[0][0]  # Gets the neighbor's index
            TI = neighbor[1]  # Get the transfer integral

            if TI > 0 and index1 < index2:  # Consider only pairs that will have hops.

                datum = [index1,
                    index2,
                    TI]

                data.append(datum) #Write the data to the list

    data = np.array(data)
    data = np.flip(data[data[:,2].argsort()], axis = 0)

    if write_external:
        np.savez(array_file, data=data, positions=positions)

    return data, positions

def get_TIs_from_external(array_file):
    npzfile = np.load(array_file)
    data = npzfile['data']
    positions = npzfile['positions']
    return data, positions

def get_all_TIs(morphct_pickle, array_file, from_external, write_external):
    if from_external:
        data, positions = get_TIs_from_external(array_file)
    else:
        data, positions = get_TIs_from_pickle(morphct_pickle, array_file, write_external)
    return data, positions

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--morphct_pickle", required=False,
            default=None,
            help="""Pass this flag to specify the morphct
            pickle file from which to get the transfer integrals.
            """)
    parser.add_argument("-af", "--array_file", required=False,
            default=None,
            help="""Pass this flag to specify 
            the data pickle to work with.
            """)
    parser.add_argument("-fe", "--from_external", required=False,
            action="store_true",
            help="""Pass this flag to specify to
            get the TIs from the data pickle and skip getting
            data from the pickle.
            """)
    parser.add_argument("-c", "--cut_off", required=False,
            help="""Pass this flag to specify 
            the TI cutoff for two chromophores to be
            considered neighbors.
            """)
    parser.add_argument("-w", "--write_external", required=False,
            action="store_true",
            help="""Pass this flag to specify 
            if you want an external pickle file with chromophoreA,
            chromophore B and TI written to help with testing TI
            cut offs. 
            """)

    args, input_list = parser.parse_known_args()

    from_external = args.from_external
    write_external = args.write_external

    cut_off = 1.0
    if args.cut_off:
        cut_off = float(args.cut_off)

    morphct_pickle = None
    if args.morphct_pickle:
        morphct_pickle = args.morphct_pickle

    array_file = None
    if args.array_file:
        array_file = args.array_file

    data, positions = get_all_TIs(morphct_pickle, array_file, from_external, write_external)
    neighbor_list = build_neighbors(data, cut_off)
    cluster_list = make_clusters(neighbor_list)
    #plot_clusters(positions, neighbor_list, cluster_list)
    #replot_clusters(positions, neighbor_list, cluster_list)
    #TCLscript(neighbor_list, cluster_list)

if __name__ == "__main__":
    #main()
    plot_bars_comparison()
