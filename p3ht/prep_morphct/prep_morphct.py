import sys
sys.setrecursionlimit(int(2e4))
sys.path.append('/Users/evanmiller/Projects/old_morphct/code')
import helperFunctions as hf

from glob import glob
import numpy as np

def identify_first_body(morphology):
    first_indices = []
    for i, val in enumerate(morphology['body']):
        if int(val) == 0:
            first_indices.append(i)
    return first_indices

def calc_distance(check_list, morphology):
    #Get the PBC checker from another script
    sys.path.append("/Users/evanmiller/Projects/evan_analysis_scripts/DBP/")
    from DBP_Helper import pbc

    box = np.array([morphology['lx'], morphology['ly'], morphology['lz']])
    bonds = []

    for pair in check_list:
        a = np.array(morphology['position'][pair[0]])
        b = np.array(morphology['position'][pair[1]])
        dist = np.linalg.norm(pbc(a-b, box))
        if dist < 2.0:
            bonds.append(pair)
    return bonds

def check_bonds_presence(morphology, check_list):
    bond_present =  False
    #Assume the if the first bond isn't present, none are.
    for pair in morphology['bond']:
        alpha = pair[1]
        beta = pair[2]
        a = check_list[0][0]
        b = check_list[0][1]
        if (alpha == a and beta == b) or (alpha == b and beta == a):
            print("Bond found for rigid body")
            bond_present =  True
            break
    return bond_present

def build_bonds_to_add_list(morphology, bonds_needed):
    max_body = max([i for i in morphology['body'] if i != -1 and morphology['body'].count(i) == 5])
    to_add_list =  [[pair[0]+11*body, pair[1]+11*body] for body in range(max_body+1) for pair in bonds_needed]
    for i in range(20):
        print(to_add_list[i])
    return to_add_list

def expand_bonds_in_xml(morphology, bonds_to_add):
    from operator import itemgetter
    bonds = morphology['bond']
    for bond in bonds_to_add:
        species_a = morphology['type'][bond[0]]
        species_b = morphology['type'][bond[1]]
        bonds.append(["{}-{}".format(species_a, species_b), bond[0], bond[1]])
    sorted_list = sorted(bonds, key=itemgetter(1,2,0))
    morphology['bond'] = sorted_list
    return morphology

def identify_and_add_to_morphology(indices, morphology):
    nfirst = len(indices)
    #Find the indices for the first rigid body
    check_list = [[indices[i], indices[j]] for i in range(nfirst-1) for j in range(i+1, nfirst)]
    #See which atoms should be bonded together
    bonds_needed = calc_distance(check_list, morphology)
    #Check to see if the bonds are there
    bonds_present = check_bonds_presence(morphology, bonds_needed)
    if not bonds_present:
        bonds_to_add = build_bonds_to_add_list(morphology, bonds_needed)
        morphology = expand_bonds_in_xml(morphology, bonds_to_add)
        return morphology
    else:
        return morphology

def add_bonds_in_rigid_bodies(morphology):
    indices = identify_first_body(morphology)
    morphology = identify_and_add_to_morphology(indices, morphology)
    return morphology

def Imagefix(morphology):
    sys.path.append('/Users/evanmiller/Projects/old_morphct/utils/fixImages')
    import fixImages as fixI
    morphology = fixI.zeroOutImages(morphology)
    bondDict = fixI.getBondDict(morphology)
    morphology = fixI.checkBonds(morphology, bondDict)
    return morphology

def fineGrain(morphology):
    sys.path.append('/Users/evanmiller/Projects/old_morphct/utils/addHydrogentoUA/')
    import addHydrogens as addH
    hydrogensToAdd, sigmaVal = addH.find_information("P3HT")
    morpholgy = hf.addUnwrappedPositions(morphology)
    hydrogenPositions = addH.calculateHydrogenPositions(morphology, hydrogensToAdd)
    morphology = addH.addHydrogensToMorph(morphology, hydrogenPositions)
    morphology = hf.addWrappedPositions(morphology)
    return morphology

def unify_bodies(morphology):
    bodies = [[i]*11 for i in range(1500)]
    bodies = [i for body in bodies for i in body]
    morphology['body'] = bodies
    return morphology

def find_statepoint(path):
    run_name = path.split('/')[-2]
    end = run_name.split('e_factor-')[-1]
    efactor = end.split('-')[0]
    T = end.split('T')[-1]
    final_dir = "/Users/evanmiller/Projects/morphct/p3ht_inputs/Efac-{}-T-{}-p3ht.xml".format(efactor, T)
    return final_dir

if __name__ == "__main__":
    directories = glob("/Users/evanmiller/Projects/Runs/p3ht/T_phi_e_sweep/e_fac_1.2/phi0.9/Neat-P3HT-sweep-dt0.001-phi0.9-P1.0-e_factor-1.2-n_mol100-T*")
    #for directory in directories:
    #    filename = 'restart.xml'
    #    path = directory + "/" + filename

    #    morphology = hf.loadMorphologyXML(path, sigma = 1.0)

    #    morphology = add_bonds_in_rigid_bodies(morphology)

    #    morphology = Imagefix(morphology)

    #    hf.writeMorphologyXML(morphology, path)

    directories = glob("/Users/evanmiller/Projects/Runs/p3ht/T_phi_e_sweep/phi1.38/Neat-P3HT-shrink-longer-dt0.001-phi1.38-P1.0-e_factor-1.2-n_mol100-T*")
    for directory in directories:
        print(directory)
        filename = 'restart.xml'

        path = directory + "/" + filename

        morphology = hf.loadMorphologyXML(path, sigma = 3.905)

        morphology = add_bonds_in_rigid_bodies(morphology)

        morphology = unify_bodies(morphology)

        morphology = Imagefix(morphology)

        morphology = fineGrain(morphology)

        hf.writeMorphologyXML(morphology, find_statepoint(path))
