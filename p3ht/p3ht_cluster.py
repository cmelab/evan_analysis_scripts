import numpy as np

import sys
sys.path.append("/Users/evanmiller/Projects/old_morphct/code")
import helperFunctions as HF

sys.path.append("/Users/evanmiller/Projects/evan_analysis_scripts/helper_functions")
import utilities as util

import p3ht_helper as p3hlp

from glob import glob
import os

from functools import partial
import multiprocessing

import matplotlib.pyplot as plt
from scipy.stats import norm

def write_order_to_file(order, phi, Temp, e_factor, write_dir = os.getcwd(), filename = "cluster_data.txt"):
    """
    Write the summarized order data to a file.
    The directory will need to be changed for the
    various machines.
    Requires:
        order - float 
        phi - str or float
        Temp - str or float
        e_factor - str or float
    Returns:
        None
    These arguments are used for writing the statepoint
    to the text file for further parsing and plotting.
    """
    write_to_file = "{} {} {} {}".format(order, e_factor, phi, Temp)
    if os.path.exists(os.path.join(write_dir, filename)):
        print(write_to_file)
        with open(filename, 'a') as textFile:
            textFile.write(write_to_file + "\n")
    else:
        print("File not found! Creating a new one!")
        with open(os.path.join(write_dir, filename), 'w+') as textFile:
            textFile.write(write_to_file + "\n")

def quantify_order(c_list, infile, write_order):
    """
    Summarizes over the cluster list to count the 
    number of clusters and their sizes to calculate
    an order parameter 'order' for the simulation.
    Requires:
        c_list - list of clusters
    Returns:
        None
    """
    cluster_counter = []
    total_cluster = []
    for i in set(c_list):
        c_size = c_list.count(i)
        if c_size >= 6:
        #if c_size >= 30:
            cluster_counter.append(c_size)
        total_cluster.append(c_size)
    order = sum(cluster_counter)/sum(total_cluster)
    if write_order:
        state_point = util.get_statepoint(infile)
        phi = state_point['phi']
        T = state_point['T']
        e_factor = state_point['e_factor']
        write_order_to_file(order, phi, T, e_factor)
    else:
        print(order)

def do_clustering(xml, infile, quant_order, write_order, tcl, print_data, plot_orientation):
    molecule = util.xml_filter(xml, 5, ['CA', 'S', 'S1', 'S2'])
    #atoms per molecule = 5 when only CA and S are considered
    molecule.centers, molecule.orientations = util.get_centers(molecule.xyz[0], molecule.unitcell_lengths, 5)
    #nlist = util.build_neighbors(molecule.centers, 
    #        1.7, 
    #        molecule.n_residues, 
    #        molecule.unitcell_lengths,
    #        orientations = molecule.orientations, 
    #        ocut=0.94)
    nlist = util.neighbors_with_freud(molecule.centers, molecule.unitcell_lengths, cut=1.7, orientations = molecule.orientations, ocut = 0.94)
    n_array = np.array(nlist)
    np.save("same_neighbors.npy", n_array)

    if plot_orientation:
        orientation_hist(nlist, molecule.orientations, infile)

    clist = util.make_clusters(nlist)
#    util.print_clusters(nlist, clist)

    if quant_order:
        quantify_order(clist, infile, write_order)
    if print_data:
        util.print_clusters(nlist, clist)
    if tcl:
        p3hlp.TCLscript(nlist, clist, xml, ".", name=sys.argv[2])

def check_species(constraint, constraint_type):
    split = constraint.split('-')
    if constraint_type == 'bond':
        req = 1
    if constraint_type == 'angle':
        req = 3
    if split.count('CT') >= req:
        return True
    else:
        return False

def calc_bond_dist(xml, infile, plot_data = False):
    box = util.get_box_from_xml(xml)
    bonds_to_calc = []
    for bond in xml['bond']:
        if check_species(bond[0], 'bond'):
            bonds_to_calc.append([bond[1], bond[2]])
    distances = []
    for pair in bonds_to_calc:
        a = np.array(xml['position'][pair[0]])
        b = np.array(xml['position'][pair[1]])
        d = np.linalg.norm(util.pbc(a-b, box))
        distances.append(d)
    distances = np.array(distances)

    mu, std = norm.fit(distances)

    if plot_data:
        plt.hist(distances, bins = 50, normed=1)
        xmin, xmax = plt.xlim()
        x = np.linspace(xmin, xmax, 100)
        p = norm.pdf(x, mu, std)
        plt.plot(x, p, 'k')
        plt.title("mu-{:.3f}, std={:.4f}".format(mu, std))
        plt.show()

    state_point = util.get_statepoint(infile)
    phi = state_point['phi']
    T = state_point['T']
    e_factor = state_point['e_factor']
    write_order_to_file(std, phi, T, e_factor, filename="bonds_data.txt")

def calc_angle_dist(xml, infile, plot_data = False):
    box = util.get_box_from_xml(xml)
    angles_to_calc = []
    for angle in xml['angle']:
        if check_species(angle[0], 'angle'):
            angles_to_calc.append([angle[1], angle[2], angle[3]])
    dots = []
    for trio in angles_to_calc:
        a = np.array(xml['position'][trio[0]])
        b = np.array(xml['position'][trio[1]])
        c = np.array(xml['position'][trio[2]])
        v1 = util.pbc(a-b, box)/np.linalg.norm(util.pbc(a-b, box))
        v2 = util.pbc(c-b, box)/np.linalg.norm(util.pbc(c-b, box))
        dots.append(np.arccos(np.dot(v2, v1))*180/np.pi)
    dots = np.array(dots)

    mu, std = norm.fit(dots)

    state_point = util.get_statepoint(infile)
    phi = state_point['phi']
    T = state_point['T']
    e_factor = state_point['e_factor']

    write_order_to_file(-1*std, phi, T, e_factor, filename="angles_data.txt")

def calc_structure(infile, 
        clustering = True,
        bond_dist = False,
        angle_dist = False,
        quant_order = False, 
        write_order = True,
        tcl = False, 
        print_data = False,
        plot_orientation = False):
    xml = HF.loadMorphologyXML(infile)
    if clustering:
        do_clustering(xml, 
                infile, 
                quant_order, 
                write_order,
                tcl, 
                print_data,
                plot_orientation)
    if bond_dist:
        calc_bond_dist(xml, infile)
    if angle_dist:
        calc_angle_dist(xml, infile)

def orientation_hist(nlist, orientations, infile):
    print("Plotting")
    state_point = util.get_statepoint(infile)
    T = state_point['T']
    dot_list = []
    for i, val in enumerate(nlist):
        for j in val:
            if i < j:
                dot = np.dot(orientations[i], orientations[j])
                if dot < 0:
                    dot+= 2.
                dot_list.append(dot)
    hist, bins = np.histogram(np.array(dot_list), bins = 50)
    width = 0.7 * (bins[1] - bins[0])
    center = (bins[:-1] + bins[1:]) / 2
    fig, ax = plt.subplots()
    ax.bar(center, hist, align='center', width=width)
    ax.set_title("T={:.1f}.png".format(T))
    plt.savefig("/Users/evanmiller/Projects/Runs/p3ht/T_phi_e_sweep/phi1.38/hist-{:.1f}.png".format(T))

def calc_for_frames():
   # infiles = glob("/Users/evanmiller/Projects/Runs/p3ht/big/performance_comparison/Neat-P3HT-longer-run-dt0.001-phi0.9-P1.0-e_factor-0.8-n_mol100-T4.0/frames/*80.xml")
    infiles = glob("/Users/evanmiller/Projects/Runs/p3ht/big/P3HT-cont-3-dt0.001-phi0.9-P1.0-e_factor-0.8-n_mol1000-T4.0/frames/*.xml")
    for infile in infiles:
        calc_structure(infile, clustering = True, quant_order=True)

if __name__ == "__main__":
    #calc_for_frames()
    #infile = sys.argv[1]
    #infile = 'restart.xml'
    #infiles = glob("/Users/evanmiller/Projects/Runs/p3ht/T_phi_e_sweep/phi1.38/Neat-P3HT-shrink-longer-dt0.001-phi1.38-P1.0-e_factor-*-n_mol100-T*/restart.xml")
    #infiles = glob("/Users/evanmiller/Projects/Runs/p3ht/T_phi_e_sweep/cont/Neat-P3HT-run-cont-dt0.001-phi*-P1.0-e_factor-*-n_mol100-T*/restart.xml")
    infiles = glob("/Users/evanmiller/Projects/Runs/p3ht/T_phi_e_sweep/e_fac_*/phi*/Neat-P3HT-sweep*/restart.xml")
    #write_dir = sys.argv[1]
    #print("Found {} files to run.".format(len(infiles)))
    calc_structure(infiles[0], clustering = True, tcl = False, quant_order=True, write_order=True, bond_dist=False, angle_dist=False)
    with multiprocessing.Pool() as pool:
        pool.map(partial(calc_structure, clustering=True, quant_order=True, write_order=True, bond_dist=False, angle_dist=False), infiles[1:])
