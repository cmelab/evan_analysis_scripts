import numpy as np
import mbuild as mb

class P3HT_Mon(mb.Compound):
    def __init__(self):
        super(P3HT_Mon, self).__init__()

        p3ht = mb.load('p3ht2.pdb', compound=self)
        # I used VMD to figure out what index to remove
        self.remove([self[8],
                 self[12]])
        # Now we need to add the ports
        # Add ports anchored to the correct carbons
        self.add(mb.Port(anchor=self[9]), label='down')
        self.add(mb.Port(anchor=self[7]), label='up')
        mb.rotate(p3ht, -np.pi/6, around = [0,0,1])
        mb.translate(p3ht, -p3ht[9].pos)
        # Move the ports approximately half a C-C bond length away from the carbon
        mb.translate(self['down'], [0, 0.154/2, 0]) 
        mb.translate(self['up'], [0, -0.154/2, 0])
        # Spin the port
        mb.rotate(self['down'], np.pi, around=[0, 1, 0])
        
        
class P3HTPolymer(mb.Compound):
    def __init__(self, chain_length=1):
        super(P3HTPolymer, self).__init__()
        last_monomer = P3HT_Mon()
        self.add(last_monomer)
        for i in range (chain_length-1):
            current_monomer = mb.clone(last_monomer)
    
            mb.force_overlap(move_this=current_monomer, 
                    from_positions=current_monomer['up'], 
                    to_positions=last_monomer['down'])
            self.add(current_monomer)
            last_monomer=current_monomer

def fix_format(filename = 'init.hoomdxml'):
    from fileinput import input
    import sys
    import re
    with open(filename, 'r') as f:
        started = False
        for line in f:
            if 'num=' in line:
                num = int(re.findall(r'num="(.*)"', line)[0])
            if '<bond>' in line:
                count = 0
                started = True
            if '</bond>' in line:
                started = False
            if started: 
                count+=1
    addnum_list = ['<type', '<body', '<diameter', '<mass', '<charge']
    for line in input(filename, inplace = 1):
        line = line.replace('\t', ' ')
        for item in addnum_list:
            if item in line:
                line = line.replace('>', " num=\"{}\">".format(num))
        if '<bond>' in line:
            line = line.replace('>', " num=\"{}\">".format(count))
        if '<configuration' in line:
            line = line.replace('>', " natoms=\"{}\">".format(num))
        if '<box' in line:
            line = line.replace('units=\"sigma\"  ', '')
            line = line.replace('Lx', 'lx')
            line = line.replace('Ly', 'ly')
            line = line.replace('Lz', 'lz')
        if '<position' in line:
            line = line.replace('units=\"sigma\" ', '')
        if 'coeffs' not in line:
            if 'C-C 0.0 0.0' not in line:
                if 'C-S 0.0 0.0' not in line:
                    if not line.startswith('<!--'):
                        sys.stdout.write(line)

def recenter():
    import recenter_molecule as recmol
    import helper_functions.matty_helper as mh
    import os
    xml, box = recmol.load_data(infile = '/Users/evanmiller/Projects/evan_analysis_scripts/p3ht/init.hoomdxml')
    xml = recmol.reform_images(xml)
    xml = recmol.center(xml, box)
    xml['diameter'] = [1 for x in range(len(xml['position']))]
    xml['body'] = [6*[-1]+5*[x] for x in range(int(len(xml['position']))//11)]
    xml['body'] = [item for sublist in xml['body'] for item in sublist]
    mh.writeMorphologyXML(xml, '/Users/evanmiller/Projects/evan_analysis_scripts/p3ht/init.hoomdxml')

def fix_types(xml):
    switch = 0
    for index, t in enumerate(xml['type']):
        if t == "S":
            if switch%2 == 0.:
                xml['type'][index] = "S1"
            if switch%2 == 1.:
                xml['type'][index] = "S2"
            switch+=1
    return xml

def align_bonds(xml):
    from operator import itemgetter
    for i,bond in enumerate(xml['bond']):
        species1 = xml['type'][bond[1]]
        species2 = xml['type'][bond[2]]
        if species1 > species2:
            xml['bond'][i] = ['{}-{}'.format(species2, species1), bond[2], bond[1]]
        if species1 <= species2:
            xml['bond'][i] = ['{}-{}'.format(species1, species2), bond[1], bond[2]]
    xml['bond'] = sorted(xml['bond'], key=itemgetter(2,0,1))
    return xml

def shrink_split():
    import helper_functions.matty_helper as mh
    sigma = 3.905
    xml = mh.loadMorphologyXML('/Users/evanmiller/Projects/evan_analysis_scripts/p3ht/init.hoomdxml')
    xml['diameter'] = [1 for x in range(len(xml['position']))]
    xml['type'] = [ 6*["CT"]+2*["CA"]+["S"]+2*["CA"] for x in range(int(len(xml['position']))//11)]
    xml['type'] = [item for sublist in xml['type'] for item in sublist]
    xml['body'] = [6*[-1]+5*[x] for x in range(int(len(xml['position']))//11)]
    xml['body'] = [item for sublist in xml['body'] for item in sublist]
    positions = np.array(xml['position'])
    positions/=sigma
    xml['lx'] = np.array(xml['lx'])/sigma
    xml['ly'] = np.array(xml['ly'])/sigma
    xml['lz'] = np.array(xml['lz'])/sigma
    xml['position'] = positions.tolist()
    xml = fix_types(xml)
    xml = align_bonds(xml)
    mh.writeMorphologyXML(xml, '/Users/evanmiller/Projects/evan_analysis_scripts/p3ht/init.hoomdxml')

def add_angs():
    import helper_functions.matty_helper as mh
    from cme_utils.manip import builder
    xml = mh.loadMorphologyXML('/Users/evanmiller/Projects/evan_analysis_scripts/p3ht/init.hoomdxml')
    infile = '/Users/evanmiller/Projects/evan_analysis_scripts/p3ht/init.hoomdxml'
    model_file = '/Users/evanmiller/Projects/evan_analysis_scripts/p3ht/model.xml'
    b = builder.building_block(infile=infile, model_file=model_file)
    b.generate_all_constraints()
    b.remove_redundant_rigid_constraints()
    xml['angle'] = b.props['angle']
    xml['dihedral'] = b.props['dihedral']
    mh.writeMorphologyXML(xml, '/Users/evanmiller/Projects/evan_analysis_scripts/p3ht/init.hoomdxml')
    
if __name__ == "__main__":
    num_mers = 4
    p3ht=P3HTPolymer(num_mers)
    size = 50/35*num_mers
    if size < 5.0:
        size = 5.0
    sim_box = mb.packing.fill_box(p3ht, 1,  box=[size, size, size])
    sim_box.save('init.hoomdxml', overwrite=True)

    fix_format()
    recenter()
    shrink_split()
    add_angs()
