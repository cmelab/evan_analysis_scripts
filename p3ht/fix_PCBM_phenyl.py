import numpy as np
import helper_functions.matty_helper as mh
import sys

def phenyl():
    pos = np.array([[-0.8092931223705871, 0.9506711176147797, -0.570886927626364],
    [-1.1337924821657217, 1.0146455094201572, -0.4276961465764283],
    [-1.2563533016279496, 1.347854216206329, -0.38233891226144723],
    [-1.0564352478507408, 1.6260974940552406, -0.48004185720382886],
    [-0.7353008048289739, 1.5708964697274557, -0.6240239314675934],
    [-0.6149064386317907, 1.2371269434790564, -0.670602676666057]])
    pos -= np.mean(pos, axis = 0)
    ring_normal = get_normal_vector(pos[0], pos[2], pos[4])
    return np.mean(pos, axis= 0), pos, ring_normal

def pbc(vec):
    """pbc is to account for the periodic boundry conditions. """
    axes = np.array([xml['lx'], xml['ly'], xml['lz']])
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def rotate_to_plane(mol, init, final):
    diff_norm = get_normal_vector(np.array([0., 0., 0.]), init, final)   
    theta = calc_theta(init, final)
    rotation_mat = arb_rot_matrix(theta, diff_norm)
    updated = []
    for atom in mol:
        updated.append(np.matmul(rotation_mat,np.array([np.array(atom)]).T).flatten())
    updated = np.array(updated)
    vec1 = updated[2]
    vec2 = updated[4]
    return updated

def build_z_matrix(theta):
    matrix = np.array([[1, 0.0, 0.0],
            [0, np.cos(theta), -np.sin(theta)],
            [0, np.sin(theta), np.cos(theta)]])
    return matrix

def build_y_matrix(theta):
    matrix = np.array([[np.cos(theta), 0.0, np.sin(theta)],
            [0.0, 1.0, 0.0],
            [-np.sin(theta), 0.0, np.cos(theta)]])
    return matrix

def calc_theta(vec1, vec2):
    theta = np.arccos(np.dot(vec1, vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2)))
    return theta

def rot_matrix(theta1, theta2):
    z = build_z_matrix(theta1)
    y = build_y_matrix(theta2)
    matrix = np.matmul(z, y)
    return matrix

def ring_rotation(com, old_positions, ring_normal, check):
    new_ring_com, new_ring_pos, new_ring_normal = phenyl()
    positions = rotate_to_plane(new_ring_pos, new_ring_normal, ring_normal)
    #
    new_ring_vec = positions[0] - new_ring_com
    old_ring_vec = old_positions[0] - com
    #
    positions = rotate_around(old_ring_vec, new_ring_vec, positions, ring_normal, check)
    return positions

def check_orientation(vec, theta, axis, check):
    rotation_mat1 = arb_rot_matrix(theta, axis)
    rot_test1 = np.matmul(rotation_mat1,np.array([np.array(vec)]).T).flatten()
    test1 = np.linalg.norm((check-rot_test1))
    rotation_mat2 = arb_rot_matrix(-theta, axis)
    rot_test2 = np.matmul(rotation_mat2,np.array([np.array(vec)]).T).flatten()
    test2 = np.linalg.norm((check-rot_test2))
    if test1 < test2:
        return rotation_mat1
    if test2 < test1:
        return rotation_mat2

def rotate_around(TO, FROM, MAT, AXIS, check):
    theta = calc_theta(FROM, TO)
    rotation_mat = check_orientation(MAT[0], theta, AXIS, check)
    updated = []
    for atom in MAT:
        updated.append(np.matmul(rotation_mat,np.array([np.array(atom)]).T).flatten())
    updated = np.array(updated)
    return updated

def arb_rot_matrix(theta, axis):
    axis = axis/np.linalg.norm(axis)
    ux = axis[0]
    uy = axis[1]
    uz = axis[2]
    iI = np.cos(theta)+ux**2*(1-np.cos(theta))
    iJ = ux*uy*(1-np.cos(theta))-uz*np.sin(theta)
    iK = ux*uz*(1-np.cos(theta))+uy*np.sin(theta)
    jI = uy*ux*(1-np.cos(theta))+uz*np.sin(theta)
    jJ = np.cos(theta)+uy**2*(1-np.cos(theta))
    jK = uz*uy*(1-np.cos(theta))-ux*np.sin(theta)
    kI = ux*uz*(1-np.cos(theta))-uy*np.sin(theta)
    kJ = uz*uy*(1-np.cos(theta))+ux*np.sin(theta)
    kK = np.cos(theta)+uz**2*(1-np.cos(theta))
    matrix = np.array([[iI, iJ, iK],
            [jI, jJ, jK],
            [kI, kJ, kK]])
    return matrix

def get_normal_vector(origin, vec1, vec2):
    vec1 = vec1-origin
    vec2 = vec2-origin
    return np.cross(vec1, vec2)/np.linalg.norm(np.cross(vec1, vec2))

def create_orient_check_vec(xml, index, com):
    ref = com - np.array(xml['position'][index])
    return ref

def ring_data(ring, xml):
    ring = np.array(ring)
    ref = np.array(xml['position'][ring[0]])
    positions = [ref]
    for atom in ring[1:]:
        pos = np.array(xml['position'][atom])
        unwrapped = pbc(ref - pos)
        new = ref - unwrapped
        positions.append(new)
    orig = np.mean(positions, axis = 0)
    positions-=orig
    com = np.mean(positions, axis = 0)
    ring_normal1 = get_normal_vector(positions[0], positions[1], positions[3])
    ring_normal2 = get_normal_vector(positions[0], positions[5], positions[3])
    ave = np.mean([ring_normal1, ring_normal2], axis = 0)
    o_check = create_orient_check_vec(xml, ring[0]-1, com)
    final = ring_rotation(com, np.array(positions), ave, positions[0])
    for i, val in enumerate(ring):
        xml['position'][val] = final[i] + orig
    return xml

filename = sys.argv[1]
xml = mh.loadMorphologyXML(filename)
filtered = [i for i, x in enumerate(xml['type']) if x in ['CA', 'CT', 'O']]
n_residues = len(filtered)//74

start_i = min(filtered)
interest = np.array([61, 67, 68, 69, 70, 71])+start_i
rings = [[i + n*74 for i in interest] for n in range(n_residues)]
c = np.mean([xml['position'][atom] for atom in rings[0]], axis = 0)

for ring in rings:
    xml = ring_data(ring, xml)

for atom in xml['position']:
    atom = pbc(np.array(atom))

mh.writeMorphologyXML(xml, 'flattened_rings.xml')
