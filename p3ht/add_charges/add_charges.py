import numpy as np
import sys
sys.path.append("/Users/evanmiller/Projects/morphct/code")
import helperFunctions as HF
from cme_utils.manip import builder
from cme_utils.manip import model_handler
from subprocess import call

sys.path.append("/Users/evanmiller/Projects/evan_analysis_scripts/p3ht")
import p3ht_helper as p3help

def ensureNeutral(lst):
    array = np.array(lst)
    netCharge = np.mean(array)
    array-=netCharge
    return array

def reduceUnits(array):
    perm = 8.854e-12 #C**2/N*m**2
    D = 3.905e-10 #m
    epsilon = 0.255*4814/6.022e23 #N*m
    fundementalCharge = 1.602e-19 #C
    reduced = array*fundementalCharge/np.sqrt(4*np.pi*perm*D*epsilon)
    return reduced

def compareSources(source):
    typeGuide = ["S", "CA1", "CA2", "CA3", "CA4", "CT1", "CT2", "CT3", "CT4", "CT5", "CT6"]
    LitSources = {'Bhatta':[-0.22, 0.18, -0.04, -0.08, 0.03, 0.136,-0.035, 0.026, 0.043, -0.032, -0.007],
            'Obata':[-0.07, 0.12, -0.07, -0.111, 0.05, 0.071, 0.03, -0.036, 0.008, 0.092, -0.075],
            'Trimer':[-0.17, 0.28, -0.03, -0.120, -0.11, 0.188, 0.055, -0.162, 0.108, 0.072, -0.102],
            'Pentamer':[-0.156, 0.242, 0.007, -0.134, -0.143, 0.191, 0.070, -0.171, 0.100, 0.083, -0.102]}
    reduced = reduceUnits(np.array(LitSources[source]))
    neutralized = ensureNeutral(reduced)
    TypeChargeMapper = {}
    for union in zip(typeGuide, neutralized):
        TypeChargeMapper[union[0]] = union[1]
    return TypeChargeMapper

def rewriteTypes(lst, typeMap):
    Nmers = len(lst)//11
    newTypes = [atom for mer in range(Nmers) for atom in typeMap]
    return newTypes

def rewriteCharges(TypeChargeMapper, lst):
    newList = []
    for index, atom in enumerate(lst):
        newList.append(TypeChargeMapper[atom])
    return newList

def CreateMorphology():
    TypeChargeMapper = compareSources('Trimer')
    #xml = HF.loadMorphologyXML('2mer.xml')
    xml = HF.loadMorphologyXML('restart.xml')
    typeMap = ['CA4', 'CA3', 'CA2', 'S', 'CA1', 'CT1', 'CT2', 'CT3', 'CT4', 'CT5', 'CT6']
    xml['type'] = p3help.rewriteTypes(xml['type'], typeMap)
    xml['charge'] = rewriteCharges(TypeChargeMapper, xml['type'])
    xml = p3help.updateConstraints(xml)
    HF.writeMorphologyXML(xml, 'output.xml')
    call(['python', '/Users/evanmiller/Projects/morphct/utils/fixImages/fixImages.py', 'output.xml'])
    return xml, TypeChargeMapper

def AddAtom(newtype, mh, TypeChargeMapper):
    if newtype[0] == 'S':
        oldtype = 'S1'
    if newtype[:2] == 'CT':
        oldtype = 'CT'
    if newtype[:2] == 'CA':
        oldtype = 'CA'
    charge = TypeChargeMapper[newtype]
    NEWATOM = model_handler.Atom(a=str(mh.atoms[oldtype].bond_type), 
            b=str(charge), 
            c=str(mh.atoms[oldtype].diameter), 
            d=str(mh.atoms[oldtype].epsilon), 
            i=str(mh.atoms[oldtype].id), 
            m=str(mh.atoms[oldtype].mass), 
            n=str(mh.atoms[oldtype].note), 
            t=str(newtype))
    mh.add_atom(NEWATOM)
    return mh

def getOldDihedral(newDihedral):
    reduced = []
    for newtype in newDihedral.split('-'):
        if newtype[0] == 'S':
            oldtype = 'S1'
        if newtype[:2] == 'CT':
            oldtype = 'CT'
        if newtype[:2] == 'CA':
            oldtype = 'CA'
        reduced.append(oldtype)
    if reduced[0] > reduced[-1]:
        print("REVERSE2", reduced)
        reduced.reverse()
    else:
        if reduced[1] > reduced[-2] and reduced[0] >= reduced[-1]:
            reduced.reverse()
    key = ""
    for atom in reduced:
        key+="{}-".format(atom)
    key=key[:-1]
    return key

def AddDihedral(newDihedral, mh):
    atom_types = newDihedral.split('-')
    if atom_types[0] > atom_types[-1]:
        atom_types.reverse()
    else:
        if atom_types[1] > atom_types[-2] and atom_types[0] >= atom_types[-1]:
            atom_types.reverse()
    key = ""
    for atom in atom_types:
        key+="{}-".format(atom)
    newDihedral=key[:-1]
    key = getOldDihedral(newDihedral)
    NEWDIHEDRAL = model_handler.Dihedral(k1=str(mh.dihedrals[key].k1),
            k2=str(mh.dihedrals[key].k2),
            k3=str(mh.dihedrals[key].k3),
            k4=str(mh.dihedrals[key].k4),
            ta=str(atom_types[0]),
            tb=str(atom_types[1]),
            tc=str(atom_types[2]),
            td=str(atom_types[3]),
            ty=str(newDihedral))
    mh.add_dihedral(NEWDIHEDRAL)

    atom_types.reverse()
    newkey = ""
    for atom in atom_types:
        newkey+="{}-".format(atom)
    newDihedral=newkey[:-1]

    NEWDIHEDRAL = model_handler.Dihedral(k1=str(mh.dihedrals[key].k1),
            k2=str(mh.dihedrals[key].k2),
            k3=str(mh.dihedrals[key].k3),
            k4=str(mh.dihedrals[key].k4),
            ta=str(atom_types[0]),
            tb=str(atom_types[1]),
            tc=str(atom_types[2]),
            td=str(atom_types[3]),
            ty=str(newDihedral))
    mh.add_dihedral(NEWDIHEDRAL)
    return mh

def getOldBond(newBond):
    reduced = []
    for newtype in newBond.split('-'):
        if newtype[0] == 'S':
            oldtype = 'S1'
        if newtype[:2] == 'CT':
            oldtype = 'CT'
        if newtype[:2] == 'CA':
            oldtype = 'CA'
        reduced.append(oldtype)
    if reduced[0] > reduced[-1]:
        reduced.reverse()
    key = ""
    for atom in reduced:
        key+="{}-".format(atom)
    key=key[:-1]
    return key

def AddBond(newBond, mh):
    atom_types = newBond.split('-')
    if atom_types[0] > atom_types[-1]:
        atom_types.reverse()
    key = ""
    for atom in atom_types:
        key+="{}-".format(atom)
    newBond=key[:-1]
    key = getOldBond(newBond)
    NEWBOND = model_handler.Bond(k=str(mh.bonds[key].k),
            l=str(mh.bonds[key].l),
            ta=str(atom_types[0]),
            tb=str(atom_types[1]),
            ty=str(newBond))
    mh.add_bond(NEWBOND)

    atom_types.reverse()
    newkey = ""
    for atom in atom_types:
        newkey+="{}-".format(atom)
    newBond=newkey[:-1]
    NEWBOND = model_handler.Bond(k=str(mh.bonds[key].k),
            l=str(mh.bonds[key].l),
            ta=str(atom_types[0]),
            tb=str(atom_types[1]),
            ty=str(newBond))
    mh.add_bond(NEWBOND)
    return mh

def getOldAngle(newAngle):
    reduced = []
    for newtype in newAngle.split('-'):
        if newtype[0] == 'S':
            oldtype = 'S1'
        if newtype[:2] == 'CT':
            oldtype = 'CT'
        if newtype[:2] == 'CA':
            oldtype = 'CA'
        reduced.append(oldtype)
    if reduced[0] > reduced[-1]:
        reduced.reverse()
    key = ""
    for atom in reduced:
        key+="{}-".format(atom)
    key=key[:-1]
    return key

def AddAngle(newAngle, mh):
    atom_types = newAngle.split('-')
    if atom_types[0] > atom_types[-1]:
        atom_types.reverse()
    key = ""
    for atom in atom_types:
        key+="{}-".format(atom)
    newAngle=key[:-1]
    key = getOldAngle(newAngle)
    print(newAngle, mh.angles[key].k, mh.angles[key].theta)
    NEWANGLE = model_handler.Angle(k=str(mh.angles[key].k),
            t=str(mh.angles[key].theta),
            ta=str(atom_types[0]),
            tb=str(atom_types[1]),
            tc=str(atom_types[2]),
            ty=str(newAngle))
    mh.add_angle(NEWANGLE)

    atom_types.reverse()
    newkey = ""
    for atom in atom_types:
        newkey+="{}-".format(atom)
    newAngle=newkey[:-1]
    NEWANGLE = model_handler.Angle(k=str(mh.angles[key].k),
            t=str(mh.angles[key].theta),
            ta=str(atom_types[0]),
            tb=str(atom_types[1]),
            tc=str(atom_types[2]),
            ty=str(newAngle))
    mh.add_angle(NEWANGLE)
    return mh

def CreateNewModel(xml, TypeChargeMapper):
    mh = model_handler.ModelHandler(filename='model.xml')
    mh.load_model('default')

    for i in list(set(xml['type'])):
        mh = AddAtom(i, mh, TypeChargeMapper)

    bonds = list(set([x[0] for x in xml['bond']]))
    for bond in bonds:
        mh = AddBond(bond, mh)

    angles = list(set([x[0] for x in xml['angle']]))
    for angle in angles:
        mh = AddAngle(angle, mh)

    dihedrals = list(set([x[0] for x in xml['dihedral']]))
    for dihedral in dihedrals:
        mh = AddDihedral(dihedral, mh)

    mh.write("testout.xml")

if __name__ == "__main__":
    xml, TypeChargeMapper = CreateMorphology()
    CreateNewModel(xml, TypeChargeMapper)
