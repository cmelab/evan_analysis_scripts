import numpy as np
import helper_functions.matty_helper as mh

def load_data():
    xml = mh.loadMorphologyXML('/home/evanmiller/Projects/morphct/inputCGMorphs/shifted.xml')
    return xml

def move_molecules(xml, displace):
    half = len(xml['position'])//2
    iteration = 0
    for move in displace:
        slide = xml
        mol1 = np.array(xml['position'])[:half]
        mol2 = np.array(xml['position'])[half:]
        mol1 = np.array([atom + move for atom in mol1])
        mol2 = np.array([atom - move for atom in mol2])
        positions = np.vstack((mol1, mol2))
        positions.tolist()
        slide['position'] = list(positions)
        write(slide, '{}_duplicate.xml'.format(iteration))
        iteration+=1
        del slide

def dup_positions(xml):
    print("Duplicating!")
    positions = np.array(xml['position'])
    shift = np.array([0, 0, 4.6])
    for atom in positions:
        atom += shift
    positions.tolist()
    xml['position'] += list(positions)
    return xml

def dup_body(xml):
    bodies_off_set = np.amax(np.array(xml['body'])) + 1
    bodies = np.array(xml['body'])
    bodies += bodies_off_set
    bodies.tolist()
    xml['body'] += list(bodies)
    return xml

def dup_bonds(xml):
    bonds = np.array(xml['bond'])
    off_list = np.array(bonds)[:,2]
    off_list = [int(x) for x in off_list]
    off_set = np.amax(off_list) + 1
    new_bonds = [[bond[0], int(bond[1])+off_set, int(bond[2])+off_set] for bond in bonds]
    xml['bond'] += new_bonds
    return xml

def dup_data(xml):
    xml['image'] += xml['image']
    xml['mass'] += xml['mass']
    xml['diameter'] += xml['diameter']
    xml['type'] += xml['type']
    return xml

def duplicate(xml):
    xml = dup_positions(xml)
    xml = dup_body(xml)
    xml = dup_data(xml)
    xml = dup_bonds(xml)
    xml['natoms'] = len(xml['body'])
    return xml

def write(xml, name):
    folder = '/home/evanmiller/Projects/morphct/inputCGMorphs/'
    mh.writeMorphologyXML(xml, folder+name)

def calc_len(xml):
    positions = np.array(xml['position'])
    a = positions[7,:]
    b = positions[359,:]
    dist = (a-b)/29
    displace = np.array([dist for x in range(15)])
    #displace = np.array([np.linspace(0, dist[x], 15) for x in range(3)]).T
    displace *= np.array([0, 1, 0]) 
    print(displace)
    return displace

if __name__ == "__main__":
    xml = load_data()
    displace = calc_len(xml)
    xml = duplicate(xml)
    move_molecules(xml, displace)
    #write(xml, 'duplicate.xml')
