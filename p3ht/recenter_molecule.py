import numpy as np
import helper_functions.matty_helper as mh

def load_data(infile ='/Users/evanmiller/Projects/evan_analysis_scripts/p3ht/init.xml'):
    xml = mh.loadMorphologyXML(infile)
    box = np.array([xml['lx'], xml['ly'], xml['lz']])
    xml = give_angles(xml)
    return xml, box

def unwrap_image(positions, xml, box):
    images = np.array(xml['image'])
    for i in range(len(positions)):
        for axis in range(3):
            positions[i][axis] += images[i][axis]*box[axis]
    return positions

def center(xml, box):
    positions = np.array(xml['position'])
    positions = unwrap_image(positions, xml, box)
    shift_around = abs(np.mean(positions, axis = 0))
    positions -= shift_around
    xml = grow_box(xml, box)
    positions.tolist()
    xml['position'] = positions
    return xml

def grow_box(xml, box):
    axes = ['lx', 'ly', 'lz']
    for axis in zip(axes, box):
        xml[axis[0]] = 1.1*np.amax(box)
    return xml

def give_angles(xml):
    orientations = ['xy', 'xz', 'yz']
    for axis in orientations:
        xml[axis] = 0.0
    return xml

def reform_images(xml):
    image = [[0, 0, 0] for x in range(len(xml['position']))]
    xml['image'] = image
    return xml

if __name__ == "__main__":
    xml, box = load_data()
    xml = reform_images(xml)

    xml = center(xml)

    mh.writeMorphologyXML(xml, '/home/evanmiller/Projects/morphct/inputCGMorphs/shifted.xml')
