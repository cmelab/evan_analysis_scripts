import numpy as np
import argparse
import os
import re

def get_statepoint():
    """
    Uses the path to extract the state point
    information.
    Requires: 
        None
    Returns:
        phi - packing fraction
        Temp - Temperature
        e_factor - simulation stickiness
    Note: These must be specified in the simation
    directory name for this function to be 
    used.
    """
    #TODO: Change it to read solely from final directory
    path = os.getcwd().split('/')
    Temp = float(re.findall(r'-T(.*)', path[-2])[0])
    phi = float(re.findall(r'phi(...)-', path[-2])[0])
    e_factor = float(re.findall(r'e_factor-(...)-', path[-2])[0])
    return phi, Temp, e_factor

def write_order(order, phi, Temp, e_factor):
    """
    Write the summarized order data to a file.
    The directory will need to be changed for the
    various machines.
    Requires:
        order - float 
        phi - str or float
        Temp - str or float
        e_factor - str or float
    Returns:
        None
    These arguments are used for writing the statepoint
    to the text file for further parsing and plotting.
    """
    write_to_file = "{} {} {} {}".format(order, e_factor, phi, Temp)
    filename = "pcbm_correlation.txt"
    if args.write_directory:
        direct = args.write_directory
        if os.path.exists(os.path.join(direct, filename)):
            with open(os.path.join(direct, filename), 'a') as textFile:
                textFile.write(write_to_file + "\n")
        else:
            with open(os.path.join(direct, filename), 'w+') as textFile:
                textFile.write(write_to_file + "\n")

def truncate(array):
    a, b = 0, 0
    lower_limit = 0.9
    upper_limit = 1.6
    for i, val in enumerate(array[:,0]):
        if val <= lower_limit:
            a = i
        if val <= upper_limit:
            b = i
    truncated = array[a:b,1]
    return truncated
    
def find_max(array):
    truncated = truncate(array)
    peak = np.amax(truncated)
    phi, Temp, e_factor = get_statepoint()
    write_order(peak, phi, Temp, e_factor)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-write", "--write_directory", help="Tell script where to write the order.")
    args = parser.parse_args()

    data = np.genfromtxt('asq.txt')
    find_max(data)

