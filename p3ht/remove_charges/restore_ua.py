import numpy as np
import sys
sys.path.append("/Users/evanmiller/Projects/morphct/code")
import helperFunctions as HF
sys.path.append("/Users/evanmiller/Projects/evan_analysis_scripts/p3ht")
import p3ht_helper as p3help

if __name__ == "__main__":
    xml = HF.loadMorphologyXML('restart.xml')
    typeMap = ['CA', 'CA', 'CA', 'S', 'CA', 'CT', 'CT', 'CT', 'CT', 'CT', 'CT']
    xml['type'] = p3help.rewriteTypes(xml['type'], typeMap)
    xml = p3help.updateConstraints(xml)
    HF.writeMorphologyXML(xml, 'output.xml')
