import sys
sys.path.append('/Users/evanmiller/Projects/evan_analysis_scripts/')
import gen_restart_from_log as genrst
from cme_utils.analyze import autocorr

if __name__=="__main__":
    t = autocorr.autocorrelation()
    step_size = t[1]
    fframe = genrst.determine_size()
    fframe = 9
    start_frame = fframe - 5
    #start_frame = fframe - step_size*5
    for i in range(start_frame+1, fframe+1, 1):
    #for i in range(start_frame+1, fframe+1, step_size):
        print(i)
        genrst.write_and_destroy(i, filename="frame_{}.xml".format(i))

