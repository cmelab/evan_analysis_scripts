import numpy as np
from cme_utils.manip.pbc import plain_pbc as pbc
import mdtraj as md
import matplotlib.pyplot as plt
import helper_functions.e_helper as cg

"""Check for hoomdxml file and create if necessary."""
cg.check_hoomdxml()

def cross_product(p1, p2, p3):
    """Calculates the cross product from three members of a ring"""
    v1 = pbc(p3 - p1, box_dim)
    v2 = pbc(p3 - p2, box_dim)
    cp = np.cross(v1, v2)
    cp /= np.linalg.norm(cp)
    return cp

def calc_norm(a, b, c, sf, ff):
    cp_list = []
    for frame in range(sf, ff):
        #NOTE neet to make sure values are integers.
        p1_list = t.xyz[frame,a:-1:apm,:]
        p2_list = t.xyz[frame,b:-1:apm,:]
        p3_list = t.xyz[frame,c:-1:apm,:]
        for i in range(nresid):
            cp_list.append(cross_product(p1_list[i], p2_list[i], p3_list[i]))
    return cp_list

def unit_vec(v):
    return v/np.linalg.norm(v)

def angle(v1, v2):
    v1u = unit_vec(v1)
    v2u = unit_vec(v2)
    deg = np.degrees(np.arccos(np.clip(np.dot(v1u, v2u), -1.0, 1.0)))
    if deg > 90.0:
        deg -= 90.0
    return deg

TRAJ_FILE = "traj.dcd" #Trajectory file
TOP_FILE = "restart.hoomdxml" #Topology file
t = md.load(TRAJ_FILE, top = TOP_FILE)

natoms = t.n_atoms
nresid = t.n_residues
apm = t.n_atoms/t.n_residues
s_frame = t.n_frames-10
f_frame = t.n_frames-1
box_dim = t.unitcell_lengths[0]

center = calc_norm(32, 50, 0, s_frame, f_frame)
phen1 = calc_norm(58, 60, 62, s_frame, f_frame)
phen2 = calc_norm(52, 54, 56, s_frame, f_frame)
phen3 = calc_norm(20, 22, 24, s_frame, f_frame)
phen4 = calc_norm(26, 28, 30, s_frame, f_frame)

angles_list = []
for frame in range(len(center)):
    angles_list.append(angle(center[frame], phen1[frame]))
    angles_list.append(angle(center[frame], phen2[frame]))
    angles_list.append(angle(center[frame], phen3[frame]))
    angles_list.append(angle(center[frame], phen4[frame]))

mean = np.mean(angles_list)
print("Samples: {}".format(len(angles_list)))

plt.hist(angles_list, bins = 100)
plt.axvspan(mean-0.2, mean+0.2, label = "Ave: {:.2f}".format(mean), alpha = 0.3, color = 'r')
plt.legend(handlelength = 0.1)
plt.xlabel("Degrees ($^{\circ}$)")
plt.savefig("hist.pdf")
