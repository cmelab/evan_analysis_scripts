import subprocess as sp
from glob import glob
import os
import multiprocessing as mp
import numpy as np

orca_directory = os.getenv('ORCA_BIN', "{}/ORCA".format(os.getcwd()))
orca_path = "{}/orca".format(orca_directory)

def get_jobs_to_run(job_dir = 'bond_stretch/'):
    jobs_to_run = glob(job_dir + "*")
    return jobs_to_run

#def assign_jobs_to_cores(jobs_to_run, n_cores = 14):
#    return [jobs_to_run[i::n_cores] for i in range(n_cores)]

def get_cpu_cores():
    try:
        proc_IDs = list(np.arange(int(os.environ.get('SLURM_NPROCS'))))
    except (AttributeError, TypeError):
        print("not loaded from slurm")
        proc_IDs = list(np.arange(mp.cpu_count()))
    return proc_IDs

if __name__ == "__main__":
    jobs_to_run = get_jobs_to_run()
#    proc_IDs = get_cpu_cores()
#    split_jobs_to_run = assign_jobs_to_cores(jobs_to_run, n_cores = len(proc_IDs))
