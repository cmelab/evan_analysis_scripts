import numpy as np
import sys
sys.path.append("/Users/evanmiller/Projects/old_morphct/code/")
import helperFunctions as mh

def pbc(vec, axes):
    """pbc is to account for the periodic boundry conditions. """
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def build_header():
    text = "! ZINDO/S\n"
    text += "! NOPOP\n"
    text += "! NOMOPRINT\n"
    text += "* xyz 0  1\n"
    return text

def build_footer(text):
    text+="*"
    return text

def wrap(positions, box):
    pos = np.copy(positions)
    for i, val in enumerate(pos):
        pos[i] = pbc(val, box)
    return pos

def center(positions):
    pos = np.copy(positions)
    pos -= np.mean(pos, axis=0)
    return pos

def write_positions(types, positions, text = ""):
    positions_text = ""
    for c in zip(types, positions):
        positions_text += " {} {:.2f} {:.2f} {:.2f}\n".format(c[0][0], c[1][0], c[1][1], c[1][2])
    text += positions_text
    return text

def write_file(text, filename):
    with open(filename, 'w') as f:
        f.write(text)

def zero_image(images):
    for i, image in enumerate(images):
        images[i] = [0, 0, 0]
    return images

def perturb_dihedral(positions):
    pass
def perturb_angle(positions):
    pass

def perturb_bond(ring, scale, positions):
    vector = get_bond_vector(ring, positions)*scale
    for i, val in enumerate(positions):
        if i in rings[ring]:
            positions[i] = val + vector
    return positions

def get_bond_vector(ring, positions):
    a = positions[rings[ring][0]]
    b = positions[rings[ring][3]]
    return (b-a)/np.linalg.norm(b-a) 

def get_ring_dictionary():
    rings ={'ring0':list(range(40,46))+list(range(80,85)),
            'ring1':list(range(46,52))+list(range(85,90)),
            'ring2':list(range(52,58))+list(range(90,95)),
            'ring3':list(range(58,64))+list(range(95,100))}
    return rings

if __name__ == "__main__":
    rings = get_ring_dictionary()

    infile = sys.argv[1]
    name = sys.argv[2]

    #for scale1 in np.arange(-0.11, 0.33, 0.02):
    #    for scale2 in np.arange(-0.11, 0.33, 0.02):
    #        name = "bond0_{:.2f}_bond2_{:.2f}.inp".format(scale1, scale2)

    #        xml = mh.loadMorphologyXML(infile)
    #        positions = np.array(xml['position'])

    #        positions = perturb_bond('ring0', scale1, positions)
    #        positions = perturb_bond('ring2', scale2, positions)

    #        text = build_header()

    #        text = write_positions(xml['type'], positions, text)
    #        text = build_footer(text)
    #        #xml['position'] = list(positions)

    #        #mh.writeMorphologyXML(xml, 'test.xml')
    #        write_file(text, './bond_stretch/'+name)
    xml = mh.loadMorphologyXML(infile)
    positions = np.array(xml['position'])

    images = xml['image']
    images = zero_image(images)
    xml['image'] = images
    box = np.array([xml['lx']/2, xml['ly']/2,xml['lz']/2])
    positions = wrap(positions, box)
    positions = center(positions)

    text = build_header()

    text = write_positions(xml['type'], positions, text)
    text = build_footer(text)

    write_file(text, './bond_stretch/'+name)
