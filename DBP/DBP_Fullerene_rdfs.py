"""Script intended to combine RDFs for DBP and Fullerenes
at different statepoints."""

import numpy as np
import matplotlib.pyplot as plt
import helper_functions.matty_helper as mh
import sys
import os

home_dir = os.environ['HOME']
sys.path.append(home_dir+'/Projects/evan_analysis_scripts/DBP/')

import DBP_Helper as DH

from freud import box, density

def calc_rdf(molecule):
    rdf = density.RDF(rmax = 10.0, dr = 0.1)
    fbox = box.Box(Lx = molecule.unitcell_lengths[0], Ly = molecule.unitcell_lengths[1], Lz = molecule.unitcell_lengths[2])
    rdf.compute(fbox, molecule.centers, molecule.centers)
    r = rdf.getR()
    y = rdf.getRDF()
    return tuple(r), tuple(y)

if __name__ == '__main__':

    print("Loading XML")
    xml = mh.loadMorphologyXML('C60-restart.xml')

    DBP_C60, FULLERENE_C60 = DH.filter_system(xml)
    DBP_C60.centers = DH.get_centers(DBP_C60)
    FULLERENE_C60.centers = DH.get_centers(FULLERENE_C60)

    xml = mh.loadMorphologyXML('C70-restart.xml')

    DBP_C70, FULLERENE_C70 = DH.filter_system(xml)
    DBP_C70.centers = DH.get_centers(DBP_C70)
    FULLERENE_C70.centers = DH.get_centers(FULLERENE_C70)

    xml = mh.loadMorphologyXML('PC60BM-restart.xml')

    DBP_PC60BM, FULLERENE_PC60BM = DH.filter_system(xml)
    DBP_PC60BM.centers = DH.get_centers(DBP_PC60BM)
    FULLERENE_PC60BM.centers = DH.get_centers(FULLERENE_PC60BM)

    xml = mh.loadMorphologyXML('PC70BM-restart.xml')

    DBP_PC70BM, FULLERENE_PC70BM = DH.filter_system(xml)
    DBP_PC70BM.centers = DH.get_centers(DBP_PC70BM)
    FULLERENE_PC70BM.centers = DH.get_centers(FULLERENE_PC70BM)

    calc_rdf(DBP_C60)
    calc_rdf(DBP_C70)
    calc_rdf(DBP_PC60BM)
    calc_rdf(DBP_PC70BM)
    plt.legend()
    plt.show()

    calc_rdf(FULLERENE_C60)
    calc_rdf(FULLERENE_C70)
    calc_rdf(FULLERENE_PC60BM)
    calc_rdf(FULLERENE_PC70BM)
    plt.legend()
    plt.show()
