import numpy as np
import helper_functions.matty_helper as mh
import sys

def pbc(vec):
    """pbc is to account for the periodic boundry conditions. """
    axes = np.array([xml['lx'], xml['ly'], xml['lz']])
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def arb_rot_matrix(theta, axis):
    axis = axis/np.linalg.norm(axis)
    ux = axis[0]
    uy = axis[1]
    uz = axis[2]
    iI = np.cos(theta)+ux**2*(1-np.cos(theta))
    iJ = ux*uy*(1-np.cos(theta))-uz*np.sin(theta)
    iK = ux*uz*(1-np.cos(theta))+uy*np.sin(theta)
    jI = uy*ux*(1-np.cos(theta))+uz*np.sin(theta)
    jJ = np.cos(theta)+uy**2*(1-np.cos(theta))
    jK = uz*uy*(1-np.cos(theta))-ux*np.sin(theta)
    kI = ux*uz*(1-np.cos(theta))-uy*np.sin(theta)
    kJ = uz*uy*(1-np.cos(theta))+ux*np.sin(theta)
    kK = np.cos(theta)+uz**2*(1-np.cos(theta))
    matrix = np.array([[iI, iJ, iK],
            [jI, jJ, jK],
            [kI, kJ, kK]])
    return matrix

def get_normal_vector(vec1, vec2, vec3):
    vec1 = np.array(vec1)
    vec2 = np.array(vec2)
    vec3 = np.array(vec3)
    vec1 = pbc(vec2-vec1)
    vec2 = pbc(vec3-vec1)
    return np.cross(vec1, vec2)/np.linalg.norm(np.cross(vec1, vec2))

def build_new_mol_plane(refs, xml):
    a = xml['position'][refs[0]]
    b = xml['position'][refs[1]]
    c = xml['position'][refs[2]]
    d = xml['position'][refs[3]]
    ring_normal1 = get_normal_vector(a, b, c)
    ring_normal2 = get_normal_vector(a, b, d)
    ave = np.mean([ring_normal1, ring_normal2], axis = 0)
    #print("PLANE TO MOVE TO", ave)
    return ave

def ring_data(ring, xml):
    ring = np.array(ring)
    refs = ring[:4]
    mol = ring[4:]
    const = np.array(xml['position'][mol[0]])
    positions = [const]
    for atom in mol[1:]:
        pos = np.array(xml['position'][atom])
        unwrapped = pbc(const - pos)
        new = const - unwrapped
        positions.append(new)
    orig = np.mean(positions, axis = 0)
    positions-=orig
    com = np.mean(positions, axis = 0)
    ave = build_new_mol_plane(refs, xml)
    final = ring_rotation(com, np.array(positions), ave)
    for i, val in enumerate(mol):
        #print(xml['position'][val], "MOVING TO", final[i] + orig)
        xml['position'][val] = final[i] + orig
    return xml

def rotate_to_plane(mol, init, final):
    diff_norm = get_normal_vector(np.array([0., 0., 0.]), init, final)   
    theta = calc_theta(init, final)
    rotation_mat = arb_rot_matrix(theta, diff_norm)
    updated = []
    for atom in mol:
        updated.append(np.matmul(rotation_mat,np.array([np.array(atom)]).T).flatten())
    updated = np.array(updated)
    return updated

def calc_theta(vec1, vec2):
    theta = np.arccos(np.dot(vec1, vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2)))
    return theta

def ring_rotation(com, old_positions, ring_normal):
    new_ring_normal = get_normal_vector(old_positions[4], old_positions[10], old_positions[8])
    positions = rotate_to_plane(old_positions, new_ring_normal, ring_normal)
    return positions

filename = sys.argv[1]
xml = mh.loadMorphologyXML(filename)
filtered = [i for i, x in enumerate(xml['type']) if x in ['C']]
n_residues = len(filtered)//64

start_i = min(filtered)
interest = np.array([5, 12, 26, 33, 0, 1, 2, 3, 19, 14, 15, 16, 20, 21, 22, 23, 24, 39, 35, 36, 37, 17, 18, 38])+start_i

rings = [[i + n*64 for i in interest] for n in range(n_residues)]
print(rings)

for ring in rings:
    xml = ring_data(ring, xml)

interest2 = np.array([5, 12, 26, 33, 6, 7, 8, 9, 10, 11, 27, 28, 29, 30, 31, 32])+start_i
rings2 = [[i + n*64 for i in interest2] for n in range(n_residues)]

for ring in rings2:
    xml = ring_data(ring, xml)

mh.writeMorphologyXML(xml, 'flattened_rings.xml')
