"""Script to contain reusable DBP specific scripts."""
import numpy as np

def pbc(vec, axes):
    """pbc is to account for the periodic boundry conditions. """
    for i,v in enumerate(vec):
        if v>axes[i]/2.:
            vec[i]=v-axes[i]
        if v<-axes[i]/2.:
            vec[i]=v+axes[i]
    return vec

def get_centers(molecule, orientation_too = False):
    """
    Parses the xyz data for the simulations to get atom positions
    and calculates the center of mass for each thiophene ring.
    Requires:
    Returns:
       mols_list - list(s) for each center of mass
    """
    selector = {"DBP":[17, 18, 21], "C60":[15, 33, 41], "PC60BM":[15, 33, 41], "C70":[5, 55, 64], "PC70BM":[17, 67, 76]}
    triforce = selector[molecule.species]
    frames = (molecule.n_frames-1)#gets the final frame
    points = np.hstack((molecule.xyz[frames,triforce[0]::molecule.apm,:],molecule.xyz[frames,triforce[1]::molecule.apm,:],molecule.xyz[frames,triforce[2]::molecule.apm,:]))
    averages = []
    orientations = []
    for point in points:
        if not orientation_too:
            center = average_position(point, molecule.unitcell_lengths, orientation_too)
            averages.append(center)
        if orientation_too:
            center, normal = average_position(point, molecule.unitcell_lengths, orientation_too)
            averages.append(center)
            orientations.append(normal)
    if orientation_too:
        return np.array(averages), np.array(orientations)
    else:
        return np.array(averages)

def average_position(array, axes, orientation_too):
    """ This function takes the points, calculates vectors so the periodic boundries can be tested then will average and append the cm list."""
    p1 = array[0:3]
    p2 = array[3:6]
    p3 = array[6:9]
    v1 = pbc((p1 - p3), axes)
    v2 = pbc((p2 - p3), axes)
    p1new = p3 + v1
    p2new = p3 + v2
    average = (p1new+p2new+p3) / 3.
    if orientation_too:
        orientation = np.cross(v1, v2)
        orientation /= np.linalg.norm(orientation)
        return average, orientation
    else:
        return average

def orientation(array, axes):
    p1 = array[0:3]
    p2 = array[3:6]
    p3 = array[6:9]
    v1 = pbc((p1 - p3), axes)
    v2 = pbc((p2 - p3), axes)

def check_same(lst1, lst2):
    same = True
    for i in zip(lst1,lst2):
        if i[0] != i[1]:
            same = False
    return same

def determine_species(bodies, types):
    apm = len(bodies)//len(set(bodies))
    if apm not in [60, 70]:
        same = check_same(types[:74],types[74:2*74])
        if same:
            apm = 74
        same = check_same(types[:84],types[84:2*84])
        if same:
            apm = 84
    possible = {84:"PC70BM",74:"PC60BM",70:"C70",60:"C60"}
    species = possible[apm]
    print("Notice: Script thinks the system has: {}.".format(species))
    return species, apm

def xml_filter(xml, DBP_present, Fullerene_present):
    """
    Utilizes the Matty helper to parse the xml data
    and filter the topology data and return 
    the needed clustering information.
    Requires:
        xml - list of toplogy data
    Returns:
        t - class containing topology arrays.
    """

    class from_xml(object):
        xyz = []
        n_frames = 0
        unitcell_lengths = []
        n_residues = 0
        n_atoms = 0
        species = ""
        apm = 0
        centers = np.array([])
        bodies = []
        types = []
    
    DBP = from_xml()

    if DBP_present:
        #print("Populating DBP class")
        indices = [i for i, x in enumerate(xml['type']) if x in['C']]
        DBP.xyz = np.array([[xml['position'][x] for x in indices]], dtype = np.float32)
        DBP.bodies = [xml['body'][x] for x in indices]
        DBP.types = [xml['type'][x] for x in indices]
        DBP.n_frames = 1
        DBP.n_residues = len(DBP.xyz[0,:,0])//64
        DBP.unitcell_lengths = np.array([xml['lx'], xml['ly'], xml['lz']], dtype = np.float32)
        DBP.n_atoms = len(DBP.xyz[0,:,0])
        DBP.species = "DBP"
        DBP.apm = 64

    FULLERENE = from_xml()
    if Fullerene_present:
        #print("Populating Fullerene class")
        indices = [i for i, x in enumerate(xml['type']) if x in['CA', 'CT', 'O']]
        FULLERENE.bodies = [xml['body'][x] for x in indices]
        FULLERENE.types = [xml['type'][x] for x in indices]
        FULLERENE.xyz = np.array([[xml['position'][x] for x in indices]], dtype = np.float32)
        FULLERENE.species, FULLERENE.apm = determine_species(FULLERENE.bodies, FULLERENE.types)
        FULLERENE.n_frames = 1
        FULLERENE.n_residues = len(FULLERENE.xyz[0,:,0])//FULLERENE.apm
        FULLERENE.unitcell_lengths = np.array([xml['lx'], xml['ly'], xml['lz']], dtype = np.float32)
        FULLERENE.n_atoms = len(FULLERENE.xyz[0,:,0])
    return DBP, FULLERENE

def filter_system(xml, DBP_present=True, Fullerene_present=True):
    """
    Creates a new trajectory from the given trajectory
    where only atoms with type CA or S are present.
    Requires:
        None
    Returns:
        DBP - mdtraj array with trajectory information.
        or 
        DBP - arrays with requisite data.
    """
    DBP, FULLERENE = xml_filter(xml, DBP_present, Fullerene_present)
    return DBP, FULLERENE

def check_distance(i, j, centers, orientation1, orientation2, molecule, dcut, dotcut):
    point1 = centers[i]
    point2 = centers[j]
    d = np.linalg.norm(pbc((point1-point2), molecule.unitcell_lengths))
    dot = np.dot(orientation1, orientation2)
    if abs(d) <= dcut and abs(dot) >= dotcut:
        return [i,j]

def build_neighbors(centers, orientations, molecule, dcut = 3.3, dotcut = 0.9):
    print("Building neighbor list.")
    print(centers.shape)
    n_list = [ [] for i in range(molecule.n_residues) ]
    neighbors_found = []
    check_list = [ [j for j in range(i+1, molecule.n_residues)] for i in range(molecule.n_residues-1)]
    for i,val in enumerate(check_list):
        for j in val:
            neighbors_found.append(check_distance(i, j, centers, orientations[i], orientations[j], molecule, dcut, dotcut))
    neighbors_found = [x for x in neighbors_found if x is not None]
    for x in neighbors_found:
        n_list[x[0]].append(x[1])
        n_list[x[1]].append(x[0])
    print("Done.")
    return n_list

def make_clusters(n_list):
    print("Creating Clusters.")
    c_list = [i for i in range(len(n_list))]
    for i in range(len(c_list)):
        c_list = update_neighbors(i, c_list, n_list)
    print("Done.")
    return c_list

def update_neighbors(particle, cluster_list, neighbor_list):
    """Recursive function to convert neighborlist into cluster list"""
    for n in neighbor_list[particle]:
        if cluster_list[n]>cluster_list[particle]:
            cluster_list[n] = cluster_list[particle]
            cluster_list = update_neighbors(n,cluster_list,neighbor_list)
        elif cluster_list[n] < cluster_list[particle]: 
            cluster_list[particle] = cluster_list[n]
            cluster_list = update_neighbors(particle,cluster_list,neighbor_list)
    return cluster_list

def print_clusters(n_list, c_list):
    for i in range(len(n_list)):
        inclust = ""
        for j,c in enumerate(c_list):
            if c==i:
                inclust+=str(j)+" "
        if inclust !="":
            print(i,inclust)
    print(len(set(c_list)), "clusters")

def TCL(neighbors, clusters, location, name = '/c_color.tcl'):
    off_set = 0
    forbidden1 = np.arange(10, 200, 32)
    forbidden2 = np.arange(1, 200, 32)
    forbidden = np.concatenate((forbidden1 , forbidden2))
    colors = [x for x in range(0, 200) if x not in forbidden]
    tclLinesToWrite = ['mol delrep 0 0\n']
    tclLinesToWrite += ['pbc wrap -center origin\n'] 
    tclLinesToWrite += ['pbc box -color black -center origin -width 4\n']
    tclLinesToWrite += ['display resetview\n']
    tclLinesToWrite += ['color change rgb 9 1.0 0.29 0.5\n']
    tclLinesToWrite += ['color change rgb 16 0.25 0.25 0.25\n']
    count = 0
    for i in range(len(neighbors)):
        inclust = ""
        for j,c in enumerate(clusters):
            if c==i:
                for k in range(off_set+j*5, off_set+j*5+5):
                    inclust +=str(k)+" "
        if inclust !="":
            tclLinesToWrite += ['mol material AOEdgy\n']
            tclLinesToWrite +=['mol color ColorID '+str(colors[count%32])+'\n']
            tclLinesToWrite +=['mol representation VDW 0.9 8.0\n']
            #tclLinesToWrite +=['mol representation QuickSurf 0.7 0.9 0.5 1.0\n']
            tclLinesToWrite += ['mol selection resid '+str(inclust)+'\n']
            tclLinesToWrite += ['mol addrep 0\n']
            count += 1
    tclLinesToWrite +=['mol color ColorID 10' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type CA' + '\n']
    tclLinesToWrite +=['mol material AOEdgy' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclLinesToWrite +=['mol color ColorID 10' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type CT' + '\n']
    tclLinesToWrite +=['mol material AOEdgy' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclLinesToWrite +=['mol color ColorID 1' + '\n']
    tclLinesToWrite +=['mol representation VDW 0.8 8.0' + '\n']
    tclLinesToWrite +=['mol selection type O' + '\n'] 
    tclLinesToWrite +=['mol material AOEdgy' + '\n']
    tclLinesToWrite +=['mol addrep 0' + '\n']
    tclFileName = location+ name
    with open(tclFileName, 'w+') as tclFile:
        tclFile.writelines(tclLinesToWrite)
    print('Tcl file written to', tclFileName)
