import helper_functions.matty_helper as mh
import numpy as np
import sys
import re
import os

class convert_DBP():
    '''This class is meant to handle rewriting
    the body data of a hoomdxml file so that
    the chromophores in charge transfer calculations
    are easily identifiable.'''
    def get_data_from_xml(self, filename):
        '''
        Reads and parses type data of an xml
        to determine which bodies belong in 
        acceptor or donor bodies. Additionally,
        it sets indices for the transitions between
        types for rebuilding the xml body list.
        Requires:
            hoomdxml file
        Returns:
            xml list
        Also sets class global variables:
            Number of DBP molecules (self.num_DBP)
            The index of the first atom in DBP (self.first_C)
            The index of the final atom in DBP (self.end_C)
            The index of the first atom in the acceptor moiety (self.first_acc)
            The index of the final atom in the acceptor moiety (self.end_acc)

        '''
        xml = mh.loadMorphologyXML(filename)
        num_atoms = len(xml['type'])
        self.num_DBP = xml['type'].count('C')//64
        num_C = xml['type'].count('C')
        num_other = num_atoms - num_C
        self.first_C = xml['type'].index('C')
        self.end_C = num_C+self.first_C
        index_list = []
        for atom in ['CA', 'O', 'CT']:
            try:
                index = xml['type'].index(atom)
                index_list.append(index)
            except:
                print(atom, "Not found")
                pass
        if len(index_list) > 0:
            self.first_acc = min(index_list)
            self.end_acc = self.first_acc + num_other
        else:
            self.first_acc = 0
            self.end_acc = 0
        return xml

    def print_bodies(self, filename, writefile):
        '''Separates list data and writes lists 
        for the bodies representing
        chromophores in the simulation.
        Requires:
            hoomdfile
            filename to write text
        Returns:
            None
        '''
        xml = self.get_data_from_xml(filename)
        bodies = np.array(xml['body'])
        donors = bodies[self.first_C:self.end_C]
        acceptors = bodies[self.first_acc:self.end_acc]
        acceptors = [x for x in set(acceptors) if x != -1]
        with open(writefile, 'w+') as f:
            f.write('DONORS\n')
            f.write(str(list(set(donors))))
            f.write('\nACCEPTORS\n')
            f.write(str(acceptors))
        
    def unify_PCBM(self, bods, acceptors):
        '''
        Rewrites the list of acceptor bodies so
        that the phenyl and butyric acid methyl
        ester chain of PCBM are added to the same
        body as the fullerene.
        Requires:
            array of acceptor bodies
        Returns:
            transformed array of acceptor bodies
        '''
        def check_same(lst1, lst2):
            same = True
            for i in zip(lst1,lst2):
                if i[0] != i[1]:
                    same = False
            return same

        if len(set(acceptors)) == 1:
            apm = len(bods)//len(set(bods))
        else:
            same = check_same(acceptors[:74],acceptors[74:2*74])
            if same:
                apm = 74
            same = check_same(acceptors[:84],acceptors[84:2*84])
            if same:
                apm = 84
        possible = {84:"PC70BM",74:"PC60BM",70:"C70",60:"C60"}
        print("Notice: Script thinks the system has: {}.".format(possible[apm]))
        for i in bods:
            if i != -1:
                first_acc_body = i
                break
        num_acceptors = len(set(bods))
        if list(set(bods)).count(-1) != 0:
            num_acceptors-=1
            num_acceptors/=2
        acceptors = np.array([ apm*[x] for x in np.arange(first_acc_body, first_acc_body+num_acceptors)] ).flatten()
        return acceptors

    def separate(self, xml):
        '''
        Extracts the acceptor bodies from the xml
        file.
        Requires:
            xml: hoomdxml list
        Returns:
            array of acceptor bodies
        '''
        bodies = np.array(xml['type'])
        acceptors = bodies[self.first_acc:self.end_acc]
        if len(acceptors) > 0:
            bodies = np.array(xml['body'])
            bods = bodies[self.first_acc:self.end_acc]
            acceptors = self.unify_PCBM(bods, acceptors)
            return acceptors
        else:
            return np.array([0])

    def unify_DBP(self, xml, acceptors, filename):
        '''
        Rewrites list of donor bodies so that 
        every atom within a donor has the same
        body tag.
        Writes the data to a file.
        Requires:
            xml - hoomdxml list
            acceptors - array of acceptor body tags
            filename - string of filename
        Returns:
            None
        '''
        updated = np.array([64*[x] for x in range(self.num_DBP)]).flatten()
        xml = self.rebuild_body(xml, updated, acceptors, shrink = True)
        mh.writeMorphologyXML(xml, filename)

    def reorder_DBP(self, xml, acceptors, filename):
        '''
        Rewrites list of donor bodies so that 
        each DPB molecule is comprised into five 
        bodies: The aromatic backbone and one for
        each of the four phenyl rings.
        Writes the data to a file.
        Requires:
            xml - hoomdxml list
            acceptors - array of acceptor body tags
            filename - string of filename
        Returns:
            None
        '''
        updated = np.array([np.array([20*[x]+6*[x+1]+6*[x+2]+20*[x]+6*[x+3]+6*[x+4]]).flatten() for x in np.arange(0, self.num_DBP*5, 5)]).flatten()
        xml = self.rebuild_body(xml, updated, acceptors, shrink = False)
        mh.writeMorphologyXML(xml, filename)

    def rebuild_body(self, xml, updated, acceptors, shrink):
        '''Combines arrays of donor and acceptor
        body tags to create a list of body tags.
        Requires:
            xml - hoomdxml list
            updated - array of donor body tags
            acceptors - array of acceptor body tags
        Returns:
            xml list with new body tag information
        '''
        if acceptors != None:
            new_acceptor = acceptors
            if shrink and self.first_C == 0:
                if np.amax(updated)+1==min(acceptors):
                    print("Fullerene bodies have already been reduced.")
                else:
                    new_acceptor-=4*self.num_DBP
            updated = list(map(int, updated))
            new_acceptor = list(map(int, new_acceptor))
            if self.first_C == 0:
                new = np.concatenate((updated, new_acceptor))
            if self.first_C != 0:
                print("DBP molecules are not first in the file.\n\
    Leaving the beginning allow and shrinking \
    DPB molecules at the end.")
                updated+=(np.amax(new_acceptor)+1)
                new = np.concatenate((new_acceptor, updated))
            new = list(new)
            xml['body'] = new
            del new_acceptor
            return xml
        else:
            new = list(updated)
            xml['body'] = new
            return xml

class print_data():
    '''Class for writing the updated body data 
    to a file.
    Requires:
        hoomdfile
        filename to write text
    Returns:
        None
    '''
    def __init__(self, filename, writefile):
       self.print_bodies(filename, writefile)

    def get_data_from_xml(self, filename):
        '''
        Reads and parses type data of an xml
        to determine which bodies belong in 
        acceptor or donor bodies. Additionally,
        it sets indices for the transitions between
        types for rebuilding the xml body list.
        Requires:
            hoomdxml file
        Returns:
            xml list
        Also sets class global variables:
            Number of DBP molecules (self.num_DBP)
            The index of the first atom in DBP (self.first_C)
            The index of the final atom in DBP (self.end_C)
            The index of the first atom in the acceptor moiety (self.first_acc)
            The index of the final atom in the acceptor moiety (self.end_acc)

        '''
        xml = mh.loadMorphologyXML(filename)
        num_atoms = len(xml['type'])
        self.num_DBP = xml['type'].count('C')//64
        num_C = xml['type'].count('C')
        num_other = num_atoms - num_C
        self.first_C = xml['type'].index('C')
        self.end_C = num_C+self.first_C
        self.first_acc = xml['type'].index('CA')
        self.end_acc = self.first_acc + num_other
        return xml

    def print_bodies(self, filename, writefile):
        '''Separates list data and writes lists 
        for the bodies representing
        chromophores in the simulation.
        Requires:
            hoomdfile
            filename to write text
        Returns:
            None
        '''
        xml = self.get_data_from_xml(filename)
        bodies = np.array(xml['body'])
        donors = bodies[self.first_C:self.end_C]
        acceptors = bodies[self.first_acc:self.end_acc]
        acceptors = [x for x in set(acceptors) if x != -1]
        with open(writefile, 'w+') as f:
            f.write('DONORS\n')
            f.write(str(list(set(donors))))
            f.write('\nACCEPTORS\n')
            f.write(str(acceptors))
        

if __name__ == "__main__":
    infile = sys.argv[1]
    system = infile[:-4]
    conversion = convert_DBP()
    xml = conversion.get_data_from_xml(infile)
    #acceptors = conversion.separate(xml)
    #conversion.reorder_DBP(xml, acceptors, 'adjusted_'+infile)
    conversion.unify_DBP(xml, acceptors=None, filename = 'fix_'+infile)
    #conversion.unify_DBP(xml, acceptors, 'fix_'+infile)
    #fixed = print_data(filename = infile, writefile = system+'_fixda.txt')
    #fixed = print_data(filename = 'fix_'+infile, writefile = system+'_fixda.txt')
    #adjusted = print_data(filename = 'adjusted_'+infile, writefile = system+'_adjustedda.txt')
