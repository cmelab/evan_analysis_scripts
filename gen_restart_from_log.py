from subprocess import call
from os import environ, path

def first_line():
    system = environ['HOME'].split('/')[1]
    if system == 'Users':
        VMD_create = "rlwrap /Applications/VMD\ 1.9.2.app/Contents/MacOS/startup.command"
    if system == 'home':
        VMD_create = "vmd"
    return VMD_create

def final_frame(write_time):
    with open('log', 'r') as f:
        lines = f.readlines()
        last = lines[-1]
    timesteps = int(last.split()[0])
    return int(timesteps//write_time)

def determine_size():
    size = path.getsize('traj.dcd')
    if size <= 6e6:
        write_time = 1e7
    else:
        write_time = 1e6
    write_time = 1e7
    #write_time = 5e6
    fframe = final_frame(write_time)
    return fframe

def write_and_destroy(fframe, filename = 'restart.xml'):
    VMD_create = first_line()
    VMD_create += " -dispdev text -eofexit < <"
    VMD_create += "(echo \"mol new {init.xml} type {hoomd} "
    VMD_create += "first {} last {} step 1 waitfor 1; ".format(fframe, fframe)
    VMD_create += "mol addfile {traj.dcd} type {dcd} "
    VMD_create += "first {} last {} step 1 waitfor 1 0; ".format(fframe, fframe)
    VMD_create += "animate write hoomd {"+"{}".format(filename)+"} beg end end end skip 1 0;exit\")"

    outputFile = "temp.sh"
    with open(outputFile, 'w+') as tclFile:
        tclFile.writelines(VMD_create)

    call('bash temp.sh > /dev/null', shell=True)
    call('rm temp.sh', shell=True)

if __name__ == "__main__":

    fframe = determine_size()
    print("Final frame: {}.".format(fframe))
    write_and_destroy(fframe)
