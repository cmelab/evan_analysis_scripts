import numpy as np
from sys import argv
import matplotlib.pyplot as plt
import mdtraj as md

Top = argv[1]
Traj = argv[2]
a = md.load(Traj, top = Top)

delta_ones = []
def get_delta_ones(traj):
    for i in range(a.n_frames-1):
        for j in range(40):
            for k in range(3):
                delta_ones.append(np.linalg.norm(traj[i][j][k]-traj[i+1][j][k]))
    return np.array(delta_ones)

t = get_delta_ones(a.xyz)
#print delta_ones
#bins = np.histogram(np.array(delta_ones),10)
print a.unitcell_lengths[a.n_frames-1]
plt.hist(delta_ones,bins =50)
plt.xlabel('absolute distance moved in x,y,z')
plt.ylabel('# moved')
plt.savefig('histogram.png')
plt.show()
