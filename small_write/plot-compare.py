import sys
import numpy as np
import matplotlib.pyplot as plt


#hoomdmsd1 = np.loadtxt(sys.argv[1],skiprows = 1)
hoomdmsd = np.loadtxt(sys.argv[1],skiprows = 1)
#regmsd1 = np.loadtxt(sys.argv[2], skiprows = 1)
regmsd = np.loadtxt(sys.argv[2], skiprows = 1)
#hoomdmsd = np.loadtxt(sys.argv[5],skiprows = 1)

scaler = 1000# For raw msd
#scaler = 10# For Delta ones
#plt.plot(hoomdmsd1[:,0],1.*hoomdmsd1[:,1],label = 'hoomd1e4')
plt.plot(hoomdmsd[:,0],1000.*hoomdmsd[:,1],label = 'hoomd1e3')
#plt.plot(float(scaler)*regmsd1[:,0], 100*regmsd1[:,1], label='unwrapped1e4')
plt.plot(1.*float(scaler)*regmsd[:,0], 1.*regmsd[:,1], label='unwrapped')
#plt.plot(hoomdmsd[:,0],hoomdmsd[:,1],label = 'hoomd1e6')
plt.legend(loc = 2)
plt.xlabel('timestep')
plt.ylabel('MSD')
plt.savefig('hoomd-v-reg.png')
plt.show()
