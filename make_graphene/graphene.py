import numpy as np
import hoomd
from hoomd import md
from hoomd import deprecated
from cme_utils.manip import builder
from cme_utils.manip import ff 
from cme_utils.manip import hoomd_xml
from cme_utils.manip.initialize_system import interactions_from_xml_verbatim, interactions_from_xml_tabulated
from random import random, randint
from subprocess import check_output
import sys
from generate_system import gen_graphene

rigid_solvent = True

graphene = gen_graphene(span = 12, ion_atoms = 50, rigid_solvent = True)
graphene.build_graphene()
pos = np.array(graphene.positions)
pos = tuple(map(tuple, pos))
c_types = []
for atom in graphene.positions:
    c_types.append('CA')

hoomd.context.initialize()
system = deprecated.init.read_xml(filename = 'init.xml')

if rigid_solvent:
    system.particles.types.add('C')
    system.particles.types.add('CH')
    system.particles.types.add('O')
    system.particles.types.add('OE')
    s_pos = np.array(graphene.s_vectors)
    s_pos = tuple(map(tuple, s_pos))
    s_types = ['CH', 'CH', 'C', 'OE', 'OE', 'O']
    s_diam = [1.0, 1.0, 0.987, 0.789, 0.789, 0.779]

system.particles.types.add('CA')

nl_c,lj, bonds, angles, dihedrals = interactions_from_xml_verbatim(system,factor=1.0, model_file= 'model.xml', model_name= 'default')

nl_c = md.nlist.cell(check_period=1)

#nl_c.reset_exclusions(exclusions=['bond','angle','dihedral','body'])

grp = hoomd.group.all()
center = hoomd.group.rigid_center()
rigid = hoomd.md.constrain.rigid()
if rigid_solvent:
    rigid.set_param('SO', positions = s_pos, types = s_types, diameters = s_diam)
rigid.set_param('G', positions=pos, types = c_types)
rigid.create_bodies()
flexible = hoomd.group.nonrigid()

graphene_atoms = len(pos)
index = len(system.particles)-len(pos)

if rigid_solvent:
    for atom in range(len(system.particles)):
        p = system.particles[atom]
        if p.type == 'CH':
            p.charge = 2.794
            p.mass = 0.58
        if p.type == 'C':
            p.charge = 22.406
            p.mass = 0.5
        if p.type == 'O':
            p.charge = -13.147
            p.mass = 0.581
        if p.type == 'OE':
            p.charge = -9.544
            p.mass = 0.581

hoomd.deprecated.dump.xml(group = hoomd.group.all(), all = True, filename = 'init.xml')
hoomd.deprecated.dump.xml(group = hoomd.group.all(), all = True, filename = 'anchor.xml')

#charged = hoomd.group.charged()
both = hoomd.group.union(name='flex-and-rigid', a = center, b = flexible)

hoomd.md.integrate.mode_standard(dt=1e-4)
#int_rig = hoomd.md.integrate.nvt(group = center,kT=5.0,tau=2.0)
#int_flex = hoomd.md.integrate.nvt(group = flexible,kT=5.0,tau=2.0)
int_flex = hoomd.md.integrate.nvt(group = both,kT=5.0,tau=2.0)

hoomd.dump.dcd(filename = "shrunk.dcd", period = 1e3, overwrite = True)

resize = hoomd.update.box_resize(Lz = hoomd.variant.linear_interp([(0,system.box.Lz),(5e3, graphene.adjust_lz)]))

hoomd.dump.dcd(filename = "traj.dcd", period = 1e3, overwrite = True)

hoomd.run(5e3)

hoomd.deprecated.dump.xml(group = hoomd.group.all(), all = True, filename = 'init.xml')
hoomd.deprecated.dump.xml(group = hoomd.group.all(), all = True, filename = 'anchor.xml')

graphene.add_anchors(index, graphene_atoms, 'anchor.xml')

exit()

resize.disable()

print("BEGINNING THE RUN!")
#hoomd.md.integrate.mode_standard(dt=1e-4)
#int_flex = hoomd.md.integrate.nvt(group = both,kT=5.0,tau=2.0)

hoomd.dump.dcd(filename = "traj.dcd", period = 1e3, overwrite = True)
hoomd.analyze.log(filename = 'log', quantities = ['potential_energy', 'pair_lj_energy'],  period = 1e2, header_prefix = '#', overwrite = False)

hoomd.run(1e5)

hoomd.deprecated.dump.xml(group = hoomd.group.all(), all = True, filename = 'restart.xml')
