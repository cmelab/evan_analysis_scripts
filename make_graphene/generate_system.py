'''The purpose of this program is to generate
to layers of graphene sheet with free cations
outside the two graphene sheets.'''
import numpy as np
import math
import helper_functions.matty_helper as mh
from cme_utils.manip import builder
import random
import time
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class gen_graphene():
    """Class designed to be called by graphene.py
    to initialize a graphene, ion, solvent system."""
    def __init__(self, **kwargs):
        self.ion_atoms = 20
        self.bl = 0.3737
        self.sigma = 3.8
        self.points = [[0., 0., 0.0],[math.sqrt(3)/2.*self.bl,0.5*self.bl, 0.0]]
        self.span = 5
        self.spread = 2.6
        self.x_max = 0.0
        self.x_min = 0.0
        self.y_max = 0.0
        self.y_min = 0.0
        self.z_max = 0.0
        self.z_min = 0.0
        self.lx = 0.0
        self.ly = 0.0
        self.lz = 0.0
        self.adjust_lz = 0.0
        self.neighbors_list = []
        self.positions = []
        self.anion_positions = []
        self.cation_positions = []
        self.solvent_positions = []
        self.s_vectors = np.array([[-.76, -1.42, 0.13],
        [0.76, -1.42, 0.13  ],
        [0.0, 0.66, -0.07   ],
        [0.9812, -0.09, 0.0 ],
        [-0.9812, -0.09, 0.0],
        [0.0, 1.815, -0.14  ]])/self.sigma
        self.total_pos = []
        self.types = []
        self.center_of_mass = []
        self.rigid_solvent = False
        for k,v in kwargs.items():
            self.__dict__[k] = v
        self.solvent_atoms = 15*self.ion_atoms

    def build_graphene(self):
        """Convenience function to call other
        functions and build the whole system."""
        start_time = time.time()
        self.gen_sheet()
        self.trim_atoms()
        self.duplicate()
        self.com()
        self.determine_area()
        self.ions()
        self.solvent()
        self.write_input()
        self.calc_z_axis()
        print("THANK YOU FOR GENERATING SOME GRAPHENE! ENJOY!")
        print(("IT TOOK: --- %s seconds ---" % (time.time() - start_time)))

    def gen_sheet(self):
        '''Takes a vector of x, y and z and 
        replicates that point every lattice
        position.
        bl (bond_length) is the bond-length
        in units of sigma. Default value is
        0.3737, which is C-C bond length in 
        graphene.
        span is the # of times the lattice
        should repeated in the +x and -x direction
        Returns a list of points.'''
        print("REPLICATING X-AXIS.")
        lst = []
        for i in range(-self.span,self.span, 1):
            for point in self.points:
                lst.append([point[0]+float(i)*self.bl*math.sqrt(3), point[1], -self.spread/2.0])
        self.positions += lst
        self.gen_y()

    def gen_y(self):
        '''Takes a list of x, y and z points
        and translates them my sqrt(3)/2 in 
        the y-direction to create a hexagonally-
        packed plane.
        bl (bond_length) is the bond-length
        in units of sigma. Default value is
        0.3737, which is C-C bondlength in 
        graphene.
        span is the # of times the lattice
        should repeated in the +x and -x direction
        Returns a list of x, y and z positions.
        '''
        print("REPLICATING Y-AXIS.")
        lst = []
        for j in self.positions:
            for i in np.arange(-self.span, self.span, 1):
                lst.append([j[0]+self.bl*math.sqrt(3)/4+math.sqrt(3)/4*self.bl*(-1)**i, j[1]+i*1.5*self.bl, -self.spread/2.0])
        self.positions = lst

    def find_neighbors(self, array):
        '''Determines which atoms are neighbors based
        off of the distance. Neighbors are defined
        as atoms that would be bonded together.
        Takes: numpy.array of positions
        Returns: list of neighbors.'''
        print("TRIMMING ATOMS.")
        n_list = [ [] for i in range(len(array)) ]
        for i in range(0, len(array)-1):
            if (i+1+3*self.span) <= len(array):
                for j in range(i+1, i+1+3*self.span):
                    a = array[i,:]
                    b = array[j,:]
                    d = np.linalg.norm(a-b)
                    if d <= (self.bl+self.bl*0.01):
                        n_list[i].append(j)
                        n_list[j].append(i)
            else:
                for j in range(i+1, len(array)):
                    a = array[i,:]
                    b = array[j,:]
                    d = np.linalg.norm(a-b)
                    if d <= (self.bl+self.bl*0.01):
                        n_list[i].append(j)
                        n_list[j].append(i)
        return n_list

    def trim_atoms(self):
        '''Self-referential function that calls
        the neighbor list and removes atoms that have 
        fewer than two neighbors.
        Takes: list of atom positions
        Returns list of atom positions.'''
        pop_list = []
        neighbors = self.find_neighbors(np.array(self.positions))
        self.neighbors_list = neighbors
        for i, n in enumerate(neighbors):
            if len(n) < 2:
                pop_list.append(i)
        pop_list.sort(reverse=True)
        if len(pop_list) > 0:
            try:
                for atom in pop_list:
                    self.positions.pop(atom)
            except IndexError:
                pass
            neighbors = self.trim_atoms()
        print("THIS IS THE END OF TRIM ATOMS!")

    def duplicate(self):
        """Adds a copy of the original graphene
        sheet to the xml at a distance given in
        units of sigma.
        Requires an xml of the original graphene
        sheet.
        """
        print("DUPLICATING THE SHEET OF GRAPHENE.")
        top_layer = np.array(self.positions)
        for i in top_layer:
            i += [0.0, 0.0, self.spread]
        top_layer = top_layer.tolist()
        self.positions += top_layer

    def com(self):
        print("CALCULATING CENTER OF MASS")
        self.center_of_mass = np.mean(np.array(self.positions), axis = 0)
        for i in range(len(self.center_of_mass)):
            if abs(self.center_of_mass[i]) <= 1e-6:
                self.center_of_mass[i] = 0.0
        print("CENTER of MASS:", self.center_of_mass)

    def determine_area(self):
        """Calculates the area which the graphene
        sheet covers in order to determine
        lithium ion placement."""
        surface = np.array(self.positions)
        x_min, x_max = np.min(surface[:,0]), np.max(surface[:,0])
        y_min, y_max = np.min(surface[:,1]), np.max(surface[:,1])
        z_min, z_max = np.min(surface[:,2]), np.max(surface[:,2])
        self.x_max = x_max
        self.x_min = x_min
        self.y_max = y_max
        self.y_min = y_min
        self.z_max = z_max
        self.z_min = z_min
        self.lx = x_max-x_min
        self.ly = y_max-y_min
        self.lz = z_max-z_min
        print("LET'S CALCULATE THE AREA!") 

    def rand_in(self):
        x = self.lx*random.random()*0.85-self.lx/2.*0.85
        y = self.ly*random.random()*0.85-self.ly/2.*0.85
        z = self.lz*random.random()*0.90-self.lz/2.*0.90
        trial = [x,y,z]
        return trial

    def set_in(self):
        trial = self.rand_in()
        while not self.check_coord(trial):
            trial = self.rand_in()
        self.total_pos += [trial]
        return trial
    
    def check_coord(self, trial):
        if abs(trial[2]) < (self.spread+1.):
            return False
        else:
            for m in self.total_pos:
                a1 = np.array(m)
                a2 = np.array(trial)
                distance = np.linalg.norm(a1-a2)
                if distance <= 2.0:
                    return False
            return True

    def ions(self):
        print("Adding anions and cations. You have {} of them.".format(2*self.ion_atoms))
        self.lz = self.ion_atoms * 5.
        print("{} Is now your z-width.".format(self.lz))
        for i in range(self.ion_atoms):
            add_atom = self.set_in()
            self.anion_positions += [add_atom]
        print("ADDED ANION TO SIMULATION")
        for i in range(self.ion_atoms):
            add_atom = self.set_in()
            self.cation_positions += [add_atom]
        print("ADDED CATION TO SIMULATION")

    def solvent(self):
        print("Adding solvent. You have {} molecules.".format(self.solvent_atoms))
        self.lz = self.ion_atoms * 5.
        count = 0
        for i in range(self.solvent_atoms):
            add_atom = self.set_in()
            self.solvent_positions += [add_atom]
            #print("Added Solvent Molecule {}".format(count))
            count +=1
        print("ADDED SOLVENT MOLECULE TO SIMULATION.")

    def neutralize(self, xml):
        num_center = 1+15*self.ion_atoms
        charges = np.array(xml['charge'])[num_center:]
        neutral = np.mean(charges)
        print(("NETURALIZING: TOTAL CHARGE WAS: {}".format(np.mean(charges))))
        charges -= neutral
        charges = charges.tolist()
        for center in range(num_center):
            charges.insert(center, 0)
        xml['charge'] = charges
        print(("NETURALIZING: Charges are now: {}".format(np.mean(charges))))
        return xml

    def write_input(self):
        xml = {'natoms':[], 'lx':[], 'ly':[], 'lz':[], 'xy':[], 'xz':[], 'yz':[], 'position':[], 'image':[], 'mass':[], 'diameter':[], 'type':[], 'body':[], 'bond':[], 'angle':[], 'dihedral':[], 'improper':[], 'charge':[]}
        xml['lx'] = self.lx
        xml['ly'] = self.ly
        xml['lz'] = self.lz
        xml['xy'] = 0
        xml['xz'] = 0
        xml['yz'] = 0
        xml['natoms'] = 1
        self.write_com(xml)
        self.write_solvent(xml)
        self.write_ions(xml)
        self.neutralize(xml)
        mh.writeMorphologyXML(xml, "init.xml")

    def write_com(self, xml):
        xml['position'] += [self.center_of_mass]
        xml['natoms'] + 1
        xml['mass'] += [100.0]
        xml['diameter'] += [1.0]
        xml['body'] += [0]
        xml['type'] += ['G']
        xml['charge'] += [0.0]
        return xml

    def bind_anion(self, xml):
        bonds = []
        for bond in range(1,7):
            bonds.append(["{}-{} {} {}".format(xml['type'][-7], xml['type'][(bond-7)], len(xml['type'])-7, len(xml['type'])+bond-7)])
            #print("{}-{} {} {}".format(xml['type'][-7], xml['type'][(bond-7)], len(xml['type'])-7, len(xml['type'])+bond-1))
        xml['bond'] += bonds
        return xml

    def anion_angles(self, xml):
        angles = []
        for at1 in range(len(xml['type'])-6, len(xml['type'])-1):
            for at2 in range(at1+1, len(xml['type'])):
                angles.append(["{}-P-{} {} {} {}".format(xml['type'][at1], at1, xml['type'][at2], len(xml['type'])-7, at2)])
                #print("{}-P-{} {} {} {}".format(xml['type'][at1], xml['type'][at2], at1, len(xml['type'])-7, at2))
        xml['angle'] += angles
        return xml
        
    def add_F(self, lst, xml):
        add_list = []
        add_list.append(np.array(lst) + np.array([0., 0., 0.]))
        add_list.append(np.array(lst) + np.array([1.6, 0., 0.])/self.sigma)
        add_list.append(np.array(lst) + np.array([-1.6, 0., 0.])/self.sigma)
        add_list.append(np.array(lst) + np.array([0., 1.6, 0.])/self.sigma)
        add_list.append(np.array(lst) + np.array([0., -1.6, 0.])/self.sigma)
        add_list.append(np.array(lst) + np.array([0., 0., 1.6])/self.sigma)
        add_list.append(np.array(lst) + np.array([0., 0., -1.6])/self.sigma)
        xml['position'] += add_list
        xml['type'] += ['P']
        xml['type'] += 2*['F1']
        xml['type'] += 2*['F2']
        xml['type'] += 2*['F3']
        xml['mass'] += [1.0]
        xml['mass'] += 6*[0.613]
        xml['diameter'] += [0.984]
        xml['diameter'] += 6*[0.821]
        xml['charge'] += [27.304]
        xml['charge'] += 6*[-7.947]
        xml['natoms'] += 7
        xml['body'] += 7*[-1]
        self.bind_anion(xml)
        self.anion_angles(xml)
        return xml

    def write_ions(self, xml):
        for anion in self.anion_positions:
            self.add_F(anion, xml)
        xml['position'] += self.cation_positions
        xml['mass'] += self.ion_atoms*[0.224]
        xml['diameter'] += self.ion_atoms*[0.384]
        xml['type'] += self.ion_atoms*['L']
        xml['charge'] += self.ion_atoms*[20.376]
        xml['body'] += self.ion_atoms*[-1]
        xml['natoms'] += 1*self.ion_atoms
        return xml
    
    def bind_solvent(self, xml):
        bonds = []
        bonds.append(["{}-{} {} {}".format(xml['type'][-6], xml['type'][-5], len(xml['type'])-6, len(xml['type'])-5)])
        bonds.append(["{}-{} {} {}".format(xml['type'][-5], xml['type'][-2], len(xml['type'])-6, len(xml['type'])-2)])
        bonds.append(["{}-{} {} {}".format(xml['type'][-6], xml['type'][-3], len(xml['type'])-5, len(xml['type'])-3)])
        bonds.append(["{}-{} {} {}".format(xml['type'][-3], xml['type'][-4], len(xml['type'])-3, len(xml['type'])-4)])
        bonds.append(["{}-{} {} {}".format(xml['type'][-2], xml['type'][-4], len(xml['type'])-2, len(xml['type'])-4)])
        bonds.append(["{}-{} {} {}".format(xml['type'][-1], xml['type'][-4], len(xml['type'])-1, len(xml['type'])-4)])
        xml['bond'] += bonds
        return xml

    def solvent_angle(self, xml):
        '''
        C-C-O 0 1 3
        C-O-C 0 4 2
        C-C-O 1 0 4
        C-O-C 1 3 2
        O-C-O 3 2 4
        O-C-O 3 2 5
        O-C-O 4 2 5
        '''
        angles = []
        key = [[len(xml['type'])-6, len(xml['type'])-5,len(xml['type'])-3],
                [len(xml['type'])-6, len(xml['type'])-2,len(xml['type'])-4],
                [len(xml['type'])-5, len(xml['type'])-6,len(xml['type'])-2],
                [len(xml['type'])-5, len(xml['type'])-3,len(xml['type'])-4],
                [len(xml['type'])-3, len(xml['type'])-4,len(xml['type'])-2],
                [len(xml['type'])-3, len(xml['type'])-4,len(xml['type'])-1],
                [len(xml['type'])-2, len(xml['type'])-4,len(xml['type'])-1]]
        for row in range(len(key)):
            angles.append(["{}-{}-{} {} {} {}".format(xml['type'][key[row][0]],
                xml['type'][key[row][1]],
                xml['type'][key[row][2]],
                key[row][0],
                key[row][1],
                key[row][2])])
        xml['angle'] += angles
        return xml

    def solvent_dihedral(self, xml):
        '''
        C-C-O-C 0 1 3 2
        C-O-C-O 0 4 2 3
        C-O-C-O 0 4 2 5
        C-C-O-C 1 0 4 2
        C-O-C-O 1 3 2 4
        C-O-C-O 1 3 2 5
        O-C-C-O 3 1 0 4
        '''
        dihedrals = []
        key = [[len(xml['type'])-6, len(xml['type'])-5,len(xml['type'])-3,len(xml['type'])-4],
                [len(xml['type'])-6, len(xml['type'])-2,len(xml['type'])-4,len(xml['type'])-3],
                [len(xml['type'])-6, len(xml['type'])-2,len(xml['type'])-4,len(xml['type'])-1],
                [len(xml['type'])-5, len(xml['type'])-6,len(xml['type'])-2,len(xml['type'])-4],
                [len(xml['type'])-5, len(xml['type'])-3,len(xml['type'])-4,len(xml['type'])-2],
                [len(xml['type'])-5, len(xml['type'])-3,len(xml['type'])-4,len(xml['type'])-1],
                [len(xml['type'])-3, len(xml['type'])-5,len(xml['type'])-6,len(xml['type'])-2]]
        for row in range(len(key)):
            dihedrals.append(["{}-{}-{}-{} {} {} {} {}".format(xml['type'][key[row][0]],
                xml['type'][key[row][1]],
                xml['type'][key[row][2]],
                xml['type'][key[row][3]],
                key[row][0],
                key[row][1],
                key[row][2],
                key[row][3])])
        xml['dihedral'] += dihedrals
        xml['improper'] += dihedrals
        return xml

    def add_solvent(self, lst, xml):
        """ Vectors for the solvent:
        -.76, -1.42, 0.13
        0.76, -1.42, 0.13
        0.0, 0.66, -0.07
        0.9812, -0.09, 0.0
        -0.9812, -0.09, 0.0
        0.0, 1.815, -0.14 
        """
        add_list = []
        if self.rigid_solvent:
            add_list.append(np.array(lst))
            xml['type'] += ['SO']
            xml['position'] += add_list
            xml['mass'] += 1*[0.1]
            xml['diameter'] += 1*[0.01]
            xml['charge'] += 1*[0.0]
            xml['natoms'] += 1
            xml['body'] += 1*[-1]
        else:
            add_list.append(np.array(lst) + np.array([-.76, -1.42, 0.13])/self.sigma)
            xml['type'] += ['CH']
            add_list.append(np.array(lst) + np.array([.76, -1.42, 0.13])/self.sigma)
            xml['type'] += ['CH']
            add_list.append(np.array(lst) + np.array([0.0, 0.66, -0.07])/self.sigma)
            xml['type'] += ['C']
            add_list.append(np.array(lst) + np.array([0.9812, -0.09, 0.0])/self.sigma)
            xml['type'] += ['OE']
            add_list.append(np.array(lst) + np.array([-0.9812, -0.09, 0.0])/self.sigma)
            xml['type'] += ['OE']
            add_list.append(np.array(lst) + np.array([0.0, 1.815, -0.14])/self.sigma)
            xml['type'] += ['O']
            xml['position'] += add_list
            xml['mass'] += 2*[0.581]
            xml['mass'] += 1*[0.517]
            xml['mass'] += 3*[0.581]
            xml['diameter'] += 2*[1.0]
            xml['diameter'] += 1*[0.987]
            xml['diameter'] += 2*[0.789]
            xml['diameter'] += 1*[0.779]
            xml['charge'] += 2*[2.794]
            xml['charge'] += 1*[22.406]
            xml['charge'] += 2*[-9.544]
            xml['charge'] += 1*[-13.147]
            xml['natoms'] += 6
            xml['body'] += 6*[-1]
            self.bind_solvent(xml)
            self.solvent_angle(xml)
            self.solvent_dihedral(xml)
        return xml

    def write_solvent(self, xml):
        for solvent in self.solvent_positions:
            self.add_solvent(solvent, xml)
        return xml

    def calc_z_axis(self):
        """NOTE: assumes a sigma value of 3.2m"""
        print("Your lx = {} and ly = {} with {} ion molecules.".format(self.lx, self.ly, self.ion_atoms))
        M = self.ion_atoms*(15*(88.0)+151.9)
        M /= 6.022e23
        self.adjust_lz = M/(1.30 * self.lx * self.ly*(self.sigma*1e-8)**3) + self.spread + 0.934
        print("LZ:", self.adjust_lz)

    def pick_graphene_carbon(self, xml):
        print("Picking an index")
        start = 8*self.ion_atoms+1+15*self.ion_atoms
        end = start + len(self.positions)
        index = random.randint(start, end)
        if xml['type'][index] == 'CA':
            print("Accepting Index {}! TYPE: {}".format(index, xml['type'][index]))
            return index
        if xml['type'][index] != 'CA':
            print("Rejecting Index {}! TYPE: {}".format(index, xml['type'][index]))
            self.pick_graphene_carbon(xml)
        return index

    def add_anchors(self, index, graphene_atoms, xml = 'anchor.xml'):
        print("ADDING ANCHOR ATOMS TO THE GRAPHENE SHEET!")
        xml = mh.loadMorphologyXML(xml)
        bonds = []
        for anchor in range(1,4):
            atom = self.pick_graphene_carbon(xml)
            print("Binding {}, index {}".format(xml['type'][atom], atom))
            new_atom = len(xml['position'])
            xml['position'].append(xml['position'][atom])
            xml['type'] += ['A']
            xml['mass'] += [1.0]
            xml['diameter'] += [1.0]
            xml['charge'] += [0.0]
            xml['natoms'] += 1
            xml['body'] += [-1]
            xml['image'] += ["0 0 0"]
            bonds.append(["{}-{} {} {}".format(xml['type'][atom], xml['type'][new_atom], atom, new_atom)])
        xml['bond'] += bonds
        mh.writeMorphologyXML(xml, "anchor.xml")

if __name__ == "__main__":
    start_time = time.time()
    '''Desired bond-length in units of sigma.'''
    bond_length = 0.3737
    '''How many times you want the lattice 
    repeated. Made to produce square sheets'''
    rep = 20
    '''Distance between two graphene sheets.'''
    spread = 2.3
    '''These two points comprise the two unique
    points within a plane of hexagonally-packed
    particles.'''
    points = [[0., 0., 0.0],[math.sqrt(3)/2.*bond_length,0.5*bond_length, 0.0]]

    graphene = gen_graphene(span=rep, rep = 6, ion_atoms = 10)
    graphene.build_graphene()
